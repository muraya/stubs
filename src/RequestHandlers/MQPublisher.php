<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 4/19/18
 * Time: 5:08 PM
 */

class MQPublisher
{
    public function publish($payload, $url)
    {

        $coreUtils = new CoreUtils();
       $response =  $coreUtils::curlPost($url, $payload);
        return $response;
    }
}