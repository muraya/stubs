<?php

/**
 * Description of BankTransfer
 *
 * @author ericwanjau
 */
class BankTransfer {
        /**
     * The log class instance.
     *
     * @var object
     */
    public $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    public $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    public $toCode;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * Constructor.
     */

    public $coreUtils;

    public $data;
    public $requestHeader;
    public $requestBody;
    public $url;
    public  $msisdn;

    public $encrypt;

    public $mysql;
    public $databaseUtilities;
    public $sqlException;
    public $clientCode;

    /**
     * TransactionRequest constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->coreUtils = new CoreUtils();
        $this->fromCode = "";
        $this->toCode = "";
        $this->tat = new BenchMark(session_id());
  //      $this->encrypt = new RSAEncryption();


        $this->initValues();
    }


    
    public function initValues()
    {
        $this -> data = CoreUtils::receivePost();
//        $this->clientCode=  Bootstrap::determineClientCode($this->data);
          $this -> data = CoreUtils::receivePost();

        $this->requestHeader = $this->data['TransactionRequest']['requestHeader'];
        $this->requestBody = SAFKEProcessor::formatRequestData($this->data['TransactionRequest']['requestData']['data']);
    
    }
    //put your code here
    public function process(){
//        $method=
        return $this->processSAFKERequests();
    }
    public function processSAFKERequests(){
        //todo add another switch per clientCode
           $commandID = $this->requestHeader['cid'];
           //echo $commandID;
           $response=array();
        switch ($commandID) {
            case "fetchMyFI":
               $response= $this->fetchMyFI();
                break;
            case "activate":
               $response= $this->activate();
                break;
            case "FinancialInstitutionToMpesa":
               $response= $this->processB2C();
                break;
           // case "FinacialInstitutionToMpesa":
             //  $response= $this->processB2C();
               // break;
            case "authUSSDCallback":
               $response= $this->processUSSDCallback();
                break;
             case "authCallback":
               $response= $this->processUSSDCallback();
                break;
            default:
                $response=$this->renderInvalidCommandID();
        }
        return $response;
    }
    
        public function processUSSDCallBack()
    {
    //    $payload = array(
      //      "AuthResponse" => array(
        //        "responseHeader" => array(
          //          "txid" => "BNK1231231231",
            //        "dt" => "20170615082020"
              //  ),
   //             "responseData" => array(
     //               "rcode" => 200,
       //             "rtext" => "Successfully processed"
         //       )
          //  )
    //    );

      //  return $payload;
	$ussdCallBack = new AuthCallback();
       $ussdCallBack->processPinPromptCallback();
    }

    
    public function processB2C(){
//        return array(
  //          "TransactionResponse" => array(
    //            "header" => array(
     //              "txid" => "SFC1231231231",
       //             "dt" => "20170615082020"
         //       ),
           //     "responseData" => array(
             //       "rcode" => 200,
               //     "rtext" => "Successfully processed"
              //  )
           // )
       // );
	$TransactionRequest = new TransactionRequest();
        return $TransactionRequest->process();

    }


    public function renderInvalidCommandID(){
        return array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "null",
                    "dt" => date('Ymdhis')
                ),
                "responseData" => array(
                    "rcode" => 403,
                    "rtext" => "Invalid  command ID used"
                )
            )
        );

    }
    
    public function fetchMyFI()
    {

          $fetchMyFI = new FetchMyFI();

        return  $fetchMyFI -> process();
       /*
        $payload = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "SFC1231231231",
                    "dt" => "20170615082020",
                    "rcode" => 200,
                    "rtext" => "Successfully processed . 2 banks founds"
                ),
                "responseData" => array(
                    "data" => array(
                        array(
                            "id" => 1,
                            "BankAlias" => "KCBKE",
                            "BankName" => "Kenya Commercial Bank",
                            "BankShortCode" => 522522,
                            "BankID" => "133",
                            "AuthType" => 2
                        ),
                        array(
                            "id" => 2,
                            "BankAlias" => "DTBKE",
                            "BankName" => "Diamond Trust Commercial Bank",
                            "BankShortCode" => 102103,
                            "BankID" => "108",
                            "AuthType" => 2
                        )
                    )
                )
            )
        );

       return $payload;
     */
    }
     public function activate()
     
{ 


      $class= new AccountActivation();
      return $class->process();
/*
        $payload = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "SFC1231231231",
                    "dt" => "20170615082020"
                ),
                "responseData" => array(
                    "rcode" => 200,
                    "rtext" => "Successfully processed activate request"
                )
               )
        );

       return $payload;*/
    }

}
