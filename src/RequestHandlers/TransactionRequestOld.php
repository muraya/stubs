<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 2/22/18
 * Time: 1:14 PM
 */

class TransactionRequest
{
    /**
     * The log class instance.
     *
     * @var object
     */
    public $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    public $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    public $toCode;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * Constructor.
     */

    public $coreUtils;

    public $data;
    public $requestHeader;
    public $requestBody;
    public $url;
    public $msisdn;

    public $rsa;

    public $mysql;
    public $databaseUtilities;
    public $sqlException;


    /**
     * TransactionRequest constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->coreUtils = new CoreUtils();
        $this->fromCode = "";
        $this->toCode = "";
        $this->tat = new BenchMark(session_id());

        $this->initValues();

        coreUtils::loadDBConnection(
            Config::HUB_HOST,
            Config::HUB_DB,
            Config::HUB_PASS,
            Config::HUB_USER,
            'hub'
        );
    }

    /**
     * initialiase your proces
     */
    public function initValues()
    {
        $this -> data = CoreUtils::receivePost();
        $this->requestHeader = $this->data['TransactionRequest']['requestHeader'];
        $this->requestBody= SAFKEProcessor::formatRequestData($this->data['TransactionRequest']['requestData']['data']);
        $this->msisdn = CoreUtils::validateMSISDN($this->requestBody['Sender']);
        $this->url = $this->requestBody['cburl'];
        $this->fromCode = "SAFKE";
        $this->toCode = ClientConfigs::$clientData[$this->requestBody['SenderBankID']]['clientCode'];
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Incoming payload" . json_encode($this->data),
            $this->fromCode,
            $this->toCode
        );

        $this -> validatePayload();
        $authentication = $this->coreUtils->authenticateRequest(
            $this->requestHeader['ln'],
            $this->requestHeader['ps'],
            $this->requestHeader['dt']
        );

        if ($authentication != SAFKEConfigs::SUCCESS) {
            $this->formatResponse(
                SAFKEConfigs::AUTHENTICATION_FAILURE,
                SAFKEMessages::DEFAULT_AUTHENTIFICATION_FAILURE_MESSAGE
            );
        }
        $this->validateBank($this->requestBody['SenderBankID']);
        $this->validateIMSI();
        $this->validateAmount();
    }
    /**
     * This a simple method to simply publish our data to queue
     */
    public function publishToQueue()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        //function to publish to quee
        $MQPublisher = new MQPublisher();
        $url = RabbitMQConfigs::TRANSACTION_REQUESTMQ_URL;
        $queueName = RabbitMQConfigs::TRANSACTION_REQUEST_QUEUE_NAME;
        $payloadArray = array(
            'payload' => json_encode($this->data),
            'queueName' => $queueName,
            'msisdn' => $this->msisdn,
            'wrapperURL' => Config::transactionRequestWrapperURL

        );
        $publisherResponse = $MQPublisher->publish($payloadArray, $url);
        $p = json_decode($publisherResponse,true);
        if ($p['code'] != Config::SUCCESS) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Error publishing the request to queue" . $publisherResponse,
                $this->fromCode,
                $this->toCode
            );
            $message = Config::INTERNAL_SERVER_ERROR_MESSAGE;
            $this->formatResponse(
                Config::INTERNAL_SERVER_ERROR,
                $message
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        } else {
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            return true;
        }
    }
    /**
     * Logs requested payload to channelRequests table before forwarding to
     * MPESA API
     * @param $arrayPayload
     * @param $status - statusCode to log in overallStatus Column
     * @param $statusDescription - description of what the status means
     * @return mixed -the channelRequestID if successful,flag duplicate for
     * duplicate merchant transactionID and null if an error occurs
     */
    public function logChannelRequest()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);

        //encode to JSON object
        $payload = json_encode($this->data, JSON_FORCE_OBJECT);


        //log the request
        try {
            /*
             * We change the query to add a small delay for async requests
             * This allows the menu to end the session
             * gracefully to allow the USSD push to happen.FYI: its not a hack :-)
             */
            //Configure primary keys
            ORM::configure('id_column_overrides', array(
                'c_channelRequests' => 'channelRequestID',
            ));
            $channelRequestLog = ORM::for_table('c_channelRequests', 'hub')->create();

            //formalate our data so we save
            $channelRequestLog->MSISDN = $this->msisdn;
            $channelRequestLog->accessPoint = SAFKEConfigs::DEFAULT_TR_ACCESS_POINT;
            $channelRequestLog->message = $this->requestBody['Remarks'];
            $channelRequestLog->gatewayID = SAFKEConfigs::DEFAULT_GATEWAYID;
            $channelRequestLog->gatewayUID = $this ->requestHeader['txid'];
            $channelRequestLog->payload = $payload;
            $channelRequestLog->priority = SAFKEConfigs::DEFAULT_PRIORITY;
            $channelRequestLog->connectorID = SAFKEConfigs::DEFAULT_CONNECTORID;
            $channelRequestLog->networkID = SAFKEConfigs::SAFARICOM_NETWORK_ID;
            $channelRequestLog->IMCID = SAFKEConfigs::IMC_ID;
            $channelRequestLog->clientSystemID = SAFKEConfigs::CLIENT_SYSTEM_ID;
            $channelRequestLog->externalSystemServiceID =  SAFKEConfigs::DEFAULT_EXTERNAL_SYSTEM_SERVICE_ID;
            $channelRequestLog->activityID = SAFKEConfigs::DEFAULT_ACTIVITY_ID;
            $channelRequestLog->clientACKID =  SAFKEConfigs::DEFAULT_CLIENT_ACKID;
            $channelRequestLog->overalStatus = StatusCodes::PENDING_STATUS;
            $channelRequestLog->set_expr('statusHistory', 'NOW()');
            $channelRequestLog->statusDescription = SAFKEConfigs::DEFAULT_STATUS_DESCRIPTION;
            $channelRequestLog->serviceDescription =  SAFKEConfigs::DEFAULT_SERVICE_DESCRIPTION;
            $channelRequestLog->set_expr('expiryDate', SAFKEConfigs::CHANNEL_REQUEST_EXPIRY_PERIOD);
            $channelRequestLog->appID = SAFKEConfigs::DEFAULT_APP_ID;
            $channelRequestLog->set_expr('dateResponded', 'NOW()');
            $channelRequestLog->set_expr('dateCreated', 'NOW()');
            $channelRequestLog->set_expr('dateClosed', 'NOW()');
            $channelRequestLog->MOCost =  SAFKEConfigs::DEFAULT_MOCOST;
            $channelRequestLog->set_expr('dateModified', 'NOW()');
            $channelRequestLog->bucketID = SAFKEConfigs::DEFAULT_BUCKET_ID;
            $channelRequestLog->set_expr('lastSend', 'NOW()');
            $channelRequestLog->set_expr('firstSend', 'NOW()');
            $channelRequestLog->set_expr('nextSend', 'NOW()');
            $channelRequestLog->numberOfSends = SAFKEConfigs::DEFAULT_NUMBER_OF_SENDS;

            if ($channelRequestLog->save()) {
                //Saved request
                $channelRequestLogID = $channelRequestLog->id();

                $this->log->debugLog(
                    Config::INFO,
                    $this->msisdn,
                    "Channel request id" . $channelRequestLogID,
                    $this->fromCode,
                    $this->toCode
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
                return true;
            } else {
                $this->log->debugLog(
                    Config::ERROR,
                    $this->msisdn,
                    "Error loggin channel request" . json_encode($this->data),
                    $this->fromCode,
                    $this->toCode
                );
                // formulate error response
                $message = Config::INTERNAL_SERVER_ERROR_MESSAGE;
                $this->formatResponse(
                    Config::INTERNAL_SERVER_ERROR,
                    $message
                );
                //some other error occurred
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
                return null;
            }
        } catch (Exception $exception) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Error logging channel request" . $exception->getMessage(),
                $this->fromCode,
                $this->toCode
            );
            // formulate error response
            $message = Config::INTERNAL_SERVER_ERROR_MESSAGE;
            $this->formatResponse(
                Config::INTERNAL_SERVER_ERROR,
                $message
            );
            //some other error occurred
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            return null;
        }
    }

    /**
     * @return bool
     *  check if there is any instance of empty key values in request header
     */
    public function validatePayload()
    {
        //check if any of the headers and the body values are empty
        if (empty($this->requestHeader['txid']) ||
            !ctype_alnum($this->requestHeader['txid']) ||
            substr($this->requestHeader['txid'], 0, 3) != SAFKEConfigs::DEFAULT_SAF_TXID ||
            empty($this->requestHeader['cid']) ||
            empty($this->requestHeader['ln']) ||
            empty($this->requestHeader['ps']) ||
            empty($this->requestHeader['dt']) ||
            empty($this->requestHeader['pid']) ||
            $this->requestHeader['pid'] != SAFKEConfigs::DEFAULT_THIRD_PARTY_ID ||
            empty($this->requestHeader['pnm']) ||
            // $this->requestHeader['pnm'] != SAFKEConfigs::DEFAULT_SAF_THIRD_PARTY_NAME ||
            empty($this->requestHeader['authcode'])) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Validating headers payload: key " . json_encode($this->requestHeader). ' was empty',
                $this->fromCode,
                $this->toCode
            );
            // formulate error response

            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE
            );
        } else {
            if (empty($this->requestBody['cburl']) ||
                empty($this->requestBody['Sender'])  ||
                !ctype_digit($this->requestBody['Sender'])||
                CoreUtils::validateMSISDN($this->requestBody['Sender']) == false ||
                empty($this->requestBody['SenderBankID']) ||
                !ctype_digit($this->requestBody['SenderBankID']) ||
                empty($this->requestBody['SenderBankName']) ||
                empty($this->requestBody['ReceiverIdentity']) ||
                !ctype_alpha($this->requestBody['ReceiverIdentity']) ||
                CoreUtils::validateMSISDN($this->requestBody['ReceiverIdentifier']) == false ||
                empty($this->requestBody['ReceiverIdentifierType']) ||
                !ctype_alpha($this->requestBody['ReceiverIdentifierType']) ||
                empty($this->requestBody['ReceiverIdentifier']) ||
                !ctype_digit($this->requestBody['ReceiverIdentifier']) ||
                empty($this->requestBody['amount']) ||
                !ctype_digit($this->requestBody['amount']) ||
                empty($this->requestBody['Remarks'])||
                $this->requestBody['amount'] < 0) {
                $this->log->debugLog(
                    Config::ERROR,
                    $this->msisdn,
                    "Some parameters on the body are missing" . json_encode($this->requestBody),
                    $this->fromCode,
                    $this->toCode
                );
                // formulate error response
                $this->formatResponse(
                    SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                    SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE
                );
            }
        }
    }

    /**
     * Simple method to check if the bank id sent is configured with us
     * @param $bank
     * @return mixed
     */
    public function validateBank($bank)
    {
        $bankCode = ClientConfigs::$clientData[$bank]['clientCode'];

        if (!empty($bankCode) && !is_null($bankCode)) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Incoming bank code" . json_encode($bankCode),
                $this->fromCode,
                $this->toCode
            );
            return ClientConfigs::$clientData[$bank]['clientCode'];
        } else {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Error in fetching bank code for" . $bank,
                $this->fromCode,
                $this->toCode
            );
            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                Responses::UNKNOWN_BANK
            );
        }
    }

    /**
     * method to move to the correct function as defined by the cid
     */

    public function process()
    {

        $cir = $this->requestHeader['cid'];

        switch ($cir) {
            case "FinancialInstitutionToMpesa":
                $this->processBankToMpesa();
                break;
            default:
                $this->processBankToMpesa();
        }

    }

    /**
     * @param $url
     * @param $payload
     * @param array $headers
     * @return bool|mixed
     */

    public function curlPost($url, $payload)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($payload));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Config::DETAULT_CURL_CONNECTION_TIMEOUT);
        curl_setopt($ch, CURLOPT_TIMEOUT, Config::DETAULT_CURL_READ_TIMEOUT);

        $response = curl_exec($ch);


        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);

        //close the connection
        curl_close($ch);

        // if we have an error throw an exception

        if ($curl_errno) {
            //log here
            $this->log->errorLog(
                Config::DEBUG,
                $this->msisdn,
                "A curl error occured. Curl error number: ".$curl_errno.", curl error message: ".$curl_error
            );
            return false; //throw new FriendlyException('Unable to process request'); //CURL_ERROR_GENERIC_MESSAGE
        }
        return $response;
    }

    public function processBankToMpesa1(){

        $url = "http://10.250.250.28:9000/mss/tests/curlTest.php";
        $this->curlPost($url, $this->payload);
    }

    /**
     *  A method to do a financial institution to mpesa prompt
     */
    public function processBankToMpesa()
    {
        $response =  $this->logChannelRequest();
        //$response =  $this->publishToQueue();
        if ($response==true) {
            $this->formatResponse(
                SAFKEConfigs::SUCCESS,
                SAFKEMessages::DEFAULT_ASYNC_B2C_RESPONSE_MESSAGE
            );
        } else {
            $message = Config::INTERNAL_SERVER_ERROR_MESSAGE;
            $this->formatResponse(
                Config::INTERNAL_SERVER_ERROR,
                $message
            );
        }
    }

    /**
     * method to format our response message
     * @param $statusCode
     * @param $message
     */
    public function formatResponse($statusCode, $message)
    {
        $response = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => $this->requestHeader['txid'],
                    "dt" => $this->requestHeader['dt']
                ),
                "responseData" => array(
                    "rcode" => $statusCode,
                    "rtext" => $message
                )
            )
        );

        $this->coreUtils ->renderResponse($response);
    }

    /**
     * This is a method to simply do a check on the last sim swap date to be greater than 48 hours
     */
    public function validateIMSI()
    {
        $start  = date_create($this->requestBody['LastSimSwap']);
        $end = date_create();// Current time and date
        $diff = date_diff($start, $end);
        if ($this->requestBody['LastSimSwap'] != null) {
            if ($diff->d <= SAFKEConfigs::SIM_SWAP_TIME_CHECK) {
                $this->log->debugLog(
                    Config::ERROR,
                    $this->msisdn,
                    "The sim has been recently had a sim swap within the last 48 hours" . $this->msisdn,
                    $this->fromCode,
                    $this->toCode
                );
                $this->formatResponse(
                    SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                    Responses::RECENTLY_SWAPPED_LINE
                );
            }
        }
    }

    /**
     * This is a simple method to validate the amount sent by customer for transaction
     */
    public function validateAmount()
    {
        if ($this->requestBody['amount'] < Config::MIN_TRANSACTION_AMOUNT ||
            $this->requestBody['amount'] > Config::MAX_TRANSACTION_AMOUNT) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "The amount being transfered is not within the specified ranges" . $this->requestBody['amount'],
                $this->fromCode,
                $this->toCode
            );
            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                Responses::INVALID_TRANSACTION_AMOUNT_LIMIT
            );
        }
    }

}

