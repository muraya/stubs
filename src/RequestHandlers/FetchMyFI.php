<?php

/**
 * This class handles FetchMyFI functionality
 * Gets the banks tied to the passed MSISDN number
 * in the payload for requesting MNO
 *
 * PHP Version 5.6.34
 *
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 * Date: 2/20/18
 * Time: 11:52 AM
 */

class FetchMyFI
{

    /**
     * The log class instance.
     *
     * @var object
     */
    public $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    public $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    public $toCode;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * @var CoreUtils
     *  Holds CoreUtils class instance
     */
    public $coreUtils;

    /**
     * @var array
     *  Holds the overall request payload
     */
    public $data;

    /**
     * @var array
     * Holds the request payload header
     */
    public $requestHeader;

    /**
     * @var array
     *  Holds the request payload body
     */
    public $requestBody;

    /**
     * @var string
     *  Holds the mobile number in the request payload
     */
    public  $msisdn;

    /**
     * @var RedisController
     * Holds RedisController class instance
     */
    public $redis;


    /**
     * FetchMyFI constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->coreUtils = new CoreUtils();

        $this->fromCode = "";
        $this->toCode = "";

        $this->redis = new RedisController();
        $this->tat = new BenchMark(session_id());

        $this->initValues();
    }

    /**
     * initiates the basic class methods
     */
    public function initValues()
    {

        $this -> data = CoreUtils::receivePost();

        $this->requestHeader = $this->data['TransactionRequest']['requestHeader'];
        $this->requestBody =  SAFKEProcessor::formatRequestData($this->data['TransactionRequest']['requestData']['data']);

        $this->msisdn = $this->requestBody['msisdn'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, "Incoming Request here: " . json_encode($this -> data));

        $this -> validatePayload();

    }

    /**
     * @return bool
     *  check if there is any instance of empty key values in request header
     */
    public function validatePayload()
    {
        //check if any of the headers and the body values are empty
        if (
            empty($this->requestHeader['txid']) || !ctype_alnum($this->requestHeader['txid']) || empty($this->requestHeader['cid']) ||
            empty($this->requestHeader['ln'])   || empty($this->requestHeader['ps'])          ||  empty($this->requestHeader['dt']) ||
            empty($this->requestHeader['pid'])  || empty($this->requestHeader['pnm'])
        )
        {

            $this->log->debugLog(Config::ERROR, $this->msisdn, " Empty Key " . json_encode($this->requestHeader). ' was empty', $this->fromCode, $this->toCode);
            // formulate error response

            $this->formatResponse($banks = null, SAFKEConfigs::INVALID_REQUEST_PARAMETERS);

        } else {
            if (!is_numeric($this->requestBody['msisdn']) ) {
                $this->log->errorLog(Config::ERROR, $this->msisdn, "Wrong MSISDN number ::" . $this->requestBody['msisdn'], $this->fromCode, $this->toCode);
                // formulate error response
                $this->formatResponse($banks = null, SAFKEConfigs::INVALID_REQUEST_PARAMETERS);

            }
            elseif ( CoreUtils::validateMSISDN($this->requestBody['msisdn'])  == false ) {
                $this->log->errorLog(
                    Config::ERROR, $this->msisdn, " Wrong MSISDN number format :: " . $this->requestBody['msisdn'], $this->fromCode, $this->toCode
                );
                // formulate error response
                $this->formatResponse($banks = null, SAFKEConfigs::INVALID_REQUEST_PARAMETERS);

            }
            elseif (substr($this->requestHeader['txid'], 0, 3) != SAFKEConfigs::DEFAULT_SAF_TXID )
            {
                $this->log->errorLog(
                    Config::ERROR, $this->msisdn, " Wrong txid :: " . $this->requestHeader['txid'], $this->fromCode, $this->toCode
                );
                // formulate error response
                $this->formatResponse($banks = null, SAFKEConfigs::INVALID_REQUEST_PARAMETERS);
            }
            elseif ($this->requestHeader['pid'] != SAFKEConfigs::DEFAULT_THIRD_PARTY_ID )
            {
                $this->log->errorLog(
                    Config::ERROR, $this->msisdn, " Wrong pid :: " . $this->requestHeader['pid'], $this->fromCode, $this->toCode
                );
                // formulate error response
                $this->formatResponse($banks = null, SAFKEConfigs::INVALID_REQUEST_PARAMETERS);

            }
            else {
                $this->msisdn = CoreUtils::validateMSISDN($this->requestBody['msisdn']);
            }
        }
    }


    /**
     * handles the transaction process flow
     *
     */
    public function process()
    {

        // Authenticate Request
        $response = CoreUtils::authenticateRequest($this->requestHeader['ln'],$this->requestHeader['ps'],$this->requestHeader['dt']);

        if ($response == Config::SUCCESS)
        {

            // fetch banking information
            $bankEnrolement = $this->getBankEnrolmentsFromRedis();

            // fetch profile info
            if ($bankEnrolement != false){

                if (count($bankEnrolement) > 0){

                    $appendedBankDetails = $this->appendBanksData($bankEnrolement);

                    if (count($appendedBankDetails) > 0) {

                        $this->formatResponse($appendedBankDetails, SAFKEConfigs::SUCCESS);

                    } else {

                        $this->formatResponse( $banks = null, SAFKEConfigs::EMPTY_BANKING_INSTITUTIONS_CODE);

                    }

                    $this->formatResponse($appendedBankDetails, SAFKEConfigs::SUCCESS);

                } else {

                    $this->log->errorLog(Config::ERROR, $this->msisdn, "Banks not tied to mobile number : ".$this->msisdn);

                    $this->formatResponse( $banks = null, SAFKEConfigs::EMPTY_BANKING_INSTITUTIONS_CODE);
                }
            } else {

                $this->formatResponse( $banks = null, SAFKEConfigs::EMPTY_BANKING_INSTITUTIONS_CODE);

            }

        } else {

            return $this->formatAuthResponse(SAFKEConfigs::AUTHENTICATION_FAILURE,SAFKEMessages::DEFAULT_AUTHENTIFICATION_FAILURE_MESSAGE);

        }
    }


    /**
     * @return bool
     *  Fetches Bank Enrolments of the passed MSISDN number from bankEnrolments field
     */
    public function getBankEnrolmentsFromRedis()
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try
        {

            $overallPayload = $this->redis->getKeyFieldValue(Config::BANK_ENROLEMENTS_REDIS_KEY.$this->msisdn, Config::BANK_ENROLEMENTS_REDIS_FIELD);

            if (empty($overallPayload)) {

                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

                $this->log->errorLog(Config::ERROR, $this->msisdn, " Empty Bank Enrollments : " . json_encode($overallPayload));

                $this->formatResponse( $banks = null, SAFKEConfigs::EMPTY_BANKING_INSTITUTIONS_CODE);

            } else {

                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

                $banksPayload = json_decode($overallPayload, true);

                $this->log->infoLog(Config::INFO, $this->msisdn, " Redis Enrolments :: ".json_encode($banksPayload));

                return $banksPayload;

            }
        }

        catch (Exception $exception) {

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            $this->log->errorLog(Config::ERROR, $this->msisdn, " Error getting information from redis :: " . $exception->getMessage());

            $this->formatResponse( $banks = null, SAFKEConfigs::EMPTY_BANKING_INSTITUTIONS_CODE);

        }
    }

    /**
     *  Append Banks
     * @param $banksData
     * @return array
     */
    public function appendBanksData($banksData)
    {

        $appendedBanksData = array();

        foreach ($banksData as $key =>  $value)
        {

            if (ClientConfigs::$clientData[$value]['isB2CActive'] != true){
                continue;
            }
            else {
                $data['id'] = $value;
                $data['BankAlias'] = ClientConfigs::$clientData[$value]['clientCode'];
                $data['BankName'] = ClientConfigs::$clientData[$value]['clientName'];
                $data['BankShortCode'] = ClientConfigs::$clientData[$value]['payBillNumber'];
                $data['BankID'] = ''.$value.'';
                $data['AuthType'] = '2';

                $appendedBanksData[] = $data;
            }
        }

        return $appendedBanksData;

    }

    /**
     *
     *  Format authentication response mainly failure instances
     * @param $statusCode
     * @param $message
     *
     */
    public function formatAuthResponse($statusCode, $message)
    {

        $response = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => $this->requestHeader['txid'],
                    "dt" => $this->requestHeader['dt']
                ),
                "responseData" => array(
                    "rcode" => $statusCode,
                    "rtext" => $message
                )
            )
        );

         $this->coreUtils ->renderResponse($response);

    }


    /**
     * Format overall response after successful authentication
     * @param null $banksData
     */
    public function formatResponse($banksData, $status )
    {

        if (!is_null($banksData)){
            $response=array(
                "TransactionResponse"=>array(
                    "responseHeader"=>array(
                        "txid"=> $this->requestHeader['txid'],
                        "dt"=> $this->requestHeader['dt'],
                        "rcode"=>SAFKEConfigs::SUCCESS,
                        "rtext"=> SAFKEMessages::SUCCESS_MESSAGE,
                    ),
                    "responseData"=>array(
                        "data" =>$banksData,
                    ),
                ),
            );
        } else {
            if ($status == SAFKEConfigs::INVALID_REQUEST_PARAMETERS)
            {
                $response=array(
                    "TransactionResponse"=>array(
                        "responseHeader"=>array(
                            "txid"=> $this->requestHeader['txid'],
                            "dt"=> $this->requestHeader['dt'],
                            "rcode"=>SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                            "rtext"=> SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE,
                        ),
                        "responseData"=>array(
                            "data" =>SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                        ),
                    ),
                );
            } else {
                $response=array(
                    "TransactionResponse"=>array(
                        "responseHeader"=>array(
                            "txid"=> $this->requestHeader['txid'],
                            "dt"=> $this->requestHeader['dt'],
                            "rcode"=>SAFKEConfigs::EMPTY_BANKING_INSTITUTIONS_CODE,
                            "rtext"=> SAFKEMessages::EMPTY_BANKING_INSTITUTIONS_MESSAGE,
                        ),
                        "responseData"=>array(
                            "data" =>array(),
                        ),
                    ),
                );
            }
        }

        $this->coreUtils -> renderResponse($response,$_GET['url']);

    }


}