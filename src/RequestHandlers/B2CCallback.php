<?php

/**
 * This class handles B2CCallback functionality
 *
 * PHP Version 5.6.34
 *
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 * Date: 3/12/18
 * Time: 3:30 PM
 */

class B2CCallback
{

    /**
     * The log class instance.
     *
     * @var object
     */
    public $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    public $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    public $toCode;

    /**
     * TAT - turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * Payload Data received from request
     * @var object
     */
    public $data;

    /**
     * Initializes an instance of coreUtils class
     * @var object
     */
    public $coreUtils;

    /**
     * Channel Request record retrieved from the
     * passed hub_id in the payload
     * @var object
     */
    public $channelRequestData;

    /**
     * Channel Request payload column data contained in
     * the channel request data
     * @var object
     */
    public $channelRequestDataPayload;

    /**
     * Mobile number from request parameters
     * @var int
     */
    public  $msisdn;

    /**
     * B2CCallback class constructor.
     */

    public function __construct()
    {

        $this->log = new CoreAppLogger();
        $this->coreUtils = new CoreUtils();

        $this->fromCode = "";
        $this->toCode = "";

        $this->tat = new BenchMark(session_id());
        $this->initValues();

    }


    /**
     * Init Values -- Initializes the core functions and variables of the class
     */
    public function initValues()
    {
        $this -> data = CoreUtils::receivePost();

        $this->log->debugLog(Config::DEBUG,-1, " Incoming request payload :" . json_encode($this->data),$this->fromCode);

        $this->msisdn = $this->data['msisdn'];
    }


    /** handles the transaction process flow
     * @return bool
     */
    public function process()
    {

        $this->channelRequestData = $this -> fetchChannelRequestData();

        $channelRequestPayloadData =  $this -> channelRequestData['payload'];

        /**
         * check if the payload is valid
         */

        if ($this -> data['msisdn'] == $this -> channelRequestData['MSISDN'])
        {
            /**
             * invoke the callback url
             */
            $formattedCallbackData = $this -> formatCallbackPayload($channelRequestPayloadData);

            /**
             * invoke the callback url and get the response
             */

            $callbackResponse = $this -> invokeCallbackUrl($formattedCallbackData);

            if (!empty($callbackResponse))
            {

                $this -> formatResponse(StatusCodes::STATUS_PUSH_SUCCESS,SAFKEMessages::SUCCESS_MESSAGE);

            }
            else {

                $this -> formatResponse(StatusCodes::STATUS_PUSH_FAILURE,SAFKEMessages::DEFAULT_FAILURE_RESPONSE);

            }

        } else {

            $this -> formatResponse(StatusCodes::STATUS_PUSH_FAILURE,SAFKEMessages::DEFAULT_FAILURE_RESPONSE);
        }

    }


    /**
     *  Fetches ChannelRequest instance from the passed channelRequestID
     * @return bool|mixed
     */
    public function fetchChannelRequestData()
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        if (is_numeric(intval($this -> data['hubID'])) && intval($this -> data['hubID']) > 0){

            try {
                $dblink = new MySQL(Config::HUB_HOST, Config::HUB_USER, Config::HUB_PASS, Config::HUB_DB, Config::HUB_PORT);

                $query = "SELECT gatewayUID,clientACKID, MSISDN, payload  FROM c_channelRequests WHERE channelRequestID = ? LIMIT 1;";

                $params = array('i', $this->data['hubID']);

                $this->log->debugLog(Config::DEBUG, $this->msisdn, "query: "
                    . CoreUtils::formatQueryForLogging($query, $params), '');

                $channelRequestData = DatabaseUtilities::executeQuery($dblink->mysqli, $query,$params);

                if ($channelRequestData) {

                    $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

                    return $channelRequestData[0];

                } else {

                    $this->log->errorLog(Config::ERROR, $this->msisdn, " Missing Channel Request Record : ");

                    $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

                    return FALSE;

                }

            } catch (SQLException $exception) {

                $this->log->errorLog(Config::ERROR, $this->msisdn, $exception->getMessage(), $this->msisdn);

                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

                $this -> formatResponse(StatusCodes::STATUS_PUSH_FAILURE, $exception->getMessage());

            } catch (Exception $ex) {

                $this->log->errorLog(Config::ERROR, $this->msisdn, $ex->getMessage(), $this->msisdn);

                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

                $this -> formatResponse(StatusCodes::STATUS_PUSH_FAILURE, $ex->getMessage());
            }
        }
        else {

            $this -> formatResponse(StatusCodes::STATUS_PUSH_FAILURE,SAFKEMessages::DEFAULT_FAILURE_RESPONSE);

        }

    }


    /**
     *  Formats the callback payload to be pushed to callBack url
     * @param $channelRequestPayloadData
     * @return array
     */
    public function formatCallbackPayload($channelRequestPayloadData)
    {

        // TODO -- handle a case scenario of failures and listing the root cause

        $finalStatus = $this -> determineFinalStatus();

        $decodedChannelRequestPayloadData = json_decode($channelRequestPayloadData, true);

        $timestamp = date('YmdHis');

        $response = array(
            "CallbackRequest"=>array(
                    "resultHeader"=>array(
                        "txid" => $this->channelRequestData['gatewayUID'],
                        "dt" => $timestamp,
                        "tid" => $this->data['payerTransactionID'],
                        "cid" => $decodedChannelRequestPayloadData['TransactionRequest']['requestHeader']['cid'],
                        "ln" => Config::OUTGOING_AUTH_USERNAME,
                        "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD,$timestamp),
                        "pid" => $decodedChannelRequestPayloadData['TransactionRequest']['requestHeader']['pid'],
                        "pnm" => $decodedChannelRequestPayloadData['TransactionRequest']['requestHeader']['pnm'],
                    ),
                "resultData" =>array(
                            "rcode" => $finalStatus['status'],
                            "rtext" => $finalStatus['message'],
                            "data"=>array(
                                            array(
                                                'key' => 'BankRefNo',
                                                'value' => $this->data['payerTransactionID'],
                                            ),
                                            array(
                                                'key' => 'MPESARefNo',
                                                'value' => $this->data['receiptNumber'],
                                            ),
                                            array(
                                                'key' => 'Amount',
                                                'value' => $this->data['amountPaid'],
                                            ),
                                            array(
                                                'key' => 'Date',
                                                'value' => date_format($this->data['paymentDate'], 'YmdHis'),
                                            ),
                                            array(
                                                'key' => 'SenderNo',
                                                'value' => $this->data['msisdn'],
                                            ),
                                            array(
                                                'key' => 'SenderAcc',
                                                'value' => '',
                                            ),
                                            array(
                                                'key' => 'ReceipientNo',
                                                'value' => $this->data['accountNumber'],
                                            ),
                                            array(
                                                'key' => 'ReceipientAcc',
                                                'value' => '',
                                            )
                                        )
                                    )
                                )
                            );

        $this->log->debugLog(Config::VERBOSE, $this->msisdn, " Callback Payload :".json_encode($response));

        return $response;

    }


    /**
     * @return array
     * Checks the response status code for the incoming request
     */
    public function determineFinalStatus()
    {
        if ($this -> data['statusCode'] == StatusCodes::STATUS_PUSH_SUCCESS)
        {
            $response = array(
                'status' => StatusCodes::OVERALL_SUCCESS,
                'message' => $this -> data['statusDescription']
            );
        }
        else {
            $response = array(
                'status' => $this -> data['statusCode'],
                'message' => SAFKEMessages::CALLBACK_FAILURE_MESSAGE
            );
        }

        return $response;
    }


    /**
     *  Invokes the  callBackUrl got from payload retrieved from channelRequest
     * @param $formattedCallbackData
     * @return mixed
     */
    public function invokeCallbackUrl($formattedCallbackData)
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);
        $requestPayload = json_decode( $this -> channelRequestData['payload'], true);

        $requestPayloadData = SAFKEProcessor::formatRequestData($requestPayload['TransactionRequest']['requestData']['data']);
        $url = $requestPayloadData['cburl'];

        $B2CCallbackResponse = CoreUtils::curlPost($url, $formattedCallbackData);

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " B2CCallbackResponse : " . $B2CCallbackResponse,$url);

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return $B2CCallbackResponse;

    }


    /**
     *  Processed overall response with passed status and message
     *
     * @param $status
     * @param $message
     * @return array
     */
    public function formatResponse($status, $message)
    {

        $response = array(
            'status' => $status,
            'message' => $message
        );

        return $response;

    }


}