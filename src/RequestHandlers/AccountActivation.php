<?php
/**
 * This class handles account activation functionality
 *
 * PHP Version 5.6.34
 *
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 * Date: 3/16/18
 * Time: 10:37 AM
 */

class AccountActivation
{
    /**
     * The log class instance.
     *
     * @var object
     */
    public $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    public $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    public $toCode;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     *  Initializes coreUtils class
     * @var CoreUtils
     */
    public $coreUtils;

    /**
     * Saves an instance of the received payload
     * @var data
     */
    public $data;

    /**
     *  Saves an instance of requestHeader in payload
     * @var requestHeader
     */
    public $requestHeader;

    /**
     * Saves an instance of requestBody in payload
     * @var requestBody
     */
    public $requestBody;

    /**
     * Saves an instance of request mobile number
     * @var msisdn
     */
    public $msisdn;

    /**
     * AccountActivation class constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->coreUtils = new CoreUtils();
        $this->fromCode = "SAF";
        $this->toCode = "";
        $this->tat = new BenchMark(session_id());

        $this->initValues();

        coreUtils::loadDBConnection(
            Config::HUB_HOST,
            Config::HUB_DB,
            Config::HUB_PASS,
            Config::HUB_USER,
            'hub'
        );

    }


    /**
     * initialize core classes and functions of the class
     */
    public function initValues()
    {
        $this -> data = CoreUtils::receivePost();

        $this->requestHeader = $this->data['TransactionRequest']['requestHeader'];
        $this->requestBody = SAFKEProcessor::formatRequestData($this->data['TransactionRequest']['requestData']['data']);

        $this->msisdn = $this->requestBody['msisdn'];

        $this -> validatePayload();

        $this->process();

    }


    /**
     * check if there is any instance of empty key values in
     * @return bool
     */
    public function validatePayload()
    {

        //check if any of the headers and the body values are empty
        if (
            empty($this->requestHeader['txid']) || !ctype_alnum($this->requestHeader['txid']) || empty($this->requestHeader['cid']) ||

            empty($this->requestHeader['ln'])   || empty($this->requestHeader['ps'])          || empty($this->requestHeader['dt'])  ||

            empty($this->requestHeader['pid'])  || empty($this->requestHeader['pnm']))
        {
            $this->log->errorLog(Config::DEBUG, $this->msisdn,
                "Validating headers payload : key ".json_encode($this->requestHeader).' was empty'
            );

            // formulate error response

            return $this->formatResponse(SAFKEConfigs::INVALID_REQUEST_PARAMETERS, SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE);

        }
        elseif (substr($this->requestHeader['txid'], 0, 3) != SAFKEConfigs::DEFAULT_SAF_TXID )
        {
            $this->log->errorLog(
                Config::ERROR, $this->msisdn, " Wrong txid :: " . $this->requestHeader['txid'], $this->fromCode, $this->toCode
            );
            // formulate error response
            $this->formatResponse($banks = null, SAFKEConfigs::INVALID_REQUEST_PARAMETERS);
        }
        else {
            if (
                empty($this->requestBody['cburl'])    || empty($this->requestBody['authcode'])    || empty($this->requestBody['msisdn'])  ||

                empty($this->requestBody['Firstname'])|| empty($this->requestBody['Lastname'])    || empty($this->requestBody['Surname']) ||

                empty($this->requestBody['idType'])   || !in_array($this->requestBody['idType'], array('1','2'))   || empty($this->requestBody['idNumber'])    ||

                empty($this->requestBody['IMSI'])     || !is_numeric($this->requestBody['msisdn']) || empty($this->requestBody['LastSimSwap']) ||

                empty($this->requestBody['Remarks'])
            )
            {

                $this->log->errorLog(Config::DEBUG, -1,
                    "Validating body payload : " . json_encode($this->requestBody) . ' was empty'
                );

                // formulate error response
                return $this->formatResponse(
                    SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                    SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE
                );
            }
        }
    }


    /**
     *  Processed the Account Activation Request
     */
    public function process()
    {

        $response = CoreUtils::authenticateRequest($this->requestHeader['ln'],$this->requestHeader['ps'],$this->requestHeader['dt']);


        if ($response == Config::SUCCESS) {

            $response =  $this->logChannelRequest();

            if ($response == true) {

                return $this->formatResponse(SAFKEConfigs::SUCCESS, SAFKEMessages::SUCCESS_MESSAGE);

            } else {

                return $this->formatResponse(SAFKEConfigs::REQUEST_UNSUCCESSFUL, Config::DEFAULT_FAILURE_RESPONSE);

            }
        } else {

            return $this->formatAuthResponse(SAFKEConfigs::AUTHENTICATION_FAILURE, SAFKEMessages::DEFAULT_AUTHENTIFICATION_FAILURE_MESSAGE);

        }

    }


    /**
     * Logs requested payload to channelRequests table
     * @return mixed|null - the channelRequestID if successful,flag duplicate for
     * duplicate merchant transactionID and null if an error occurs
     */
    public function logChannelRequest()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        //get a db connection
        $dblink = new MySQL( Config::HUB_HOST, Config::HUB_USER, Config::HUB_PASS, Config::HUB_DB, Config::HUB_PORT);

        //encode to JSON object
        $payload = json_encode($this->data, JSON_FORCE_OBJECT);

        //log the request
        try {
            // logging into channel requests table
            $query = "INSERT INTO c_channelRequests (MSISDN, accessPoint,message,gatewayID, gatewayUID,"
                . " payload,priority,connectorID, networkID, IMCID, clientSystemID, externalSystemServiceID,activityID,
                clientACKID,overalStatus,statusHistory,statusDescription,serviceDescription,"
                . "expiryDate,appID,dateResponded,dateCreated,dateClosed,MOCost,dateModified,bucketID,lastSend,firstSend
                ,nextSend,numberOfSends )"
                . " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            //prepare the query for execution
            $dt = date("Y-m-d") ;
            //query params
            $params = array(
                'ississiiiiisisisississsssisssi',
                $this->msisdn,          //msisdn
                SAFKEConfigs::DEFAULT_TR_ACCESS_POINT,        //accesspoint
                $this->requestBody['Remarks'],    //message
                SAFKEConfigs::DEFAULT_GATEWAYID,                                    //gateway id
                $this ->requestHeader['txid'],                    //gatewayuid
                $payload,                                        //payload
                SAFKEConfigs::DEFAULT_PRIORITY,                                              //priority
                SAFKEConfigs::DEFAULT_CONNECTORID,                                              //connectorID
                SAFKEConfigs::SAFARICOM_NETWORK_ID,                   //network id
                SAFKEConfigs::IMC_ID,                                 //imcid
                Config::ACTIVATION_CLIENT_SYSTEM_ID,                                              //clientSystemID
                SAFKEConfigs::DEFAULT_EXTERNAL_SYSTEM_SERVICE_ID,
                //externalsystemserviceid
                SAFKEConfigs::DEFAULT_ACTIVITY_ID,                                              //activity id
                SAFKEConfigs::DEFAULT_CLIENT_ACKID,                                         //clientACKID
                StatusCodes::PENDING_STATUS,                                              //overalHistory
                $dt,                    //statusHistory
                SAFKEConfigs::DEFAULT_STATUS_DESCRIPTION,                                          //statusDescription
                SAFKEConfigs::DEFAULT_SERVICE_DESCRIPTION,                                          //serviceDesc
                $dt,                 //expireyDate
                SAFKEConfigs::DEFAULT_APP_ID,                                  //appID
                $dt,         //dateresponded
                $dt,         //dateCreated
                $dt,       //dateClosed
                SAFKEConfigs::DEFAULT_MOCOST,                          //mocost
                $dt,     //dateModified
                SAFKEConfigs::DEFAULT_BUCKET_ID,                         //bucketid
                $dt,      //lastsend
                $dt,      //firstsend
                $dt,      //nextsend
                SAFKEConfigs::DEFAULT_NUMBER_OF_SENDS                          //numberof sends
            );

            //log the query after formatting it
            $this->log->debugLog(Config::DEBUG, $this->msisdn, "logChannelRequest query: "
                . CoreUtils::formatQueryForLogging($query, $params), $this->msisdn);

            $result = DatabaseUtilities::executeQuery($dblink->mysqli, $query, $params);

            //get the lastInsertID
            $lastInsertID = $dblink->mysqli->insert_id;

            //log the request
            if ($lastInsertID != null) {

                $this->log->infoLog(Config::INFO, $this->msisdn, "Successfully Logged, channelRequestID: " . $lastInsertID, $this->msisdn);
                return true;

            } else {
                $this->log->info(Config::INFO, $this->msisdn, "Did not get the last insert ID ");
            }

            $dblink->mysqli->close();
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return $lastInsertID;

        } catch (SQLException $ex) {
            $this->log->errorLog(
                Config::DEBUG,
                -1,
                "Error in logging the channel request: " .$ex
            );
            // formulate error response
            $this->formatResponse(
                Config::INTERNAL_SERVER_ERROR,
                Config::INTERNAL_SERVER_ERROR_MESSAGE
            );

            //some other error occurred
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return null;

        } catch (Exception $ex) {

            $this->log->error(Config::ERROR, $this->msisdn, "Error persisting to channelRequests. Mysql ErrorCode: "
                . $ex->getCode() . " Error: " . $ex->getMessage(), $this->msisdn);

            //some other error occurred
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            // formulate error response
            $this->formatResponse(
                SAFKEConfigs::INTERNAL_SERVER_ERROR,
                SAFKEMessages::INTERNAL_SERVER_ERROR_MESSAGE
            );

            return null;

        }
    }


    /**
     *
     *  Format authentication response
     * @param $statusCode
     * @param $message
     *
     */
    public function formatAuthResponse($statusCode, $message)
    {

        $response = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => $this->requestHeader['txid'],
                    "dt" => $this->requestHeader['dt']
                ),
                "responseData" => array(
                    "rcode" => $statusCode,
                    "rtext" => $message
                )
            )
        );

        $this->coreUtils ->renderResponse($response);

    }

    /**
     *  Formats the overall response payload
     * @param $statusCode
     * @param $message
     */
    public function formatResponse($statusCode, $message)
    {

        $response = array(
            "TransactionResponse" => array(
                "header" => array(
                    "txid" => $this->requestHeader['txid'],
                    "dt" => $this->requestHeader['dt']
                ),
                "responseData" => array(
                    "rcode" => $statusCode,
                    "rtext" => $message
                )
            )
        );

        $this->coreUtils ->renderResponse($response);

    }


}