<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * WalletRequestWrapper class -- handles the wallet request functionality
 *
 * @author ericwanjau
 * PHP Version 5.4.13
 *
 * @category Core
 * @package  Syndication
 * @author    Eric Wanjau <eric.wanjau@cellulant.com>
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 */
class WalletRequestWrapperDB
{

    /** other params * */
    private $msisdn;

    private $channelRequestID;

    private $bankID;

    /***
     * @var CoreAppLogger the log class
     */

    public $log;

    /***
     * @var BenchMark for analyzing processing times per function
     */

    private $tat;

    /***
     * @var CoreUtils for extending core functions of the application
     */
    private $coreUtils;

    /***
     * @var Stores the received channelRequest data
     */
    private $channelRequestPayload;
    private $channelRequestPayloadData;

    /**
     * To hold the data for payload which will be received with a queue name
     * @var $queue
     */
    private $queue;

    /**
     * WalletRequestWrapper constructor.
     *
     *   Instantiated CoreAppLogger, BenchMark and CoreUtils Classes
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->tat = new BenchMark(session_id());
        $this->coreUtils=new CoreUtils;
        $this->initValues();
    }


    public function getRequestPayload(){
        if(empty($_POST)){
            return CoreUtils::receivePost();
        }else{
            return $_POST;
        }
    }

    /**
     * Init Values
     *  Handles the basic request payload decoding process
     */
    public function initValues()
    {

        $payload=  $this->getRequestPayload();
        $this->msisdn = (isset($payload['msisdn']) ? $payload['msisdn'] : NULL);

        $this->channelRequestID = (isset($payload['channelRequestID']) ? $payload['channelRequestID'] : NULL);
        $this->channelRequestPayload =  $payload['payload'];

        $this->queue = (isset($payload['queueName']) ? $payload['queueName'    ] : NULL);
        $this -> channelRequestPayloadData = SAFKEProcessor::formatRequestData($this->channelRequestPayload['TransactionRequest']['requestData']['data']);

    }


    /**
     *  Handles the Sequence flow of the application
     *
     * @return array
     */
    public function process()
    {

        /**
         * get the bankID from payload
         */
        $this->getBankFromPayload();

        /**
         * get bank code from clientConfigs class
         */
        $bankCode = $this->getBankCode();

        /**
         * get associated bank wallet configs from walletConfigs class
         */
        $bankWalletConfigs = $this->fetchWalletConfigs($bankCode);

        /**
         * fetch customer data for the provided MSISDN number
         */
        $fetchedCustomerData = $this->fetchCustomerData($bankWalletConfigs);

        if (count($fetchedCustomerData) > 0) {

            /**
             * proceed to invoke pin prompt api
             */
            $this -> invokePinPromptApi();

            $response = json_encode($fetchedCustomerData);

            $this->logChannelRequest();

            return $this->formulateResponse(StatusCodes::PIN_PROMPT_INVOKED_SUCCESFULLY, $response);

        } else {

            /**
             * to invoke the pin prompt menu
             */
            $contactNumber = ClientConfigs::$clientData[$this->bankID]['contactNumber'];
            $customerMessage = SAFKEProcessor::formulateBlockedAccountMessage($contactNumber);

            $this->invokeInvalidAccountPromt($customerMessage);

            /**
             * return error code three for failure
             */
            $this -> formulateResponse(StatusCodes::FETCH_CUSTOMER_WALLET_DATA_FAILED, SAFKEMessages::FETCH_CUSTOMER_WALLET_DATA_FAILED);

        }
    }


    /**
     *  Gets BankID from the channelRequest payload and assigns it to the
     *  bankID variable
     *
     * @return array
     */
    public function getBankFromPayload()
    {

        //TODO ---- Format data payload from Safaricom to key value format

        $this->bankID = $this -> channelRequestPayloadData['SenderBankID'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " the bank id from payload is : " . $this->bankID);

        if (!$this->bankID) {

            $response = 'Could not get bank id';

            return $this->formulateResponse($response);

        } else {

            return $this -> channelRequestPayloadData['SenderBankID'];

        }
    }


    /**
     *  Gets the bankCode from ClientConfigs class using bankID provided in the
     *   getBankFromPayload function
     * @return mixed
     */
    public function getBankCode()
    {
        $bankCode = ClientConfigs::$clientData[$this->bankID]['clientCode'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " got the bank code as : " . $bankCode);

        if (!empty($bankCode) && !is_null($bankCode)) {

            return ClientConfigs::$clientData[$this->bankID]['clientCode'];

        } else {

            $this->formulateResponse('Could not fetch bank code');

        }
    }


    /**
     *  Returns walletConfigs of the bank details fetched from WalletConfigs class
     *  fetched using the bankCode returned in getBankCode function
     * @param $bankCode
     * @return mixed
     */
    public function fetchWalletConfigs($bankCode)
    {
        $walletConfigs = WalletConfigs::$bankWallets[$bankCode];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " got the wallet configs : " . json_encode($walletConfigs));

        if (!empty($walletConfigs) && !is_null($walletConfigs)) {

            return WalletConfigs::$bankWallets[$bankCode];

        } else {

            $statusCode = Config::OVERALSTATUS_FAILURE_CODE;
            $response = 'Could not get bank code';

            $this->formulateResponse($statusCode, $response);

        }
    }


    /**
     *  invokes the fetchCustomerData api that fetched wallet details of the client
     *  from the passed bankDetails.
     * @param $bankDetails
     * @return array
     */
    public function fetchCustomerData($bankDetails)
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $url = $bankDetails['fetchCustomerDataParams']['apiUrl'];


        $username = $bankDetails['fetchCustomerDataParams']['apiCredentials']['username'];
        $password = $bankDetails['fetchCustomerDataParams']['apiCredentials']['password'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " username is : " .$username .' and password is '.$this->log->printArray($password));

        $fetchCustomerDataHelper = new FetchCustomerDataHelper();

        $fetchCustomerDataHelperResponse = $fetchCustomerDataHelper -> queryWallet(
            $this->msisdn,
            $username,
            $password,
            $url,
            $this->log
        );

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return $fetchCustomerDataHelperResponse;
    }


    /**
     *  A method to do a financial institution to mpesa prompt
     */
    public function invokePinPromptApi()
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $menuString  = $this -> formatPinValidationMessage();

        $payload = array(

            "MSISDN" => $this->msisdn, //mandatory
            "clientCode" => SAFKEConfigs::DEFAULT_CLIENT_CODE,
            "callBackUrl" => SAFKEConfigs::DEFAULT_SAFKE_PINPROMT_HANDLECALLBACK, //optional
            "menuString" =>  $menuString, //optional
            "extraData" => array(
                'authcode'=> $this->channelRequestPayload['TransactionRequest']['requestHeader']['authcode'],
                'txid'=> $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid'],
                'pid'=> $this->channelRequestPayload['TransactionRequest']['requestHeader']['pid'],
                'encparam'=>  SAFKEProcessor::getBlowfishKey(
                    $this->msisdn,
                    $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid']
                ), // SAFKEConfigs::BLOW_FISH_KEY,
                'iv'=> SAFKEConfigs::IV,
                'enc' => SAFKEConfigs::ENC
            ),
            "cellulantKey" => SAFKEConfigs::DEFAULT_ACCESS_CODE,
        );

        $curlResponse = $this->coreUtils->curlPost(SAFKEConfigs::DEFAULT_SAFKE_PINPROMPT_URL, $payload);

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " Pin Prompt Response :" . $curlResponse);

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        if ($rcode['statusCode'] == SAFKEConfigs::SUCCESS) {

            return true;

        } else {

            $this->log->errorLog(Config::ERROR, -1, "Failed on invoking the pin prompt action:" . $curlResponse);

            return $this->formulateResponse(StatusCodes::PIN_PROMPT_INVOKED_FAILED, Config::DEFAULT_FAILURE_RESPONSE);

        }
    }


    /**
     * Formats a pin validation message with parameters inputs
     * @return mixed
     */
    public function formatPinValidationMessage()
    {

        $payload = array(
            "amount" => $this -> channelRequestPayloadData['amount'],
            "bank" => ClientConfigs::$clientData[$this->bankID]['clientName'],
            "msisdn" =>  $this -> channelRequestPayloadData['ReceiverIdentifier'],
            "charge" =>  ClientConfigs::$clientData[$this->bankID]['B2CCharge'],
        );

        $formattedResponse = SAFKEProcessor::formulateMBankingPinPrompt($payload);

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " formatted response  : " . $formattedResponse);

        return $formattedResponse;
    }


    /**
     * @param  $message
     *  A method to invoke a push menu prompt for invalid and blocked accounts
     */
    public function invokeInvalidAccountPromt($message)
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);
        $dt= date('Ymdhis');
        $header = array(
            "txid" => $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid'],
            "cid" => SAFKEConfigs::DEFAULT_COMMAND_ID,
            "ln" => Config::OUTGOING_AUTH_USERNAME,

            /**
             * Default Password
             */
            "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD, $dt),

            /**
             *  user $dt variable
             */
            "dt" => $dt,

            "pid" =>$this->channelRequestPayload['TransactionRequest']['requestHeader']['pid'],

            /**
             * check the docs on whether pid will be sent from api else add a DEFAULT Pid param in config
             */
            "pnm" => SAFKEConfigs::DEFAULT_THIRD_PARTY_NAME
        );

        $data = array(

            "cburl" => SAFKEConfigs::DEFAULT_SAFKE_ACCOUNT_SELECTION_HANDLECALLBACK,

            /**
             * $cburl, //if the callback url is not specified we will use a config  which we have defined
             * eg http://localhost/pushMenus/PinPromptMenu/handleCallback
             */
            "authcode" => $this->channelRequestPayload['TransactionRequest']['requestHeader']['authcode'],
            "msisdn" => $this->msisdn,
            "CustomerPrompt" => $message,

            /**
             * If the menu is not specified in payload call a function to generate one
             */
            "enc" => SAFKEConfigs::ENC_DEFAULT, //enc
        );

        $generatedData = SAFKEProcessor:: generateSafKeyValueFormat($data);

        $payload = array(
            "AuthRequest" => array(
                "requestHeader" => $header,
                "requestData" => array(
                    "data" => $generatedData
                )
            )
        );

        $curlResponse = $this->coreUtils->curlPost(SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT, $payload, $header);

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " Push menu Prompt Response for invalid/blocked account :" . $curlResponse);

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        if ($rcode['responseData']['rcode'] == SAFKEConfigs::SUCCESS) {

            return true;

        } else {

            $this->log->errorLog(Config::ERROR, -1, "Failed invoking the push menu :: invalid or blocked account:" . $curlResponse);

            return $this->formulateResponse(StatusCodes::PIN_PROMPT_INVOKED_FAILED, Config::DEFAULT_FAILURE_RESPONSE);
        }
    }


    /**
     * Logs requested payload to channelRequests table
     * @return mixed|null - the channelRequestID if successful,flag duplicate for
     * duplicate merchant transactionID and null if an error occurs
     */
    public function logChannelRequest()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        //get a db connection
        $dblink = new MySQL( Config::HUB_HOST, Config::HUB_USER, Config::HUB_PASS, Config::HUB_DB, Config::HUB_PORT);

        //encode to JSON object
        $payload = json_encode($this -> channelRequestPayload, JSON_FORCE_OBJECT);

        //log the request
        try {
            // logging into channel requests table
            $query = "INSERT INTO c_channelRequests (MSISDN, accessPoint,message,gatewayID, gatewayUID,"
                . " payload,priority,connectorID, networkID, IMCID, clientSystemID, externalSystemServiceID,activityID,
                clientACKID,overalStatus,statusHistory,statusDescription,serviceDescription,"
                . "expiryDate,appID,dateResponded,dateCreated,dateClosed,MOCost,dateModified,bucketID,lastSend,firstSend
                ,nextSend,numberOfSends )"
                . " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            //prepare the query for execution
            $dt = date("Y-m-d") ;
            //query params
            $params = array(
                'ississiiiiisisisississsssisssi',
                $this->msisdn,          //msisdn
                SAFKEConfigs::DEFAULT_TR_ACCESS_POINT,        //accesspoint
                SAFKEMessages::DEFAULT_TR_MESSAGE,    //message
                SAFKEConfigs::DEFAULT_GATEWAYID,                                    //gateway id
                $this -> channelRequestPayload['TransactionRequest']['requestHeader']['txid'],                    //gatewayuid
                $payload,                                        //payload
                SAFKEConfigs::DEFAULT_PRIORITY,                                              //priority
                SAFKEConfigs::DEFAULT_CONNECTORID,                                              //connectorID
                SAFKEConfigs::SAFARICOM_NETWORK_ID,                   //network id
                SAFKEConfigs::IMC_ID,                                 //imcid
                Config::ACTIVATION_CLIENT_SYSTEM_ID,                                              //clientSystemID
                SAFKEConfigs::DEFAULT_EXTERNAL_SYSTEM_SERVICE_ID,
                //externalsystemserviceid
                SAFKEConfigs::DEFAULT_ACTIVITY_ID,                                              //activity id
                SAFKEConfigs::DEFAULT_CLIENT_ACKID,                                         //clientACKID
                StatusCodes::PENDING_STATUS,                                              //overalHistory
                $dt,                    //statusHistory
                SAFKEConfigs::DEFAULT_STATUS_DESCRIPTION,                                          //statusDescription
                SAFKEConfigs::DEFAULT_SERVICE_DESCRIPTION,                                          //serviceDesc
                $dt,                 //expireyDate
                SAFKEConfigs::DEFAULT_APP_ID,                                  //appID
                $dt,         //dateresponded
                $dt,         //dateCreated
                $dt,       //dateClosed
                SAFKEConfigs::DEFAULT_MOCOST,                          //mocost
                $dt,     //dateModified
                SAFKEConfigs::DEFAULT_BUCKET_ID,                         //bucketid
                $dt,      //lastsend
                $dt,      //firstsend
                $dt,      //nextsend
                SAFKEConfigs::DEFAULT_NUMBER_OF_SENDS                          //numberof sends
            );

            //log the query after formatting it
            $this->log->debugLog(Config::DEBUG, $this->msisdn, "logChannelRequest query: "
                . CoreUtils::formatQueryForLogging($query, $params), $this->msisdn);

            $result = DatabaseUtilities::executeQuery($dblink->mysqli, $query, $params);

            //get the lastInsertID
            $lastInsertID = $dblink->mysqli->insert_id;
            $this->channelRequestID = $lastInsertID;

            //log the request
            if ($lastInsertID != null) {

                $this->log->infoLog(Config::INFO, $this->msisdn, "Successfully Logged, channelRequestID: " . $lastInsertID, $this->msisdn);
                return true;

            } else {
                $this->log->info(Config::INFO, $this->msisdn, "Did not get the last insert ID ");
            }

            $dblink->mysqli->close();
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return $lastInsertID;

        } catch (SQLException $ex) {

            $this->log->errorLog(Config::DEBUG, -1, "Error in logging the channel request: " .$ex);

            $this->formatResponse(Config::INTERNAL_SERVER_ERROR, Config::INTERNAL_SERVER_ERROR_MESSAGE , true);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return null;

        } catch (Exception $ex) {

            $this->log->error(Config::ERROR, $this->msisdn, "Error persisting to channelRequests. Mysql ErrorCode: "
                . $ex->getCode() . " Error: " . $ex->getMessage(), $this->msisdn);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            $this->formatResponse(Config::INTERNAL_SERVER_ERROR, Config::INTERNAL_SERVER_ERROR_MESSAGE, true);

            return null;

        }
    }


    /**
     *  Formulates the response to be returned to the daemon
     * @param $response
     * @return array
     */
    public function formulateResponse($statusCode, $StatusMessage, $status = null)
    {

        if (!empty($this->queue) && $status == null) {

            $this->logChannelRequest();

        }

        return array(
            'code' => $statusCode,
            'description' => $StatusMessage
        );
    }


}
