<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 07/03/18
 * Time: 16:20
 *  A function to process the payload sent back by safaricom to the authussdcallback url
 */
include_once ('./lib/IXR_Library.php');

class AuthCallback
{
    /**
     *  The log class instance
     * @var
     */
    public $log;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * Constructor.
     */

    public $coreUtils;

    /**
     * this holds the data of the body of the payload
     * @var $requestBody
     */
    public $requestBody;

    /**
     * this holds the data of the payload for header values
     * @var $requestHeader
     */
    public $requestHeader;

    /**
     * this holds the payload received
     * @var $payload
     */
    public $payload;

    /**
     * This holds the value for the bank id
     * @var $bankID
     */
    public $bankID;

    /**
     * this holds the value for the channel request id
     * @var $channelRequestID
     */
    public $channelRequestID;

    /**
     * this holds the array for the customer accounts
     * @var $customerAccounts
     */
    public $customerAccounts;

    /**
     * this holds the array for the customer accounts fetched from redis
     * @var $customerAccounts
     */
    public $customerAccountsRedis;

    /**
     * this is to hold the mobile number from the payload received
     * @var $msisdn
     */
    public $msisdn;

    /**
     * to hold the data to show where the request is heading to
     * @var $toCode
     */
    public $toCode;

    /**
     * to hold the data to show where the request is originating from
     * @var $fromCode
     */
    public $fromCode;

    /**
     * used to hold the data being received from the post function
     * @var $data
     */
    public $data;

    /**
     * used when initialising the safKEProcessor class
     * @var $safKEProcessor
     */
    public $safKEProcessor;
    /**
     * @var
     *  to authenticate the cellulant key
     */
    public $auth;

    /**
     * @var
     * used to initialise the redis controller
     */
    public $redis;

    /**
     * @var
     * This holds the redis key
     */
    public $redisKey;

    /**
     * AuthCallback constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->tat = new BenchMark(session_id());
        $this->coreUtils = new CoreUtils();
        $this->auth = new Authentication();
        $this->toCode = "";
        $this->fromCode = "";
        $this->initValues();

    }

    /**
     *  initialise your process
     */
    public function initValues()
    {
        $this->redis = new RedisController();
        $this->safKEProcessor = new SAFKEProcessor();
        $this->data = CoreUtils::receivePost();
        $redisData = $this->data;
        $this->requestBody= SAFKEProcessor::formatRequestData($redisData['TransactionRequest']['requestData']['data']);
        $this->requestHeader = $redisData['TransactionRequest']['requestHeader'];
        $this->validatePayload();
        $this->msisdn = $this->requestBody['msisdn'];
        $this->redisKey = Config::REDIS_CHANNEL_REQUEST_KEY.Config::ACTIVATION_CLIENT_SYSTEM_ID.".".$this->requestHeader['txid'];
        //to handle the authentication of the password
        $authentication = $this->coreUtils->authenticateRequest(
            $this->requestHeader['ln'],
            $this->requestHeader['ps'],
            $this->requestHeader['dt']
        );
        $redisResults = $this->fetchRedisChannelRequestLogs($this->redisKey);
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "result resis" . json_encode($redisResults),
            $this->fromCode,
            $this->toCode
        );

        if ($authentication != SAFKEConfigs::SUCCESS) {
            $this->formatResponse(
                SAFKEConfigs::AUTHENTICATION_FAILURE,
                SAFKEMessages::DEFAULT_AUTHENTIFICATION_FAILURE_MESSAGE
            );
        }
        $newPayload = null;
        if ($this->requestBody['resCode']
            != SAFKEConfigs::SUCCESS) {
            $fieldsValuesArray = array(
                'overallStatus' => Config::OVERALSTATUS_FAILURE_CODE,
                'resCode' => $this->requestBody['resCode'],
                'overallStatusMessage' => Config::OVERALSTATUS_FAILURE_MSG
            );

            $this -> redis -> setKeyMultipleValues($this->redisKey, $fieldsValuesArray);

            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Customer did not input pin" . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );
            exit();
        }

        if ($redisResults['overallStatus'] == Config::OVERALSTATUS_PROCESSED_TRANSACTION_CODE) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "The customer transaction has already been processed" . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );
            exit();
        }

        $this->customerAccountsRedis = json_decode($redisResults['statusDescription'], true);
        $this->fromCode = "SAFKE";
        $this->toCode = "WALLET";
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Customer accounts" . json_encode($this->customerAccountsRedis),
            $this->fromCode,
            $this->toCode
        );
    }

    /**
     * Simple method to check whether any of the key values sent in the header are not empty
     */
    public function validatePayload()
    {
        //check if the headers and body values are empty
        if (empty($this->requestHeader['txid']) ||
            empty($this->requestHeader['cid']) ||
            empty($this->requestHeader['ln']) ||
            empty($this->requestHeader['ps']) ||
            empty($this->requestHeader['dt'])  ||
            empty($this->requestHeader['pnm']) ||
            empty($this->requestHeader['pid']) ||
            empty($this->requestBody['EncData']) ||
            empty($this->requestBody['cburl']) ||
            empty($this->requestBody['authcode']) ||
            empty($this->requestBody['msisdn'])) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Validating the payload " . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );

            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE
            );
        }
    }


    /**
     * method to process the pin prompt ussd callback
     */
    public function processPinPromptCallback()
    {

        //TODO remove the or condition
        if (count($this->customerAccountsRedis) ==  1) {
            $this->formulatePayload($this->customerAccountsRedis[1]);
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Incoming payload" . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );
            //methods to be invoked
            $this->sendViaXMLRPC();
        } elseif ($this->customerAccountsRedis == null) {
            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                Responses::NO_ACCOUNTS_MESSAGE
            );
        } elseif (count($this->customerAccountsRedis) > 1) {
            $this->invokeRedisAccountSelectionPrompt();
        }
    }

    /**
     * method to process the account selection function
     */
    public function processAccountSelection()
    {

        $channelRequestData = $this->fetchRedisChannelRequestLogs($this->redisKey);
        $accountKeys = array();
        foreach ($this->customerAccountsRedis as $key => $value) {
            array_push($accountKeys, $key);
        }

        // small check to see if the account selected is valid
        if (!in_array($this->requestBody['EncData'], $accountKeys)) {
            //check to see the number of retrys done for incorect account selection choice
            if ($channelRequestData['accountSelectionRetry'] <= Config::MAX_ACCOUNT_SELECTION_RETRY) {
                $this->invokeRedisRetryAccountSelectionPrompt();
            } else {
                $this->log->debugLog(
                    Config::ERROR,
                    $this->msisdn,
                    "Max account selection retries reached",
                    $this->fromCode,
                    $this->toCode
                );
                $this->formatResponse(
                    SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                    Responses::NO_ACCOUNTS_MESSAGE
                );
            }
        } else {
            //end point to be shared when invoking the customer to choose account as the cburl
            $this->formulatePayload($this->customerAccountsRedis[$this->requestBody['EncData']]);
            $this->sendViaXMLRPC();
        }
    }

    /**
     * Fetch the payload for the channel request
     * @param $txid
     * @return array|bool|mixed|null|ORM
     */
    public function fetchChannelRequestLogs($txid)
    {
        $this->safKEProcessor = new SAFKEProcessor();
        $channelRequestLog = $this->safKEProcessor->getChannelRequestData($txid, $this->msisdn);
        return $channelRequestLog;
    }

    public function fetchRedisChannelRequestLogs($redisKey)
    {

        $channelRequestLog = $this->redis->getKeyValue($redisKey);
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Channel request data stored in redis" . $redisKey,
            $this->fromCode,
            $this->toCode
        );
        return $channelRequestLog;
    }

    /**
     * simple method to formulate our payload to send to wallet
     */
    public function formulatePayload($accountDetails)
    {

        $channelRequestDataRedis = $this->fetchRedisChannelRequestLogs($this->redisKey);
        $channelRequestLogRedis = json_decode($channelRequestDataRedis['payload'], true);
        $data = SAFKEProcessor::formatRequestData($channelRequestLogRedis['TransactionRequest']['requestData']['data']);
        $this->bankID = $data['SenderBankID'];
        $this->statusDescription = json_decode($channelRequestDataRedis['statusDescription'], true);
        //fetch the Wallet URL details
        $bankCode = $this->getBankCode();
        $walletConfigs = $this->fetchWalletConfigs($bankCode);

        if (!empty($channelRequestDataRedis['pin'])) {
            $pin = $channelRequestDataRedis['pin'];
        } else {
            $pin = $this->requestBody['EncData'];
        }

        $pin=trim($pin);
        $encryptPin = $this->encryptPin($pin, $walletConfigs['walletDebitRequest']['apiParameters']['B2CIMCREQUESTID']);
        $this->payload = array(
            "serviceID"=> $walletConfigs['walletDebitRequest']['apiParameters']['B2CSERVICEID'], //configs ya wallet b2c
            "flavour" =>  $walletConfigs['walletDebitRequest']['apiParameters']['B2CFLAVOUR'], //configs wallet b2c
            "pin" => $encryptPin,
            //configs per wallet(EncrptionIMCRequestID)
            "accountAlias" =>  $accountDetails['accountAlias'],  // services description column
            "amount" =>  $data['amount'],
            "merchantCode" =>  $walletConfigs['walletDebitRequest']['apiParameters']['B2CMERCHANTCODE'],
            //config wallet b2c
            "CBSID" =>  $walletConfigs['walletDebitRequest']['apiParameters']['B2CCBSID'],  //wallete config b2c
            "enroll" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CENROLL'],  // wallete config
            "columnD" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CCOLUMND'],  // null wallete
            "columnA" => CoreUtils::validateMSISDN($data['ReceiverIdentifier']), //RECIPIENT NUMBER
            //recipients  TODO to verify on wether column A to be account number or mobile numbre
            "columnB" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CCOLUMNB'], //null wallete
            "columnC" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CCOLUMNC'], //null wallete
            "accountID" => $accountDetails['accountID'] //service description column
        );
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "The payload " . json_encode($this->data).' bank id' . $this->bankID,
            $this->fromCode,
            $this->toCode
        );
    }


    /**
     * method to decrypt the pin sent and encrypt again with wallet encryption standard
     * @param $pin
     * @param $imcrequestID
     * @return bool|string
     */
    public function encryptPin($pin, $imcrequestID)
    {
        $encryption1 = new BlowFishEncryption();
        //$decodedCipherText = $encryption1->SimpleDecrypt($pin);
        $decodedCipherText = $encryption1->simpleDecryptWithDynamicKey(
            $pin,
            SAFKEProcessor::getBlowfishKey(
                $this->msisdn,
                $this->requestHeader['txid']
            )
        );

        $decodedCipherText = trim($decodedCipherText);
        $encryption = new Encryption();
        $encryptedInput = $encryption->encryptPin(
            $decodedCipherText,
            1
        );

        $this->log->debugLog(
            Config::DEBUG,
            $this->msisdn,
            " walletPin ".$encryptedInput,
            $this->fromCode,
            $this->toCode
        );
        //    echo $decodedCipherText;
        //    exit();
        return $encryptedInput;
    }

    /**
     * this is a method to fetch the bank code
     * @return mixed
     */
    public function getBankCode()
    {
        $bankCode = ClientConfigs::$clientData[$this->bankID]['clientCode'];

        if (!empty($bankCode) && !is_null($bankCode)) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Bank code " . json_encode($bankCode). 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            return ClientConfigs::$clientData[$this->bankID]['clientCode'];
        } else {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Failed to get the bank code for bank id" .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            $this->formatResponse(
                StatusCodes::FAILURE,
                Config::BANK_CODE_ERROR_MESSAGE
            );
        }
    }

    /**
     * this is the method to handle fetching of wallete config data
     * @param $bankCode
     * @return mixed
     */
    public function fetchWalletConfigs($bankCode)
    {
        $walletConfigs = WalletConfigs::$bankWallets[$bankCode];

        if (!empty($walletConfigs) && !is_null($walletConfigs)) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Wallet data got" . json_encode($walletConfigs) . 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            return WalletConfigs::$bankWallets[$bankCode];
        } else {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Failed to get wallet configs for bank id" . $this->bankID,
                $this->fromCode,
                $this->toCode
            );
            $this->formatResponse(
                StatusCodes::FAILURE,
                Config::API_FETCH_ERROR_MESSAGE
            );
        }
    }

    /**
     * this is a method to handle the pushing of data to wallete
     */
    public function sendViaXMLRPC()
    {
        $this->logChannelRequest();
        try {
            $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            //fetch the Wallet URL details
            $bankCode = $this->getBankCode();
            $walletConfigs = $this->fetchWalletConfigs($bankCode);

            $apiUrl = $walletConfigs['walletDebitRequest']['apiUrl'];
            $apiFunction = $walletConfigs['walletDebitRequest']['apiFunction'];

            //convert array into XML format
            //formulate xml payload.
            $request_xml = '';
            $request_xml = '<Payload>';
            foreach ($this->payload as $key => $value) {
                $request_xml .='<' . $key . '>' . $value . '</' . $key . '>';
            }

            $request_xml .= '</Payload>';

            $payload = $request_xml;
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Newly formatted payload" .$payload . 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );

            $credentials = array(
                'cloudUser' => $walletConfigs['walletDebitRequest']['apiCredentials']['username'],
                'cloudPass' => $walletConfigs['walletDebitRequest']['apiCredentials']['password'],
            );

            $dt = date('Y-m-d H:i:s');

            //define cloud packet data
            $cloudPacket = array(
                "MSISDN" => $this->requestBody['msisdn'],
                "destination" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CDESTINATION'],
                //wallete config
                "IMCID" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CIMCID'], // wallete config
                "channelRequestID" => $this->channelRequestID,
                "networkID" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CNETWORKID'],  //wallete config
                "cloudDateReceived" => $dt,
                "payload" => base64_encode($payload),
                "imcRequestID" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CIMCREQUESTID'],
                //wallete config
                "requestMode" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CREQUESTMODE'],  //
            );

            //package our data
            $params = array(
                'credentials' => $credentials,
                'cloudPacket' => $cloudPacket,
            );
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Data being pushed to wallet" . json_encode($params) . 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            //make API call
            $client = new IXR_Client($apiUrl);
            if (!$client->query($apiFunction, $params)) {
                $this->log->debugLog(
                    Config::INFO,
                    $this->msisdn,
                    "IXR client error occured" . $client->getErrorCode() . 'error message:'
                    .$client->getErrorMessage().
                    'wallet url: '. $apiUrl,
                    $this->fromCode,
                    $this->toCode
                );
                $this->formatResponse(
                    SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                    SAFKEMessages::DEFAULT_FAILURE_RESPONSE
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            }

            //get response
            $result = $client->getResponse();
            $data = json_decode($result, true);
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Wallet response" . json_encode($data) . 'wallet url' .$apiUrl,
                $this->fromCode,
                $this->toCode
            );
            $response = array();

            // return $data;
            if ($data['STAT_CODE'] != StatusCodes::WALLET_SUCCESS_STATUS_CODE) {
                $this->formatResponse(
                    SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                    SAFKEMessages::DEFAULT_FAILURE_RESPONSE
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            } else {
                $channelRequestDataRedis = $this->fetchRedisChannelRequestLogs($this->redisKey);
                $channelRequestLog = json_decode($channelRequestDataRedis['payload'], true);

                if (!empty($channelRequestDataRedis['accountSelectionRetry'])
                    && !empty($channelRequestDataRedis['pin'])
                ) {
                    $accountSelection = $channelRequestDataRedis['accountSelectionRetry'];
                    $accountSelectionRetryArray = array(
                        'accountSelectionRetry' => $accountSelection
                    );
                    $pin = '****';
                    $pinArray = array(
                        'pin' => $pin
                    );
                }

                $fieldsValuesArray = array(
                    'pin' => $pin,
                    'accountSelectionRetry' => $accountSelection,
                    'overallStatus' => Config::OVERALSTATUS_PROCESSED_TRANSACTION_CODE
                );

                $this -> redis -> setKeyMultipleValues($this->redisKey, $fieldsValuesArray);
                $this-> redis-> expireRedisRecord($this->redisKey, Config::REDIS_TIMEOUT);
                $newPayload = array_merge($channelRequestLog, $pinArray, $accountSelectionRetryArray);
                $this->safKEProcessor->updateChannelRequestOverallStatus(
                    $this->requestBody['resCode'],
                    $this->msisdn,
                    Config::OVERALSTATUS_PROCESSED_TRANSACTION_CODE,
                    Config::OVERALSTATUS_PROCESSED_TRANSACTION_MSG,
                    $this->channelRequestID,
                    $newPayload
                );
                $this->formatResponse(
                    SAFKEConfigs::SUCCESS,
                    SAFKEMessages::SUCCESS_MESSAGE
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            }
        } catch (Exception $exception) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Wallet exception error" . $exception->getMessage() . 'wallet url' .$apiUrl,
                $this->fromCode,
                $this->toCode
            );

            $this->formatResponse(
                SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                SAFKEMessages::DEFAULT_FAILURE_RESPONSE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        }
    }

    /**
     * this is a function to handle the account selection
     */
    public function invokeRedisAccountSelectionPrompt()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        $dt= date('Ymdhis');

        $accountSelection = Config::DEFAULT_ACCOUNT_SELECTION_RETRY;
        $accountSelections = $accountSelection +1;

        $fieldsValuesArray = array(
            'pin' => $this->requestBody['EncData'],
            'accountSelectionRetry' => $accountSelections
        );
        $this -> redis -> setKeyMultipleValues($this->redisKey, $fieldsValuesArray);

        $header = array(
            "txid" => $this->requestHeader['txid'],//
            "cid" => SAFKEConfigs::DEFAULT_COMMAND_ID,//
            "ln" => Config::OUTGOING_AUTH_USERNAME,//
            "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD, $dt),//default-password
            "dt" => $dt,//use $dt variable
            "pid" => $this->requestHeader['pid'],
            //check the docs on ehther pid will be sent from api else add a DEFAULT Pid param in config
            "pnm" => SAFKEConfigs::DEFAULT_THIRD_PARTY_NAME//configs
        );

        $data = array(
            "cburl" => SAFKEConfigs::DEFAULT_SAFKE_ACCOUNT_SELECTION_HANDLECALLBACK,
            //$cburl, //if the callback url is not specified we will use a config  which we have defined/
            // eg http://localhost/pushMenus/PinPromptMenu/handleCallback
            "authcode" => $this->requestBody['authcode'], //
            "msisdn" => $this->requestBody['msisdn'], //
            "CustomerPrompt" => $this->formulateRedisAccountSelectionPrompt(),
            //if the mrnu is not specified in paylosd csll sa function to generate one
            "enc" => SAFKEConfigs::ENC_DEFAULT, //enc
        );

        $generatedData = SAFKEProcessor:: generateSafKeyValueFormat($data);

        $payload = array(
            "AuthRequest" => array(
                "requestHeader" => $header,
                "requestData" => array(
                    "data" => $generatedData
                )
            )
        );

        $curlResponse = $this->coreUtils->curlPost(
            SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT,
            //add a config on SFKE= USSDPUS ENDPOINT
            $payload,
            $header
        );

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Curl response" . json_encode($curlResponse) .
            'for url' .SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT .
            'payload sent' . json_encode($payload),
            $this->fromCode,
            $this->toCode
        );

        if ($rcode['responseData']['rcode'] == SAFKEConfigs::SUCCESS) {
            $this->formatResponse(
                SAFKEConfigs::SUCCESS,
                SAFKEMessages::SUCCESS_MESSAGE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        } else {
            $this->formatResponse(
                SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                SAFKEMessages::DEFAULT_FAILURE_RESPONSE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        }
    }

    /**
     * @return string
     * Used to formulate the message template for an account selection
     */
    public function formulateRedisAccountSelectionPrompt()
    {
        $string = "";
        foreach ($this->customerAccountsRedis as $key => $value) {
            $string .= "\n".$key.' : '.$value['accountAlias'];
        }
        $message = Responses::B2C_ACCOUNT_SELECTION_PROMPT_TEMPLATE.$string;

        return $message;
    }

    /**
     * This is a method for pushing the account selection retry prompt.
     */
    public function invokeRedisRetryAccountSelectionPrompt()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        $dt= date('Ymdhis');
        $channelRequestData = $this->fetchRedisChannelRequestLogs($this->redisKey);
        $accountSelection = $channelRequestData['accountSelectionRetry'];
        $accountSelections = $accountSelection +1;
        $header = array(
            "txid" => $this->requestHeader['txid'],//
            "cid" => SAFKEConfigs::DEFAULT_COMMAND_ID,//
            "ln" => Config::OUTGOING_AUTH_USERNAME,//
            "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD, $dt),//default-password
            "dt" => $dt,//use $dt variable
            "pid" => $this->requestHeader['pid'],
            //check the docs on ehther pid will be sent from api else add a DEFAULT Pid param in config
            "pnm" => SAFKEConfigs::DEFAULT_THIRD_PARTY_NAME//configs
        );

        $data = array(
            "cburl" => SAFKEConfigs::DEFAULT_SAFKE_ACCOUNT_SELECTION_HANDLECALLBACK,
            //$cburl, //if the callback url is not specified we will use a config  which we have defined/
            // eg http://localhost/pushMenus/PinPromptMenu/handleCallback
            "authcode" => $this->requestBody['authcode'], //
            "msisdn" => $this->requestBody['msisdn'], //
            "CustomerPrompt" => $this->formulateRetryAccountSelectionPrompt($accountSelection),
            //if the mrnu is not specified in paylosd csll sa function to generate one
            "enc" => SAFKEConfigs::ENC_DEFAULT, //enc
        );

        $generatedData = SAFKEProcessor:: generateSafKeyValueFormat($data);

        $payload = array(
            "AuthRequest" => array(
                "requestHeader" => $header,
                "requestData" => array(
                    "data" => $generatedData
                )
            )
        );

        $curlResponse = $this->coreUtils->curlPost(
            SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT,
            //add a config on SFKE= USSDPUS ENDPOINT
            $payload,
            $header
        );

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Curl response" . json_encode($curlResponse) .
            'for url' .SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT .
            'payload sent' . json_encode($payload),
            $this->fromCode,
            $this->toCode
        );

        if ($rcode['responseData']['rcode'] == SAFKEConfigs::SUCCESS) {
            $fieldsValuesArray = array(
                'pin' => $channelRequestData['pin'],
                'accountSelectionRetry' => $accountSelections
            );
            $this -> redis -> setKeyMultipleValues($this->redisKey, $fieldsValuesArray);

            $this->formatResponse(
                SAFKEConfigs::SUCCESS,
                SAFKEMessages::SUCCESS_MESSAGE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        } else {
            $this->formatResponse(
                SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                SAFKEMessages::DEFAULT_FAILURE_RESPONSE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        }
    }

    /**
     * @param $data
     * @return string
     * this is a method for formulating the prompt message for an account selection retry
     */
    public function formulateRetryAccountSelectionPrompt($data)
    {
        $remainingRetrys =  Config::MAX_ACCOUNT_SELECTION_RETRY - $data;
        $baseMessage  = Responses::B2C_SECOND_ACCOUNT_SELECTION_PROMPT_TEMPLATE;
        $hashMaps = array("#number#");
        $hashMapValues   = array($remainingRetrys);

        $formattedMessage = str_replace($hashMaps, $hashMapValues, $baseMessage);
        $string = "";
        foreach ($this->customerAccountsRedis as $key => $value) {
            $string .= "\n".$key.' : '.$value['accountAlias'];
        }
        $message = $formattedMessage.$string;

        return $message;
    }


    /**
     * Logs requested payload to channelRequests table before forwarding to
     * MPESA API
     * @param $arrayPayload
     * @param $status - statusCode to log in overallStatus Column
     * @param $statusDescription - description of what the status means
     * @return mixed -the channelRequestID if successful,flag duplicate for
     * duplicate merchant transactionID and null if an error occurs
     */
    public function logChannelRequest()
    {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);

        //encode to JSON object
        $channelRequestDataRedis = $this->fetchRedisChannelRequestLogs($this->redisKey);
        $payload = $channelRequestDataRedis['payload'];
        $p = json_decode($payload, true);
        $remarks = SAFKEProcessor::formatRequestData($p['TransactionRequest']['requestData']['data']);


        //log the request
        try {
            /*
             * We change the query to add a small delay for async requests
             * This allows the menu to end the session
             * gracefully to allow the USSD push to happen.FYI: its not a hack :-)
             */
            coreUtils::loadDBConnection(
                Config::HUB_HOST,
                Config::HUB_DB,
                Config::HUB_PASS,
                Config::HUB_USER,
                'hub'
            );
            //Configure primary keys
            ORM::configure('id_column_overrides', array(
                'c_channelRequests' => 'channelRequestID',
            ));
            $channelRequestLog = ORM::for_table('c_channelRequests', 'hub')->create();

            //formalate our data so we save
            $channelRequestLog->MSISDN = $this->msisdn;
            $channelRequestLog->accessPoint = SAFKEConfigs::DEFAULT_TR_ACCESS_POINT;
            $channelRequestLog->message = $remarks['Remarks'];
            $channelRequestLog->gatewayID = SAFKEConfigs::DEFAULT_GATEWAYID;
            $channelRequestLog->gatewayUID = $this->requestHeader['txid'];
            $channelRequestLog->payload = $payload;
            $channelRequestLog->priority = SAFKEConfigs::DEFAULT_PRIORITY;
            $channelRequestLog->connectorID = SAFKEConfigs::DEFAULT_CONNECTORID;
            $channelRequestLog->networkID = SAFKEConfigs::SAFARICOM_NETWORK_ID;
            $channelRequestLog->IMCID = SAFKEConfigs::IMC_ID;
            $channelRequestLog->clientSystemID = SAFKEConfigs::CLIENT_SYSTEM_ID;
            $channelRequestLog->externalSystemServiceID =  SAFKEConfigs::DEFAULT_EXTERNAL_SYSTEM_SERVICE_ID;
            $channelRequestLog->activityID = SAFKEConfigs::DEFAULT_ACTIVITY_ID;
            $channelRequestLog->clientACKID =  SAFKEConfigs::DEFAULT_CLIENT_ACKID;
            $channelRequestLog->overalStatus = StatusCodes::PENDING_STATUS;
            $channelRequestLog->set_expr('statusHistory', 'NOW()');
            $channelRequestLog->statusDescription = SAFKEConfigs::DEFAULT_STATUS_DESCRIPTION;
            $channelRequestLog->serviceDescription =  SAFKEConfigs::DEFAULT_SERVICE_DESCRIPTION;
            $channelRequestLog->set_expr('expiryDate', SAFKEConfigs::CHANNEL_REQUEST_EXPIRY_PERIOD);
            $channelRequestLog->appID = SAFKEConfigs::DEFAULT_APP_ID;
            $channelRequestLog->set_expr('dateResponded', 'NOW()');
            $channelRequestLog->set_expr('dateCreated', 'NOW()');
            $channelRequestLog->set_expr('dateClosed', 'NOW()');
            $channelRequestLog->MOCost =  SAFKEConfigs::DEFAULT_MOCOST;
            $channelRequestLog->set_expr('dateModified', 'NOW()');
            $channelRequestLog->bucketID = SAFKEConfigs::DEFAULT_BUCKET_ID;
            $channelRequestLog->set_expr('lastSend', 'NOW()');
            $channelRequestLog->set_expr('firstSend', 'NOW()');
            $channelRequestLog->set_expr('nextSend', 'NOW()');
            $channelRequestLog->numberOfSends = SAFKEConfigs::DEFAULT_NUMBER_OF_SENDS;

            if ($channelRequestLog->save()) {
                //Saved request
                $channelRequestLogID = $channelRequestLog->id();
                $this->channelRequestID = $channelRequestLogID;

                $this->log->debugLog(
                    Config::INFO,
                    $this->msisdn,
                    "Channel request id" . $channelRequestLogID,
                    $this->fromCode,
                    $this->toCode
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
                return true;
            } else {
                $this->log->debugLog(
                    Config::ERROR,
                    $this->msisdn,
                    "Error loggin channel request" . json_encode($this->data),
                    $this->fromCode,
                    $this->toCode
                );
                // formulate error response
                $message = Config::INTERNAL_SERVER_ERROR_MESSAGE;
                $this->formatResponse(
                    Config::INTERNAL_SERVER_ERROR,
                    $message
                );
                //some other error occurred
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
                return null;
            }
        } catch (Exception $exception) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Error logging channel request" . $exception->getMessage(),
                $this->fromCode,
                $this->toCode
            );
            // formulate error response
            $message = Config::INTERNAL_SERVER_ERROR_MESSAGE;
            $this->formatResponse(
                Config::INTERNAL_SERVER_ERROR,
                $message
            );
            //some other error occurred
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            return null;
        }
    }

    /**
     * @param $statusCode
     * @param $message
     * Simple method used to format the response
     */
    public function formatResponse($statusCode, $message)
    {
        $dt = date('YmdHis');
        $response = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => $this->requestHeader['txid'],
                    "dt" => $dt
                ),
                "responseData" => array(
                    "rcode" => $statusCode,
                    "rtext" => $message
                )
            )
        );

        $this->coreUtils ->renderResponse($response);
    }
}
