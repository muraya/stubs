<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * WalletRequestWrapper class -- handles the wallet request functionality
 *
 * @author ericwanjau
 * PHP Version 5.4.13
 *
 * @category Core
 * @package  Syndication
 * @author    Eric Wanjau <eric.wanjau@cellulant.com>
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 */
class WalletRequestWrapper
{

    /** other params * */
    private $msisdn;

    private $channelRequestID;

    private $bankID;

    /***
     * @var CoreAppLogger the log class
     */

    public $log;

    /***
     * @var BenchMark for analyzing processing times per function
     */

    private $tat;

    /***
     * @var CoreUtils for extending core functions of the application
     */
    private $coreUtils;

    /***
     * @var Stores the received channelRequest data
     */
    private $channelRequestPayload;
    private $channelRequestPayloadData;

    /**
     * To hold the data for payload which will be received with a queue name
     * @var $queue
     */
    private $queue;

    /**
     * @var RedisController
     *  Holds class instance of RedisController
     */
    private  $redis;

    /**
     * @var string
     * Redis Keyg generated from transaction
     */
    private $redisKey;

    /**
     * WalletRequestWrapper constructor.
     *
     *   Instantiated CoreAppLogger, BenchMark and CoreUtils Classes
     */

    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->tat = new BenchMark(session_id());
        $this->coreUtils = new CoreUtils;

        $this -> redis = new RedisController();

        $this->initValues();
    }

    /**
     * @return array
     *
     */
    public function getRequestPayload(){
        if(empty($_POST)){
            return CoreUtils::receivePost();
        }else{
            return $_POST;
        }
    }

    /**
     * Init Values
     *  Handles the basic request payload decoding process
     */
    public function initValues()
    {

        $payload=  $this->getRequestPayload();
        $this->msisdn = (isset($payload['msisdn']) ? $payload['msisdn'] : NULL);

        $this->channelRequestID = (isset($payload['channelRequestID']) ? $payload['channelRequestID'] : NULL);
        $this->channelRequestPayload =  $payload['payload'];

        $this->queue = (isset($payload['queueName']) ? $payload['queueName'    ] : NULL);
        $this -> channelRequestPayloadData = SAFKEProcessor::formatRequestData($this->channelRequestPayload['TransactionRequest']['requestData']['data']);

    }


    /**
     *  Handles the Sequence flow of the application
     *
     * @return array
     */
    public function process()
    {

        /**
         * get the bankID from payload
         */
        $this->getBankFromPayload();

        /**
         * get bank code from clientConfigs class
         */
        $bankCode = $this->getBankCode();

        /**
         * get associated bank wallet configs from walletConfigs class
         */
        $bankWalletConfigs = $this->fetchWalletConfigs($bankCode);

        /**
         * fetch customer data for the provided MSISDN number
         */
        $fetchedCustomerData = $this->fetchCustomerData($bankWalletConfigs);

        $this->redisKey =
            Config::REDIS_CHANNEL_REQUEST_KEY.
            Config::ACTIVATION_CLIENT_SYSTEM_ID."."
            .$this->channelRequestPayload['TransactionRequest']['requestHeader']['txid'];

        $keyValue = $this->redis->getKeyFieldValue($this->redisKey,'gatewayUID');

        if(empty($keyValue) OR $keyValue != $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid'])
        {
            if (count($fetchedCustomerData) > 0) {

                /**
                 * proceed to invoke pin prompt api
                 */
                $this -> invokePinPromptApi();

                $response = json_encode($fetchedCustomerData);

                $this->saveChannelRequestToRedis(StatusCodes::PIN_PROMPT_INVOKED_SUCCESFULLY, $response);

            } else {

                $this -> log -> debugLog(Config::DEBUG,-1, 'wallet no information '.json_encode($fetchedCustomerData));

                /**
                 * to invoke the pin prompt menu
                 */
                $contactNumber = ClientConfigs::$clientData[$this->bankID]['contactNumber'];
                $customerMessage = SAFKEProcessor::formulateBlockedAccountMessage($contactNumber);

                $this->invokeInvalidAccountPrompt($customerMessage);

                /**
                 * return error code three for failure
                 */
                $this -> saveChannelRequestToRedis(StatusCodes::FETCH_CUSTOMER_WALLET_DATA_FAILED,
                    SAFKEMessages::FETCH_CUSTOMER_WALLET_DATA_FAILED);

            }
        } else {

            $MQPublisher = new MQPublisher();

                $queuePayload = array_merge($this->channelRequestPayload,
                    array('overallStatus' => StatusCodes::STATUS_PUSH_FAILURE,
                        'statusDescription' => Responses::DUPLICATE_TRANSACTION_MESSAGE));

                $this->log->debugLog(Config::DEBUG, $this->msisdn, " Duplicate transaction :: ". $this->redisKey." :: ".json_encode($queuePayload));

                $payloadArray = array(
                'payload' => $queuePayload,
                'queueName' => RabbitMQConfigs::WALLET_WRAPPER_DUPLICATES,
                );

                $MQPublisher->publish($payloadArray, RabbitMQConfigs::WALLET_WRAPPER_DUPLICATESMQ_URL);

               $this -> formulateResponse(StatusCodes::STATUS_PUSH_FAILURE, Responses::DUPLICATE_TRANSACTION_MESSAGE);

        }
    }


    /**
     *  Gets BankID from the channelRequest payload and assigns it to the
     *  bankID variable
     *
     * @return array
     */
    public function getBankFromPayload()
    {

        //TODO ---- Format data payload from Safaricom to key value format

        $this->bankID = $this -> channelRequestPayloadData['SenderBankID'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " the bank id from payload is : " . $this->bankID);

        if (!$this->bankID) {

            $this->saveChannelRequestToRedis(Config::OVERALSTATUS_FAILURE_CODE, 'Could not get bank from payload');

        } else {

            return $this -> channelRequestPayloadData['SenderBankID'];

        }
    }


    /**
     *  Gets the bankCode from ClientConfigs class using bankID provided in the
     *   getBankFromPayload function
     * @return mixed
     */
    public function getBankCode()
    {
        $bankCode = ClientConfigs::$clientData[$this->bankID]['clientCode'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " got the bank code as : " . $bankCode);

        if (!empty($bankCode) && !is_null($bankCode)) {

            return ClientConfigs::$clientData[$this->bankID]['clientCode'];

        } else {

            $this->saveChannelRequestToRedis(Config::OVERALSTATUS_FAILURE_CODE, 'Could not fetch bank code');

        }
    }


    /**
     *  Returns walletConfigs of the bank details fetched from WalletConfigs class
     *  fetched using the bankCode returned in getBankCode function
     * @param $bankCode
     * @return mixed
     */
    public function fetchWalletConfigs($bankCode)
    {
        $walletConfigs = WalletConfigs::$bankWallets[$bankCode];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " got the wallet configs : " . json_encode($walletConfigs));

        if (!empty($walletConfigs) && !is_null($walletConfigs)) {

            return WalletConfigs::$bankWallets[$bankCode];

        } else {

            $this->saveChannelRequestToRedis(Config::OVERALSTATUS_FAILURE_CODE, 'Could not get bank code');

        }
    }


    /**
     *  invokes the fetchCustomerData api that fetched wallet details of the client
     *  from the passed bankDetails.
     * @param $bankDetails
     * @return array
     */
    public function fetchCustomerData($bankDetails)
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $url = $bankDetails['fetchCustomerDataParams']['apiUrl'];


        $username = $bankDetails['fetchCustomerDataParams']['apiCredentials']['username'];
        $password = $bankDetails['fetchCustomerDataParams']['apiCredentials']['password'];

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " username is : " .$username .' and password is '.$this->log->printArray($password));

        $fetchCustomerDataHelper = new FetchCustomerDataHelper();

        $fetchCustomerDataHelperResponse = $fetchCustomerDataHelper -> queryWallet(
            $this->msisdn,
            $username,
            $password,
            $url,
            $this->log
        );

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return $fetchCustomerDataHelperResponse;
    }


    /**
     *  A method to do a financial institution to mpesa prompt
     */
    public function invokePinPromptApi()
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $menuString  = $this -> formatPinValidationMessage();

        $payload = array(

            "MSISDN" => $this->msisdn, //mandatory
            "clientCode" => SAFKEConfigs::DEFAULT_CLIENT_CODE,
            "callBackUrl" => SAFKEConfigs::DEFAULT_SAFKE_PINPROMT_HANDLECALLBACK, //optional
            "menuString" =>  $menuString, //optional
            "extraData" => array(
                'authcode'=> $this->channelRequestPayload['TransactionRequest']['requestHeader']['authcode'],
                'txid'=> $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid'],
                'pid'=> $this->channelRequestPayload['TransactionRequest']['requestHeader']['pid'],
                'encparam'=>  SAFKEProcessor::getBlowfishKey(
                    $this->msisdn,
                    $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid']
                ), // SAFKEConfigs::BLOW_FISH_KEY,
                'iv'=> SAFKEConfigs::IV,
                'enc' => SAFKEConfigs::ENC
            ),
            "cellulantKey" => SAFKEConfigs::DEFAULT_ACCESS_CODE,
        );

        $curlResponse = $this->coreUtils->curlPost(SAFKEConfigs::DEFAULT_SAFKE_PINPROMPT_URL, $payload);

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " Pin Prompt Response :" . $curlResponse);

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        if ($rcode['statusCode'] == SAFKEConfigs::SUCCESS) {

            return true;

        } else {

            $this->log->errorLog(Config::ERROR, -1, "Failed on invoking the pin prompt action:" . $curlResponse);

            $this->saveChannelRequestToRedis(StatusCodes::PIN_PROMPT_INVOKED_FAILED, Config::DEFAULT_FAILURE_RESPONSE);

        }
    }


    /**
     * Formats a pin validation message with parameters inputs
     * @return mixed
     */
    public function formatPinValidationMessage()
    {

        $payload = array(
            "amount" => $this -> channelRequestPayloadData['amount'],
            "bank" => ClientConfigs::$clientData[$this->bankID]['clientName'],
            "msisdn" =>  $this -> channelRequestPayloadData['ReceiverIdentifier'],
            "charge" =>  ClientConfigs::$clientData[$this->bankID]['B2CCharge'],
        );

        $formattedResponse = SAFKEProcessor::formulateMBankingPinPrompt($payload);

        $this->log->debugLog(Config::DEBUG, $this->msisdn, " formatted response  : " . $formattedResponse);

        return $formattedResponse;
    }


    /**
     * @param  $message
     *  A method to invoke a push menu prompt for invalid and blocked accounts
     */
    public function invokeInvalidAccountPrompt($message)
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);
        $dt= date('Ymdhis');
        $header = array(
            "txid" => $this->channelRequestPayload['TransactionRequest']['requestHeader']['txid'],
            "cid" => SAFKEConfigs::DEFAULT_COMMAND_ID,
            "ln" => Config::OUTGOING_AUTH_USERNAME,

            /**
             * Default Password
             */
            "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD, $dt),

            /**
             *  user $dt variable
             */
            "dt" => $dt,

            "pid" =>$this->channelRequestPayload['TransactionRequest']['requestHeader']['pid'],

            /**
             * check the docs on whether pid will be sent from api else add a DEFAULT Pid param in config
             */
            "pnm" => SAFKEConfigs::DEFAULT_THIRD_PARTY_NAME
        );

        $data = array(

            "cburl" => SAFKEConfigs::DEFAULT_SAFKE_ACCOUNT_SELECTION_HANDLECALLBACK,

            /**
             * $cburl, //if the callback url is not specified we will use a config  which we have defined
             * eg http://localhost/pushMenus/PinPromptMenu/handleCallback
             */
            "authcode" => $this->channelRequestPayload['TransactionRequest']['requestHeader']['authcode'],
            "msisdn" => $this->msisdn,
            "CustomerPrompt" => $message,

            /**
             * If the menu is not specified in payload call a function to generate one
             */
            "enc" => SAFKEConfigs::ENC_DEFAULT, //enc
        );

        $generatedData = SAFKEProcessor:: generateSafKeyValueFormat($data);

        $payload = array(
            "AuthRequest" => array(
                "requestHeader" => $header,
                "requestData" => array(
                    "data" => $generatedData
                )
            )
        );

        $invokeInvalidAccountPromptResponse = $this->coreUtils->curlPost(
            SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT,
            $payload,
            $header
        );

        $decodedResponse = json_decode($invokeInvalidAccountPromptResponse, true);

        $this->log->debugLog(
            Config::DEBUG,
            $this->msisdn,
            " Push menu Prompt Response for invalid/blocked account :" . $this -> log -> printArray($decodedResponse));

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        if ($decodedResponse['AuthResponse']['responseData']['rcode'] == SAFKEConfigs::SUCCESS) {

            $this->log->errorLog(Config::ERROR, -1, " Invoked invalid pin prompt :: " . $invokeInvalidAccountPromptResponse);

            return true;

        } else {

            $this->log->errorLog(Config::ERROR, -1, "Failed invoking the push menu :: invalid or blocked account:" . $invokeInvalidAccountPromptResponse);

            $this->saveChannelRequestToRedis(StatusCodes::PIN_PROMPT_INVOKED_FAILED, Config::DEFAULT_FAILURE_RESPONSE);
        }
    }



    /**
     * Logs requested payload to channelRequests table
     * @return mixed|null - the channelRequestID if successful,flag duplicate for
     * duplicate merchant transactionID and null if an error occurs
     */
    public function saveChannelRequestToRedis($statusCode, $statusMessage)
    {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

                $this->log->debugLog(Config::DEBUG, $this->msisdn, " New Transaction with redis key :: ". $this->redisKey);

                $fieldsValuesArray = array(
                    'gatewayUID' => $this -> channelRequestPayload['TransactionRequest']['requestHeader']['txid'],
                    'MSISDN' => $this->msisdn,
                    'payload' => json_encode($this -> channelRequestPayload),
                    'serviceDescription' => intval(SAFKEConfigs::DEFAULT_SERVICE_DESCRIPTION),
                    'overallStatus' => intval($statusCode),
                    'statusDescription' => $statusMessage,
                );

                $this -> redis -> setKeyMultipleValues($this->redisKey, $fieldsValuesArray);



            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            $this -> formulateResponse($statusCode, $statusMessage);

        } catch (Exception $e) {

            $this->log->errorLog(Config::CRON_ERROR, -1, " Redis Server Error". $e -> getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            $this -> formulateResponse($statusCode, $statusMessage);
        }
    }

    /**
     *  Formulates the response to be returned to the daemon
     * @param $response
     * @return array
     */
    public function formulateResponse($statusCode, $statusMessage)
    {
        $response =  array(
            'code' => $statusCode,
            'description' => $statusMessage
        );

        $this->coreUtils -> renderResponse($response);

    }

}