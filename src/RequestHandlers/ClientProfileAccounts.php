<?php

/**
 * This class contains CRUD functionality of managing an client profile account
 *
 * PHP Version 5.6.34
 *
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 */
class ClientProfileAccounts {

    /**
     * The log class instance.
     *
     * @var object
     */
    private $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    private $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    private $toCode;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    private $tat;

    public $authorization;

    public $credentials;

    public $dataPacket;

    public $clientID;

    public $MSISDN;

    /**
     * Constructor.
     */
    public function __construct() {
        $this->log = new CoreAppLogger();
        $this->fromCode = "";
        $this->toCode = "";
        $this->tat = new BenchMark(session_id());
        $this->authorization = new Authorization();
        $this->initValues();
    }

    public function initValues() {

        $data = CoreUtils::receivePost();
        $post = $data['payload'];

        $this->credentials = $post['credentials'];
        $this->dataPacket = $post['packet'];

        $username = $this->credentials['username'];
        $this->MSISDN = $this->dataPacket['msisdn'];

        $this->clientID = $this->authorization->getUserclientID($username);
        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Client ID = " . $this->clientID);

//        $this->authorization->validateToken($this->clientID);

    }

    /**
     * handles Creating profile account request information
     * @return type
     */
    public function create() {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);


        /** Profile  account information  */
        $sourceAddress = !empty($this->dataPacket['sourceAddress']) ? $this->dataPacket['sourceAddress'] : NULL;
        $origin = Config::DEFAULT_REQUEST_ORIGIN;

        $deviceID = !empty($this->dataPacket['deviceID']) ? $this->dataPacket['deviceID'] : NULL;
        $parseInstallationID = !empty($this->dataPacket['parseInstallationID']) ? $this->dataPacket['parseInstallationID'] : NULL;

        $osVersion = !empty($this->dataPacket['osVersion']) ? $this->dataPacket['osVersion'] : NULL;
        $deviceName = !empty($this->dataPacket['deviceName']) ? $this->dataPacket['deviceName'] : NULL;

        $isExplicit = "Y";
        $devicePlatform = !empty($this->dataPacket['devicePlatform']) ? $this->dataPacket['devicePlatform'] : NULL;

        $email = !empty($this->dataPacket['email']) ? $this->dataPacket['email'] : NULL;
        $currentAppVersion = !empty($this->dataPacket['currentAppVersion']) ? $this->dataPacket['currentAppVersion'] : NULL;

        $surname = !empty($this->dataPacket['surname']) ? $this->dataPacket['surname'] : NULL;
        $otherNames = !empty($this->dataPacket['otherNames']) ? $this->dataPacket['otherNames'] : NULL;

        /**
         * Creating hub profile account. If a success, we continue to create
         * hub client profile account
         */
        $responseCreateHubProfile = $this->createHubProfile($origin);
        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Response: " . json_encode($responseCreateHubProfile));

        if ($responseCreateHubProfile['statusCode'] != StatusCodes::OVERALL_SUCCESS) {

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Failed to create hub profile, returning a failed response...");

            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => Config::DEFAULT_FAILURE_RESPONSE,
                'DATA' => $responseCreateHubProfile['DATA']['REASON'],
            );

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Failed to create hub profile Response: " . json_encode($responseArray));
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return $this->formulateResponse($responseArray);

        } else {

            $profileID = $responseCreateHubProfile['DATA']['DATA'][0]['profileID'];


            $userData = array(
                'profileID' => $profileID, 'deviceID' => $deviceID, 'parseInstallationID' => $parseInstallationID,
                'osVersion' => $osVersion, 'deviceName' => $deviceName, 'origin' => $origin, 'sourceAddress' => $sourceAddress,
                'devicePlatform' => $devicePlatform, 'email' => $email, 'hubServiceID' => $this->dataPacket['serviceID'],
                'hubClientID' => $this->clientID, 'activationStatus' => Config::ACTIVE_ACTIVATION_STATUS,
                'activationStatusDescription' => Config::ACTIVE_ACTIVATION_STATUS_DESCRIPTION, 'isExplicit' => $isExplicit,
                'currentAppVersion' => $currentAppVersion, 'surname' => $surname, 'otherNames' => $otherNames,
            );

            //==================================================================
            //  Create client profile
            //==================================================================

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Lets create a Hub Client Profile");

            $createClientProfileResponse = $this->createHubClientProfile($userData);

            if ($createClientProfileResponse['statusCode'] != StatusCodes::OVERALL_SUCCESS) {

                $responseArray = array(
                    'statusCode' => StatusCodes::FAILURE,
                    'statusDescription' => 'Failed to create Hub client profile',
                    'DATA' => $createClientProfileResponse['DATA']['REASON'],
                );

                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
                return $this->formulateResponse($responseArray);

            } else {

                $clientProfileID = $createClientProfileResponse['DATA']['DATA'][0]['clientProfileID'];

                $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Successfully created hub client profile. Client ProfileID: " . $clientProfileID);

                //==================================================================
                //  Create client profile Account
                //==================================================================

                $merchantAccountNumber = $this->dataPacket['accountNumber'];
                $accountAlias = !empty($this->dataPacket['accountAlias']) ? $this->dataPacket['accountAlias'] : NULL;
                $merchantAccountName = !empty($this->dataPacket['accountName']) ? $this->dataPacket['accountName'] : NULL;

                $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Lets create a hub client profile account");

                $clientProfileAccountsResponse = $this->createHubClientProfileAccounts($clientProfileID,
                    $merchantAccountNumber, $origin, $accountAlias, $isExplicit, $merchantAccountName);

                $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Create client profile account response: " . json_encode($clientProfileAccountsResponse));

                if ($clientProfileAccountsResponse['statusCode'] != StatusCodes::OVERALL_SUCCESS) {

                    $responseArray = array(
                        'statusCode' => StatusCodes::FAILURE,
                        'statusDescription' => 'Your request to create an account could not be fulfilled',
                        'DATA' => $clientProfileAccountsResponse['DATA']['REASON'],
                    );

                    $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
                    return $this->formulateResponse($responseArray);

                } else {

                    $responseArray = array(
                        'statusCode' => StatusCodes::OVERALL_SUCCESS,
                        'statusDescription' => 'Successfully created account',
                        'DATA' => $this->formatSingleAccountResponse($clientProfileAccountsResponse['DATA']['DATA'], __FUNCTION__),
                    );

                    $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
                    return $this->formulateResponse($responseArray);

                }
            }
        }
    }

    /**
     * Query for client profile information
     * @return type
     */
    public function query() {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);


        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Credentials: " . json_encode($this->log->printArray($this->credentials)));
        $customerProfileData = $this->fetchCustomerData();

        if ($customerProfileData['statusCode'] == StatusCodes::OVERALL_SUCCESS) {

            $customerData = $customerProfileData['DATA']['DATA']['DATA']['merchantAccounts'];

            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully fetched customer information',
                'DATA' => $this->formatMerchantAccounts($customerData),
            );

        } else {

            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => 'Customer information unavailable',
                'DATA' => $customerProfileData['DATA'],
            );

        }

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return$this->formulateResponse($responseArray);

    }

    /**
     * Update account information
     * @return type
     */
    public function update() {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);
        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Lets formulate "
            . "request to use while updating the account info");

        $accountInfo = array(
            'clientID' => $this->clientID,
            'msisdn' => $this->MSISDN,
            'clientProfileAccountID' => $this->dataPacket['clientProfileAccountID'],
            'origin' => Config::DEFAULT_REQUEST_ORIGIN,
            'merchantServiceID' => $this->dataPacket['serviceID'],
            'merchantAccountNumber' => $this->dataPacket['accountNumber'],
            'isExplicit' => 'Y',
            'accountAlias' => '',
            'merchantAccountName' => $this->dataPacket['accountName'],
        );

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Lets formulate request to use while updating the account info");


        $updateAccResponse = $this->updateHubClientProfileAccounts($accountInfo);

        if ($updateAccResponse['statusCode'] == StatusCodes::OVERALL_SUCCESS) {

            $customerData = $updateAccResponse['DATA']['DATA'];

            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully updated account information',
                'DATA' => $this->formatSingleAccountResponse($customerData, __FUNCTION__),
            );

        } else {

            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => 'Unfortunately the account was not updated',
                'DATA' => $updateAccResponse['DATA']['REASON'],
            );

        }

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return $this->formulateResponse($responseArray);

    }

    /**
     * Delete for user information
     * @return type
     */
    public function delete() {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Lets prepare to delete Hub Client Profile Account");
        $deleteAccResponse = $this->deleteHubClientProfileAccounts();

        if ($deleteAccResponse['statusCode'] == StatusCodes::OVERALL_SUCCESS) {

            $customerData = $deleteAccResponse['DATA']['DATA'];

            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully deleted account',
                'DATA' => $this->formatSingleAccountResponse($customerData, __FUNCTION__),
            );

        } else {

            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => 'Deletion of the account was unsuccessful',
                'DATA' => $deleteAccResponse['DATA']['REASON'],
            );

        }

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return $this->formulateResponse($responseArray);
    }

    //==========================================================================
    //  HUB API INVOCATIONS
    //==========================================================================

    /**
     * Fetch customer data
     * @param type $MSISDN
     * @param type $clientID
     * @param type $origin
     * @param type $credentials
     * @return type
     */
    public function fetchCustomerData() {

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Fetch Customer Data");

        $url = Config::HUB_PROFILING_API_URL . Config::HUB_PROFILING_API_FETCH_CLIENT_PROFILE;

        $dataString = "MSISDN=" . $this->MSISDN;
        if (!empty($this->clientID)) {
            $dataString .="&clientID=" . $this->clientID;
        }

        $data = rawurlencode($dataString);
        $url .= $data;

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "URL: " . $url);

        // Base 64 encode the credentials as
        // username and password respectively

        $encodedCredentials = base64_encode($this->credentials['username'] . ":" . $this->credentials['password']);

        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
            "authorization: Basic " . $encodedCredentials,
            "cache-control: no-cache",
        );

        $customerData = CoreUtils::curlGet($url, $headers);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Raw response "
            . "received after sending the request: " . json_encode($customerData));

        $content = json_decode($customerData, true);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Profile content: " . json_encode($content));

        if ($content['SUCCESS'] == "TRUE") {

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Successfully "
                . "fetched customer information");
            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully fetched customer Data',
                'DATA' => $content
            );

        } else {

            $this->log->errorLog(Config::DEBUG, $this->MSISDN, "Failed to "
                . "fetch customer information");
            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => 'Customer Data unavailable',
                'DATA' => 'Customer Data unavailable'
            );

        }

        $this->log->infoLog(Config::DEBUG, $this->MSISDN, "Returning the "
            . "following response: ".  json_encode($responseArray));

        return $responseArray;

    }

    /**
     * Create Customer profile
     * @param type $MSISDN
     * @param type $origin
     * @param type $credentials
     */
    public function createHubProfile($origin) {

        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $url = Config::HUB_PROFILING_API_URL . Config::HUB_PROFILING_API_CREATE_PROFILE;

        $dataString = "MSISDN=" . $this->MSISDN . "&requestOriginCode=" . $origin;

        $data = rawurlencode($dataString);
        $url .= $data;

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "URL: " . $url);

        //Base 64 encode the credentials
        $encodedCredentials = base64_encode($this->credentials['username']
            . ":" . $this->credentials['password']);

        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
            "authorization: Basic " . $encodedCredentials,
            "cache-control: no-cache",
        );

        $createProfileResponse = CoreUtils::curlPost($url, $data, $headers);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Create Profile Response: " . json_encode($createProfileResponse));

        $decodeResponse = json_decode($createProfileResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Decode Response: " . json_encode($decodeResponse));

        if ($decodeResponse['SUCCESS'] == "TRUE") {

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Successfully created hub client profile");

            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully created user profile',
                'DATA' => $decodeResponse
            );

        } else {

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "We failed to create hub client profile");
            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => isset($decodeResponse['REASON']) ?
                    $decodeResponse['REASON'] : 'Failed to create Hub Profile',
                'DATA' => $decodeResponse
            );

        }

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Response: " . json_encode($responseArray));

        return $responseArray;

    }

    /**
     * Create client profile account
     * @param type $MSISDN
     * @param type $userData
     * @param type $credentials
     */
    public function createHubClientProfile($userData) {

        $url = Config::HUB_PROFILING_API_URL . Config::HUB_PROFILING_API_CREATE_CLIENT_PROFILE;

        $dataString = "MSISDN=" . $this->MSISDN . "&profileID=" . $userData['profileID'] . "&deviceID="
            . $userData['deviceID'] . "&installationId=" . $userData['parseInstallationID']
            . "&deviceOSVersion=" . $userData['osVersion'] . "&deviceName=" . $userData['deviceName']
            . "&requestOriginCode=" . $userData['origin'] . "&sourceAddress=" . $userData['sourceAddress']
            . "&devicePlatform=" . $userData['devicePlatform'] . "&emailAddress=" . $userData['email']
            . "&merchantServiceID=" . $userData['hubServiceID'] . "&clientID=" . $userData['hubClientID']
            . "&activationStatus=" . $userData['activationStatus']
            . "&activationStatusDescription=" . $userData['activationStatusDescription']
            . "&isExplicit=" . $userData['isExplicit']
            . "&currentAppVersion=" . $userData['currentAppVersion']
            . "&surname=" . $userData['surname'] . "&otherNames=" . $userData['otherNames'];

        $data = rawurlencode($dataString);
        $url .= $data;

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "URL: " . $url);

        //Base 64 encode the credentials
        $encodedCredentials = base64_encode($this->credentials['username'] . ":" . $this->credentials['password']);

        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
            "authorization: Basic " . $encodedCredentials,
            "cache-control: no-cache",
        );

        $createClientProfileResponse = CoreUtils::curlPost($url, $data, $headers);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Create Client Profile Response: " . json_encode($createClientProfileResponse));

        $decodeResponse = json_decode($createClientProfileResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Decode Response: " . json_encode($decodeResponse));

        if ($decodeResponse['SUCCESS'] == "TRUE") {

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Successfully "
                . "created Hub Client Profile");
            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully created client profile',
                'DATA' => $decodeResponse
            );

        } else {

            $this->log->errorLog(Config::DEBUG, $this->MSISDN, "Failed to "
                . "create Hub Client Profile");
            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => isset($decodeResponse['REASON']) ? $decodeResponse['REASON'] : 'Failed to created client profile',
                'DATA' => $decodeResponse
            );

        }

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Response: " . json_encode($responseArray));

        return $responseArray;
    }

    /**
     * Create client accounts
     * @param type $MSISDN
     * @param type $clientProfileID
     * @param type $merchantaccountnumber
     * @param type $origin
     * @param type $hubServiceID
     * @param type $accountAlias
     * @param type $isExplicit
     * @param type $merchantAccountName
     * @param type $credentials
     */
    public function createHubClientProfileAccounts($clientProfileID, $merchantaccountnumber, $origin, $accountAlias, $isExplicit, $merchantAccountName) {

        $url = Config::HUB_PROFILING_API_URL . Config::HUB_PROFILING_API_CREATE_CLIENT_PROFILE_ACCOUNTS;

        $dataString = "MSISDN=" . $this->MSISDN . "&clientProfileID=" . $clientProfileID . "&merchantAccountNumber="
            . $merchantaccountnumber . "&merchantServiceID=" . $this->dataPacket['serviceID']
            . "&requestOriginCode=" . $origin . "&accountAlias=" . $accountAlias
            . "&isExplicit=" . $isExplicit . "&merchantAccountName=" . $merchantAccountName;

        $data = rawurlencode($dataString);
        $url .= $data;
        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "URL" . $url);

        //Base 64 encode the credentials
        $encodedCredentials = base64_encode($this->credentials['username'] . ":" . $this->credentials['password']);

        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
            "authorization: Basic " . $encodedCredentials,
            "cache-control: no-cache",
        );

        $createClientProfileAccountResponse = CoreUtils::curlPost($url, $data, $headers);


        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Create Client Profile Account Response: " . json_encode($createClientProfileAccountResponse));

        $decodeResponse = json_decode($createClientProfileAccountResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Decode Response: " . json_encode($decodeResponse));

        if ($decodeResponse['SUCCESS'] == "TRUE") {

            $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Successfully created Client Profile Account");

            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully created client profile account',
                'DATA' => $decodeResponse
            );

        } else {

            $this->log->errorLog(Config::DEBUG, $this->MSISDN, "Failed to create Client Profile Account");

            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => isset($decodeResponse['REASON']) ? $decodeResponse['REASON'] : 'Failed to create profile account',
                'DATA' => $decodeResponse
            );

        }

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Response: " . json_encode($responseArray));

        return $responseArray;

    }

    /**
     * Update account information
     * @param type $accountInfo
     * @param type $credentials
     * @return type
     */
    public function updateHubClientProfileAccounts($accountInfo) {

        $url = Config::HUB_PROFILING_API_URL . Config::HUB_PROFILING_API_UPDATE_CLIENT_PROFILE_ACCOUNTS;

        $dataString = "";

        if (isset($accountInfo['clientID'])) {

            $dataString .= "clientID=" . $accountInfo['clientID'];

        } else if (isset($accountInfo['msisdn'])) {

            if ($dataString == "") {

                $dataString .= "MSISDN=" . $accountInfo['msisdn'];

            } else {

                $dataString .= "&MSISDN=" . $accountInfo['msisdn'];

            }
        } else if (isset($accountInfo['origin'])) {

            if ($dataString == "") {

                $dataString .= "requestOriginCode=" . $accountInfo['origin'];

            } else {

                $dataString .= "&requestOriginCode=" . $accountInfo['origin'];

            }
        } else if (isset($accountInfo['merchantServiceID'])) {

            if ($dataString == "") {

                $dataString .= "merchantServiceID=" . $accountInfo['merchantServiceID'];

            } else {

                $dataString .= "&merchantServiceID=" . $accountInfo['merchantServiceID'];

            }
        } else if (isset($accountInfo['merchantAccountNumber'])) {

            if ($dataString == "") {

                $dataString .= "merchantAccountNumber=" . $accountInfo['merchantAccountNumber'];

            } else {

                $dataString .= "&merchantAccountNumber=" . $accountInfo['merchantAccountNumber'];

            }
        } else if (isset($accountInfo['isExplicit'])) {

            if ($dataString == "") {

                $dataString .= "isExplicit=" . $accountInfo['isExplicit'];

            } else {

                $dataString .= "&isExplicit=" . $accountInfo['isExplicit'];

            }
        } else if (isset($accountInfo['accountAlias'])) {

            if ($dataString == "") {

                $dataString .= "accountAlias=" . $accountInfo['accountAlias'];

            } else {

                $dataString .= "&accountAlias=" . $accountInfo['accountAlias'];

            }
        } else if (isset($accountInfo['merchantAccountName'])) {

            if ($dataString == "") {

                $dataString .= "merchantAccountName=" . $accountInfo['merchantAccountName'];

            } else {

                $dataString .= "&merchantAccountName=" . $accountInfo['merchantAccountName'];

            }
        }


        $dataString .= "&clientProfileAccountID=" . $accountInfo['clientProfileAccountID'];

        $data = rawurlencode($dataString);
        $url .= $data;

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "URL" . $url);

        //Base 64 encode the credentials
        $encodedCredentials = base64_encode($this->credentials['username'] . ":" . $this->credentials['password']);

        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
            "authorization: Basic " . $encodedCredentials,
            "cache-control: no-cache",
        );

        $updateAccResponse = CoreUtils::curlPost($url, $data, $headers);

        $decodeResponse = json_decode($updateAccResponse, true);


        if ($decodeResponse['SUCCESS'] == "TRUE") {

            $this->log->infoLog(Config::DEBUG, $this->MSISDN, "Successfully updated Hub Client Profile Account");
            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully update account information',
                'DATA' => $decodeResponse
            );

        } else {

            $this->log->errorLog(Config::DEBUG, $this->MSISDN, "Failed to update Hub Client Profile Account");
            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => isset($decodeResponse['REASON']) ? $decodeResponse['REASON'] : 'Failed to update account information',
                'DATA' => $decodeResponse
            );

        }

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Response: " . json_encode($responseArray));

        return $responseArray;

    }

    /**
     * Delete Account
     * @return type
     */
    public function deleteHubClientProfileAccounts() {

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, __METHOD__);

        $url = Config::HUB_PROFILING_API_URL . Config::HUB_PROFILING_API_DELETE_CLIENT_PROFILE_ACCOUNTS;

        $dataString = "active=6&MSISDN=" . $this->MSISDN . "&clientProfileAccountID=" . $this->dataPacket['clientProfileAccountID'];

        if (isset($this->clientID)) {

            $dataString .= "&clientID=" . $this->clientID;

        }

        $data = rawurlencode($dataString);
        $url .= $data;

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "URL" . $url);

        //Base 64 encode the credentials
        $encodedCredentials = base64_encode($this->credentials['username'] . ":" . $this->credentials['password']);

        $headers = array(
            "Content-Type: application/json",
            "Content-Length: " . strlen($data),
            "authorization: Basic " . $encodedCredentials,
            "cache-control: no-cache",
        );

        $deleteAccResponse = CoreUtils::curlPost($url, $data, $headers);

        $decodeResponse = json_decode($deleteAccResponse, true);

        $this->log->debugLog(Config::DEBUG, $this->MSISDN, "Decoded Delete Hub "
            . "Client Profile Account Response: " . json_encode($decodeResponse));


        if ($decodeResponse['SUCCESS'] == "TRUE") {

            $this->log->infoLog(Config::DEBUG, $this->MSISDN, "Successfully deleted hub client profile account");

            $responseArray = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Successfully deleted client profile account',
                'DATA' => $decodeResponse
            );

        } else {

            $this->log->errorLog(Config::DEBUG, $this->MSISDN, "Failed to delete hub client profile account");

            $responseArray = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => isset($decodeResponse['REASON'])
                    ? $decodeResponse['REASON'] : 'Failed to delete client profile account',
                'DATA' => $decodeResponse
            );

        }

        $this->log->infoLog(Config::DEBUG, $this->MSISDN, "Reurning the following response: " . json_encode($responseArray));

        return $responseArray;

    }

    /**
     * Standardize responses
     * @param type $response
     */
    public function formulateResponse($response) {
        $responseArray = array();
        $authArray = array();

        $statusCode = $response['statusCode'];

        switch ($statusCode) {
            case StatusCodes::OVERALL_SUCCESS:
                $authArray['authStatusCode'] = StatusCodes::CLIENT_AUTHENTICATED_SUCCESSFULLY;
                $authArray['authStatusDescription'] = 'Authentication was a success';
                break;

            case StatusCodes::FAILURE:
                $authArray['authStatusCode'] = StatusCodes::CLIENT_AUTHENTICATION_FAILED;
                $authArray['authStatusDescription'] = 'Authentication failed';
                break;

            default:
                break;
        }

        $responseArray['authStatus'] = $authArray;
        $responseArray['results'] = !empty($response['DATA']) ? $response['DATA'] : array();

        return $responseArray;
    }

    /**
     * Format response to merchant
     * @param type $merchantAccounts
     * @return type
     */
    public function formatMerchantAccounts($merchantAccounts) {
        $formatedArray = array();
        foreach ($merchantAccounts as $account) {
            $formatedArray[] = array(
                'statusCode' => StatusCodes::OVERALL_SUCCESS,
                'statusDescription' => 'Fetched account successfully',
                'clientProfileAccountID' => $account['clientProfileAccountID'],
                //Service info
                'serviceID' => $account['HUB_SERVICE_ID'],
                'serviceCode' => $account['SERVICE_CODE'],
                'serviceName' => $account['SERVICE_NAME'],
                //Account info
                'accountNumber' => $account['ACCOUNT_NUMBER'],
                'accountName' => $account['ACCOUNT_NAME'],
                'accountAlias' => $account['ACCOUNT_ALIAS'],
            );
        }
        return $formatedArray;
    }

    /**
     * formatting response back to merchant
     * @param type $merchantAccounts
     * @param type $method
     * @return string
     */
    public function formatSingleAccountResponse($merchantAccounts, $method) {
        $formatedArray = array();
        $account = $merchantAccounts[0];
        $formatedArray[] = array(
            'statusCode' => StatusCodes::OVERALL_SUCCESS,
            'statusDescription' => "Successfully " . $method . "d account",
            'clientProfileAccountID' => $account['clientProfileAccountID'],
            //Service info
            'serviceID' => $account['merchantServiceID'],
            'accountNumber' => $account['merchantAccountNumber'],
            'accountName' => $account['merchantAccountName'],
            'accountAlias' => $account['accountAlias'],
            'active' => $account['active'],
        );
        return $formatedArray;
    }

}
