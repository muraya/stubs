<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 07/03/18
 * Time: 16:20
 *  A function to process the payload sent back by safaricom to the authussdcallback url
 */
include_once ('./lib/IXR_Library.php');

class AuthCallbackDB
{
    /**
     *  The log class instance
     * @var
     */
    public $log;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * Constructor.
     */

    public $coreUtils;

    /**
     * this holds the data of the body of the payload
     * @var $requestBody
     */
    public $requestBody;

    /**
     * this holds the data of the payload for header values
     * @var $requestHeader
     */
    public $requestHeader;

    /**
     * this holds the payload received
     * @var $payload
     */
    public $payload;

    /**
     * This holds the value for the bank id
     * @var $bankID
     */
    public $bankID;

    /**
     * this holds the value for the channel request id
     * @var $channelRequestID
     */
    public $channelRequestID;

    /**
     * this holds the array for the customer accounts
     * @var $customerAccounts
     */
    public $customerAccounts;

    /**
     * this is to hold the mobile number from the payload received
     * @var $msisdn
     */
    public $msisdn;

    /**
     * to hold the data to show where the request is heading to
     * @var $toCode
     */
    public $toCode;

    /**
     * to hold the data to show where the request is originating from
     * @var $fromCode
     */
    public $fromCode;

    /**
     * used to hold the data being received from the post function
     * @var $data
     */
    public $data;

    /**
     * used when initialising the safKEProcessor class
     * @var $safKEProcessor
     */
    public $safKEProcessor;
    /**
     * @var
     *  to authenticate the cellulant key
     */
    public $auth;

    /**
     * AuthCallback constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->tat = new BenchMark(session_id());
        $this->coreUtils = new CoreUtils();
        $this->auth = new Authentication();
        $this->toCode = "";
        $this->fromCode = "";
        $this->initValues();
    }

    /**
     *  initialise your process
     */
    public function initValues()
    {
        $this->safKEProcessor = new SAFKEProcessor();
        $this->data = CoreUtils::receivePost();
        $this->requestBody= SAFKEProcessor::formatRequestData($this->data['TransactionRequest']['requestData']['data']);
        $this->requestHeader = $this->data['TransactionRequest']['requestHeader'];
        $this->validatePayload();
        $this->msisdn = $this->requestBody['msisdn'];
        //to handle the authentication of the password
        $authentication = $this->coreUtils->authenticateRequest(
            $this->requestHeader['ln'],
            $this->requestHeader['ps'],
            $this->requestHeader['dt']
        );

        $this->fetchChannelRequestLogs($this->requestHeader['txid']);

        $g = $this->fetchChannelRequestLogs($this->requestHeader['txid']);
        $this->channelRequestID = $g['channelRequestID'];


        if ($authentication != SAFKEConfigs::SUCCESS) {
            $this->formatResponse(
                SAFKEConfigs::AUTHENTICATION_FAILURE,
                SAFKEMessages::DEFAULT_AUTHENTIFICATION_FAILURE_MESSAGE
            );
        }
        $newPayload = null;
        if ($this->requestBody['resCode']
            != SAFKEConfigs::SUCCESS) {
            $this->safKEProcessor->updateChannelRequestOverallStatus(
                $this->requestBody['resCode'],
                $this->msisdn,
                Config::OVERALSTATUS_FAILURE_CODE,
                Config::OVERALSTATUS_FAILURE_MSG,
                $this->channelRequestID,
                $newPayload
            );
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Customer did not input pin" . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );
            exit();
        }

        if ($g['overalStatus'] == Config::OVERALSTATUS_PROCESSED_TRANSACTION_CODE) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "The customer transaction has already been processed" . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );
            exit();
        }
        $this->customerAccounts = json_decode($g['statusDescription'], true);
        $this->fromCode = "SAFKE";
        $this->toCode = "WALLET";
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Customer accounts" . json_encode($this->customerAccounts),
            $this->fromCode,
            $this->toCode
        );
    }

    /**
     * method to process the pin prompt ussd callback
     */
    public function processPinPromptCallback()
    {

        //TODO remove the or condition
        if (count($this->customerAccounts) == 1) {
            $this->formulatePayload($this->customerAccounts[1]);
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Incoming payload" . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );
            //methods to be invoked
            $this->sendViaXMLRPC();
        } elseif ($this->customerAccounts == null) {
            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                Responses::NO_ACCOUNTS_MESSAGE
            );
        } elseif (count($this->customerAccounts) > 1) {
            $this->invokeAccountSelectionPrompt();
        }
    }

    /**
     * method to process the account selection function
     */
    public function processAccountSelection()
    {

        $channelRequestData = $this->fetchChannelRequestLogs($this->requestHeader['txid']);
        $channelRequestLog = json_decode($channelRequestData['payload'], true);
        $accountKeys = array();
        foreach ($this->customerAccounts as $key => $value) {
            array_push($accountKeys, $key);
        }

        // small check to see if the account selected is valid
        if (!in_array($this->requestBody['EncData'], $accountKeys)) {
            //check to see the number of retrys done for incorect account selection choice
            if ($channelRequestLog['accountSelectionRetry'] <= Config::MAX_ACCOUNT_SELECTION_RETRY) {
                $this->invokeRetryAccountSelectionPrompt();
            } else {
                $this->log->debugLog(
                    Config::ERROR,
                    $this->msisdn,
                    "Max account selection retries reached",
                    $this->fromCode,
                    $this->toCode
                );
                $this->formatResponse(
                    SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                    Responses::NO_ACCOUNTS_MESSAGE
                );
            }
        } else {
            //end pont to be shared when invoking the customer to chosose account as the cburl
            $this->formulatePayload($this->customerAccounts[$this->requestBody['EncData']]);
            $this->sendViaXMLRPC();
        }

    }

    /**
     * @param $statusCode
     * @param $message
     * Simple method used to format the response
     */
    public function formatResponse($statusCode, $message)
    {
        $dt = date('YmdHis');
        $response = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => $this->requestHeader['txid'],
                    "dt" => $dt
                ),
                "responseData" => array(
                    "rcode" => $statusCode,
                    "rtext" => $message
                )
            )
        );

        $this->coreUtils ->renderResponse($response);

    }

    /**
     * Fetch the payload for the channel request
     * @param $txid
     * @return array|bool|mixed|null|ORM
     */
    public function fetchChannelRequestLogs($txid)
    {
        $this->safKEProcessor = new SAFKEProcessor();
        $channelRequestLog = $this->safKEProcessor->getChannelRequestData($txid, $this->msisdn);
        return $channelRequestLog;
    }

    /**
     * simple method to formulate our payload to send to wallet
     */
    public function formulatePayload($accountDetails)
    {

        $channelRequestData = $this->fetchChannelRequestLogs($this->requestHeader['txid']);
        $channelRequestLog = json_decode($channelRequestData['payload'], true);
        $data = SAFKEProcessor::formatRequestData($channelRequestLog['TransactionRequest']['requestData']['data']);
        $this->bankID = $data['SenderBankID'];
        $this->channelRequestID = $channelRequestData['channelRequestID'];
        $this->statusDescription = json_decode($channelRequestData['statusDescription'], true);
        //fetch the Wallet URL details
        $bankCode = $this->getBankCode();
        $walletConfigs = $this->fetchWalletConfigs($bankCode);

        if (!empty($channelRequestLog['pin'])) {
            $pin = $channelRequestLog['pin'];
        } else {
            $pin = $this->requestBody['EncData'];
        }

        $pin=trim($pin);
        $encryptPin = $this->encryptPin($pin, $walletConfigs['walletDebitRequest']['apiParameters']['B2CIMCREQUESTID']);
        $this->payload = array(
            "serviceID"=> $walletConfigs['walletDebitRequest']['apiParameters']['B2CSERVICEID'], //configs ya wallet b2c
            "flavour" =>  $walletConfigs['walletDebitRequest']['apiParameters']['B2CFLAVOUR'], //configs wallet b2c
            "pin" => $encryptPin,
            //configs per wallet(EncrptionIMCRequestID)
            "accountAlias" =>  $accountDetails['accountAlias'],  // services description column
            "amount" =>  $data['amount'],
            "merchantCode" =>  $walletConfigs['walletDebitRequest']['apiParameters']['B2CMERCHANTCODE'],
            //config wallet b2c
            "CBSID" =>  $walletConfigs['walletDebitRequest']['apiParameters']['B2CCBSID'],  //wallete config b2c
            "enroll" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CENROLL'],  // wallete config
            "columnD" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CCOLUMND'],  // null wallete
            "columnA" => CoreUtils::validateMSISDN($data['ReceiverIdentifier']), //RECIPIENT NUMBER
            //recipients  TODO to verify on wether column A to be account number or mobile numbre
            "columnB" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CCOLUMNB'], //null wallete
            "columnC" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CCOLUMNC'], //null wallete
            "accountID" => $accountDetails['accountID'] //service description column
        );
        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "The payload " . json_encode($this->data).' bank id' . $this->bankID,
            $this->fromCode,
            $this->toCode
        );

    }

    /**
     * Simple method to check whether any of the key values sent in the header are not empty
     */
    public function validatePayload()
    {
        //check if the headers and body values are empty
        if (empty($this->requestHeader['txid']) ||
            empty($this->requestHeader['cid']) ||
            empty($this->requestHeader['ln']) ||
            empty($this->requestHeader['ps']) ||
            empty($this->requestHeader['dt'])  ||
            empty($this->requestHeader['pnm']) ||
            empty($this->requestHeader['pid']) ||
            empty($this->requestBody['EncData']) ||
            empty($this->requestBody['cburl']) ||
            empty($this->requestBody['authcode']) ||
            empty($this->requestBody['msisdn'])) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Validating the payload " . json_encode($this->data),
                $this->fromCode,
                $this->toCode
            );

            $this->formatResponse(
                SAFKEConfigs::INVALID_REQUEST_PARAMETERS,
                SAFKEMessages::INVALID_REQUEST_PARAMETERS_MESSAGE
            );
        }
    }

    /**
     * method to decrypt the pin sent and encrypt again with wallet encryption standard
     * @param $pin
     * @param $imcrequestID
     * @return bool|string
     */
    public function encryptPin($pin, $imcrequestID)
    {
        $encryption1 = new BlowFishEncryption();
        //$decodedCipherText = $encryption1->SimpleDecrypt($pin);
        $decodedCipherText = $encryption1->simpleDecryptWithDynamicKey(
            $pin,
            SAFKEProcessor::getBlowfishKey(
                $this->msisdn,
                $this->requestHeader['txid']
            )
        );

        $decodedCipherText = trim($decodedCipherText);
        $encryption = new Encryption();
        $encryptedInput = $encryption->encryptPin(
            $decodedCipherText,
            1
        );

        $this->log->debugLog(
            Config::DEBUG,
            $this->msisdn,
            " walletPin ".$encryptedInput,
            $this->fromCode,
            $this->toCode
        );
        //    echo $decodedCipherText;
        //    exit();
        return $encryptedInput;
    }

    /**
     * this is a method to fetch the bank code
     * @return mixed
     */
    public function getBankCode()
    {
        $bankCode = ClientConfigs::$clientData[$this->bankID]['clientCode'];

        if (!empty($bankCode) && !is_null($bankCode)) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Bank code " . json_encode($bankCode). 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            return ClientConfigs::$clientData[$this->bankID]['clientCode'];
        } else {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Failed to get the bank code for bank id" .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            $this->formatResponse(
                StatusCodes::FAILURE,
                Config::BANK_CODE_ERROR_MESSAGE
            );
        }
    }

    /**
     * this is the method to handle fetching of wallete config data
     * @param $bankCode
     * @return mixed
     */
    public function fetchWalletConfigs($bankCode)
    {
        $walletConfigs = WalletConfigs::$bankWallets[$bankCode];

        if (!empty($walletConfigs) && !is_null($walletConfigs)) {
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Wallet data got" . json_encode($walletConfigs) . 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            return WalletConfigs::$bankWallets[$bankCode];
        } else {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Failed to get wallet configs for bank id" . $this->bankID,
                $this->fromCode,
                $this->toCode
            );
            $this->formatResponse(
                StatusCodes::FAILURE,
                Config::API_FETCH_ERROR_MESSAGE
            );
        }
    }

    /**
     * this is a method to handle the pushing of data to wallete
     */
    public function sendViaXMLRPC()
    {
        $this->safKEProcessor = new SAFKEProcessor();
        try {
            $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            //fetch the Wallet URL details
            $bankCode = $this->getBankCode();
            $walletConfigs = $this->fetchWalletConfigs($bankCode);

            $apiUrl = $walletConfigs['walletDebitRequest']['apiUrl'];
            $apiFunction = $walletConfigs['walletDebitRequest']['apiFunction'];

            //convert array into XML format
            //formulate xml payload.
            $request_xml = '';
            $request_xml = '<Payload>';
            foreach ($this->payload as $key => $value) {
                $request_xml .='<' . $key . '>' . $value . '</' . $key . '>';
            }

            $request_xml .= '</Payload>';

            $payload = $request_xml;
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Newly formatted payload" .$payload . 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );

            $credentials = array(
                'cloudUser' => $walletConfigs['walletDebitRequest']['apiCredentials']['username'],
                'cloudPass' => $walletConfigs['walletDebitRequest']['apiCredentials']['password'],
            );

            $dt = date('Y-m-d H:i:s');

            //define cloud packet data
            $cloudPacket = array(
                "MSISDN" => $this->requestBody['msisdn'],
                "destination" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CDESTINATION'],
                //wallete config
                "IMCID" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CIMCID'], // wallete config
                "channelRequestID" => $this->channelRequestID,
                "networkID" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CNETWORKID'],  //wallete config
                "cloudDateReceived" => $dt,
                "payload" => base64_encode($payload),
                "imcRequestID" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CIMCREQUESTID'],
                //wallete config
                "requestMode" => $walletConfigs['walletDebitRequest']['apiParameters']['B2CREQUESTMODE'],  //
            );

            //package our data
            $params = array(
                'credentials' => $credentials,
                'cloudPacket' => $cloudPacket,
            );
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Data being pushed to wallet" . json_encode($params) . 'for bank id' .$this->bankID,
                $this->fromCode,
                $this->toCode
            );
            //make API call
            $client = new IXR_Client($apiUrl);
            if (!$client->query($apiFunction, $params)) {
                $this->log->debugLog(
                    Config::INFO,
                    $this->msisdn,
                    "IXR client error occured" . $client->getErrorCode() . 'error message:'
                    .$client->getErrorMessage().
                    'wallet url: '. $apiUrl,
                    $this->fromCode,
                    $this->toCode
                );
                $this->formatResponse(
                    SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                    SAFKEMessages::DEFAULT_FAILURE_RESPONSE
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            }

            //get response
            $result = $client->getResponse();
            $data = json_decode($result, true);
            $this->log->debugLog(
                Config::INFO,
                $this->msisdn,
                "Wallet response" . json_encode($data) . 'wallet url' .$apiUrl,
                $this->fromCode,
                $this->toCode
            );
            $response = array();

            // return $data;
            if ($data['STAT_CODE'] != StatusCodes::WALLET_SUCCESS_STATUS_CODE) {
                $this->formatResponse(
                    SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                    SAFKEMessages::DEFAULT_FAILURE_RESPONSE
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            } else {
                $channelRequestData = $this->fetchChannelRequestLogs($this->requestHeader['txid']);
                $channelRequestLog = json_decode($channelRequestData['payload'], true);

                if (!empty($channelRequestLog['accountSelectionRetry']) && !empty($channelRequestLog['pin'])) {
                    $accountSelection = $channelRequestLog['accountSelectionRetry'];
                    $accountSelectionRetryArray = array(
                        'accountSelectionRetry' => $accountSelection
                    );
                    $pin = '****';
                    $pinArray = array(
                        'pin' => $pin
                    );
                }

                $newPayload = array_merge($channelRequestLog, $pinArray, $accountSelectionRetryArray);
                $this->safKEProcessor->updateChannelRequestOverallStatus(
                    $this->requestBody['resCode'],
                    $this->msisdn,
                    Config::OVERALSTATUS_PROCESSED_TRANSACTION_CODE,
                    Config::OVERALSTATUS_PROCESSED_TRANSACTION_MSG,
                    $this->channelRequestID,
                    $newPayload
                );
                $this->formatResponse(
                    SAFKEConfigs::SUCCESS,
                    SAFKEMessages::SUCCESS_MESSAGE
                );
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
            }
        } catch (Exception $exception) {
            $this->log->debugLog(
                Config::ERROR,
                $this->msisdn,
                "Wallet exception error" . $exception->getMessage() . 'wallet url' .$apiUrl,
                $this->fromCode,
                $this->toCode
            );

            $this->formatResponse(
                SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                SAFKEMessages::DEFAULT_FAILURE_RESPONSE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        }
    }

    public function formulateAccountSelectionPrompt()
    {
        $string = "";
        foreach ($this->customerAccounts as $key => $value) {
            $string .= "\n".$key.' : '.$value['accountAlias'];
        }
        $message = Responses::B2C_ACCOUNT_SELECTION_PROMPT_TEMPLATE.$string;

        return $message;
    }

    /**
     * this is a function to handle the account selection
     */
    public function invokeAccountSelectionPrompt()
    {
        $this->safKEProcessor = new SAFKEProcessor();
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        $dt= date('Ymdhis');

        $channelRequestData = $this->fetchChannelRequestLogs($this->requestHeader['txid']);
        $channelRequestLog = json_decode($channelRequestData['payload'], true);
        $accountSelection = Config::DEFAULT_ACCOUNT_SELECTION_RETRY;
        $accountSelections = $accountSelection +1;

        $accountSelectionRetryArray = array(
            'accountSelectionRetry' => $accountSelections
        );
        $pinArray = array(
            'pin' => $this->requestBody['EncData']
        );

        $newPayload = array_merge($channelRequestLog, $pinArray, $accountSelectionRetryArray);
        $this->safKEProcessor->updateChannelRequestPayload(
            $newPayload,
            $this->msisdn,
            $this->channelRequestID
        );

        $header = array(
            "txid" => $this->requestHeader['txid'],//
            "cid" => SAFKEConfigs::DEFAULT_COMMAND_ID,//
            "ln" => Config::OUTGOING_AUTH_USERNAME,//
            "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD, $dt),//default-password
            "dt" => $dt,//use $dt variable
            "pid" => $this->requestHeader['pid'],
            //check the docs on ehther pid will be sent from api else add a DEFAULT Pid param in config
            "pnm" => SAFKEConfigs::DEFAULT_THIRD_PARTY_NAME//configs
        );

        $data = array(
            "cburl" => SAFKEConfigs::DEFAULT_SAFKE_ACCOUNT_SELECTION_HANDLECALLBACK,
            //$cburl, //if the callback url is not specified we will use a config  which we have defined/
            // eg http://localhost/pushMenus/PinPromptMenu/handleCallback
            "authcode" => $this->requestBody['authcode'], //
            "msisdn" => $this->requestBody['msisdn'], //
            "CustomerPrompt" => $this->formulateAccountSelectionPrompt($accountSelections),
            //if the mrnu is not specified in paylosd csll sa function to generate one
            "enc" => SAFKEConfigs::ENC_DEFAULT, //enc
        );

        $generatedData = SAFKEProcessor:: generateSafKeyValueFormat($data);

        $payload = array(
            "AuthRequest" => array(
                "requestHeader" => $header,
                "requestData" => array(
                    "data" => $generatedData
                )
            )
        );

        $curlResponse = $this->coreUtils->curlPost(
            SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT,
            //add a config on SFKE= USSDPUS ENDPOINT
            $payload,
            $header
        );

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Curl response" . json_encode($curlResponse) .
            'for url' .SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT .
            'payload sent' . json_encode($payload),
            $this->fromCode,
            $this->toCode
        );

        if ($rcode['responseData']['rcode'] == SAFKEConfigs::SUCCESS) {
            $this->formatResponse(
                SAFKEConfigs::SUCCESS,
                SAFKEMessages::SUCCESS_MESSAGE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        } else {
            $this->formatResponse(
                SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                SAFKEMessages::DEFAULT_FAILURE_RESPONSE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        }
    }

    public function formulateRetryAccountSelectionPrompt($data)
    {
        $remainingRetrys =  Config::MAX_ACCOUNT_SELECTION_RETRY - $data;
        $baseMessage  = Responses::B2C_SECOND_ACCOUNT_SELECTION_PROMPT_TEMPLATE;
        $hashMaps = array("#number#");
        $hashMapValues   = array($remainingRetrys);

        $formattedMessage = str_replace($hashMaps, $hashMapValues, $baseMessage);
        $string = "";
        foreach ($this->customerAccounts as $key => $value) {
            $string .= "\n".$key.' : '.$value['accountAlias'];
        }
        $message = $formattedMessage.$string;

        return $message;
    }

    public function invokeRetryAccountSelectionPrompt()
    {
        $this->safKEProcessor = new SAFKEProcessor();
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        $dt= date('Ymdhis');
        $channelRequestData = $this->fetchChannelRequestLogs($this->requestHeader['txid']);
        $channelRequestLog = json_decode($channelRequestData['payload'], true);
        $accountSelection = $channelRequestLog['accountSelectionRetry'];
        $accountSelections = $accountSelection +1;
        $header = array(
            "txid" => $this->requestHeader['txid'],//
            "cid" => SAFKEConfigs::DEFAULT_COMMAND_ID,//
            "ln" => Config::OUTGOING_AUTH_USERNAME,//
            "ps" => CoreUtils::encodePasswordString(Config::OUTGOING_AUTH_PASSWORD, $dt),//default-password
            "dt" => $dt,//use $dt variable
            "pid" => $this->requestHeader['pid'],
            //check the docs on ehther pid will be sent from api else add a DEFAULT Pid param in config
            "pnm" => SAFKEConfigs::DEFAULT_THIRD_PARTY_NAME//configs
        );

        $data = array(
            "cburl" => SAFKEConfigs::DEFAULT_SAFKE_ACCOUNT_SELECTION_HANDLECALLBACK,
            //$cburl, //if the callback url is not specified we will use a config  which we have defined/
            // eg http://localhost/pushMenus/PinPromptMenu/handleCallback
            "authcode" => $this->requestBody['authcode'], //
            "msisdn" => $this->requestBody['msisdn'], //
            "CustomerPrompt" => $this->formulateRetryAccountSelectionPrompt($accountSelection),
            //if the mrnu is not specified in paylosd csll sa function to generate one
            "enc" => SAFKEConfigs::ENC_DEFAULT, //enc
        );

        $generatedData = SAFKEProcessor:: generateSafKeyValueFormat($data);

        $payload = array(
            "AuthRequest" => array(
                "requestHeader" => $header,
                "requestData" => array(
                    "data" => $generatedData
                )
            )
        );

        $curlResponse = $this->coreUtils->curlPost(
            SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT,
            //add a config on SFKE= USSDPUS ENDPOINT
            $payload,
            $header
        );

        $rcode = json_decode($curlResponse, true);

        $this->log->debugLog(
            Config::INFO,
            $this->msisdn,
            "Curl response" . json_encode($curlResponse) .
            'for url' .SAFKEConfigs::DEFAULT_SAFKE_USSDPUSH_ENDPOINT .
            'payload sent' . json_encode($payload),
            $this->fromCode,
            $this->toCode
        );

        if ($rcode['responseData']['rcode'] == SAFKEConfigs::SUCCESS) {
            $pinArray = array(
                'pin' => $channelRequestLog['pin']
            );
            $accountSelectionRetryArray = array(
                'accountSelectionRetry' => $accountSelections
            );
            $newPayload = array_merge($channelRequestLog, $pinArray, $accountSelectionRetryArray);
            $this->safKEProcessor->updateChannelRequestPayload(
                $newPayload,
                $this->msisdn,
                $this->channelRequestID
            );
            $this->formatResponse(
                SAFKEConfigs::SUCCESS,
                SAFKEMessages::SUCCESS_MESSAGE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        } else {
            $this->formatResponse(
                SAFKEConfigs::REQUEST_UNSUCCESSFUL,
                SAFKEMessages::DEFAULT_FAILURE_RESPONSE
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$this->msisdn);
        }
    }
}

