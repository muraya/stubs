<?php

/**
 * This Class Handles  Bill Management features
 *
 * PHP Version 5.4.13
 *
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 */

class Bills
{

    /**
     * The log class instance.
     *
     * @var object
     */
    private $log;

    /**
     * Service/Client code where request is coming from.
     *
     * @var int
     */
    private $fromCode;

    /**
     * Service/Client code where request is going to.
     *
     * @var int
     */
    private $toCode;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    private $tat;

    /**
     * Constructor.
     */

    private $coreUtils;


    private $authorization;
    private $credentials;
    private $data;
    private $dataPacket;
    private $clientID;
    private $serviceID;
    private $accountNumber;
    private $MSISDN;


    /**
     *  Constructor -- Creates an instance of Bills class.
     *  Initializes CoreAppLogger, CoreUtils, Benchmark and Authorization classes.
     */
    public function __construct() {

        $this->log = new CoreAppLogger();
        $this -> coreUtils = new CoreUtils();
        $this->fromCode = "";
        $this->toCode = "";
        $this->tat = new BenchMark(session_id());

        $this -> authorization = new Authorization();

        $this -> initValues();
    }


    /**
     * Receives decoded post data and passes it to the public data object defined
     * just above the constructor
     *
     * Passes clientID to the public clientID passed from the getUserID function
     *
     * Authenticates the passed client token against the one stored in the ClientConfigs
     * Class - Throws a friendlyException of this fails
     * @throws FriendlyException
     */
    public function initValues() {
        $this -> data = CoreUtils::receivePost();

        $this->credentials = $this -> data['payload']['credentials'];
        $this->dataPacket = $this -> data['payload']['packet'];

        $this -> getPacketDetails();

        $this -> getUserClientID($this->credentials['username']);

        $this->fromCode = ClientConfigs::$clientData[$this->clientID]['clientCode'];

        $this->log->debugLog(Config::DEBUG, $this -> MSISDN, "Client ID = ".$this->clientID,$this -> fromCode);
//        $this->authorization->validateToken($this->clientID);

    }

    /**
     * Decodes the first serviceID, accountNumber and msisdn 
     */
    public function getPacketDetails()
    {
        $this->log->debugLog(Config::DEBUG, $this -> MSISDN, "Raw request received : ".$this ->log -> printArray($this -> data),$this -> fromCode);
        foreach ($this -> data['payload']['packet'] as $packetkey => $packetValue)
        {
            $this -> serviceID = $packetValue['serviceID'];
            $this -> accountNumber = $packetValue['accountNumber'];
            $this -> MSISDN = array_key_exists('msisdn', $this -> data['payload']['packet']) ? $packetValue['msisdn'] : '';
            break;
        }
    }

    /**
     * @param $username
     *  Checks if the username of the client exists
     */
    public function getUserClientID($username)
    {
        $clientID =  $this->authorization->getUserclientID($username);
        if ($clientID == false)
        {
            $this->coreUtils -> renderError('Provided username does not exist :'.$username,StatusCodes::CLIENT_AUTHENTICATION_FAILED);
        }
        else
        {
            $this -> clientID = $clientID;
        }
    }

    /**
     *  This function adds function and payload to the data packet instantiated in the
     *   Constructor
     * @author Muraya Duncan<duncan.muraya@cellulant.com>
     *
     * @return array Containing the authentication status and the results
     */
    public function query() {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $this -> data["function"]="BEEP.queryBill";
        $this -> data['payload'] = json_encode($this -> data['payload']);

        $this->log->debugLog(
            Config::DEBUG, $this->MSISDN, "The FINAL response to merchant: ".  json_encode($this->data),
            $this->fromCode, $this->toCode
        );

//        echo json_encode($this->data);
//        exit();

        $jsonResponse = CoreUtils::post(Config::VALIDATION_URL, json_encode($this -> data));

        $this->log->infoLog(
            Config::INFO,
            $this->MSISDN,
            "JSON API function " . __METHOD__ . " response payload: " . json_encode($jsonResponse),
            $this->fromCode,
            Config::VALIDATION_URL
        );

            $this->log->debugLog(
                Config::INFO, $this->MSISDN, "The FINAL response to merchant: ".  $jsonResponse,
                $this->fromCode, $this->toCode
            );

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return json_decode($jsonResponse);
    }


    /**
     * This function validates all the parameters for validating account numbers using set rules
     * passed to it from the API before passing it on to the merchant system for account
     * number verification.
     *
     * @return array Returns a multi-dimensional assoc array
     *
     */
    public function validate() {
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        $this->log->infoLog(
            Config::INFO, $this->MSISDN, "Received  data packet".  json_encode($this->data),
            $this->fromCode, $this->toCode
        );

        $this->data["function"]="BEEP.validateAccount";
        $this->data['payload'] = json_encode($this->data['payload']);

        $jsonResponse = CoreUtils::post(Config::VALIDATION_URL, json_encode($this->data));

        $this->log->infoLog(Config::INFO, $this->MSISDN, "JSON API function " . __METHOD__ . " response payload: " . json_encode($jsonResponse), $this->clientID, Config::VALIDATION_URL);

        $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        return json_decode($jsonResponse,true);
    }

}
