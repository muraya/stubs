<?php

/**
 * This is the class that will handle endpoint invocations by requests that extend it
 * @author ericwanjau
 */
class CoreHandler {
    
    
    
      /**
       * @var CoreUtils
       */      
    private $coreUtils;
    
    private $log;
    /**
     * 
     * @return array returns an array to the Bootstrap router method
     */
    public function process(){
        $this->coreUtils= new CoreUtils();
         $this->log = new CoreAppLogger();
        
        $jsonPayload =  file_get_contents("php://input");
         $requestPayload=  json_decode($jsonPayload, true);
         
        //$this->log->infoLog(Config::INFO, -1 , "payload : ".$this->log->printArray($jsonPayload));
        $this->log->infoLog(Config::INFO, -1 , "payload : ".$jsonPayload);
        return $this->processRequestByClientCode($requestPayload);
     }
     
     /**
      * 
      * @param array $requestPayload
      * @return array
      */
    public function processRequestByClientCode($requestPayload){
             
         $clientCode= self::determineClientCode($requestPayload);
         if(empty($clientCode)){ 
            return $this->coreUtils->renderError("payload can't be processed by ".__CLASS__);
         }else{
            $processor =  $clientCode.'Processor';
             $classProcessor=new $processor;
            return $classProcessor->process($requestPayload);
    
        }
    }
    
        
    /** load all the classes in the clients folder
     * 
     * @param type $clientCode
     * @return null
     */
    public static function loadClassesbyClientCode($clientCode){
        
        if($clientCode==null){
            return;
        }
        $dir =__DIR__. "/clients/".$clientCode.'/';
        $dh  = opendir($dir);
        $dir_list = array($dir);
        
       //Create an array of directories and subdirectories if any
        while (false !== ($filename = readdir($dh))) {
            if($filename!="."&&$filename!=".."&&is_dir($dir.$filename)){
                array_push($dir_list, $dir.$filename."/");
            }
        }
        //loop though each directory and include the files
        foreach ($dir_list as $dir) {
            foreach (glob($dir."*.php") as $filename){
                require_once $filename;
            }    
        }
    }
    
    /** This method will be used to add more clients by checking 
     * for a unique param in the payload and then determine the client
     * @param array $requestPayload
     */
    public static function determineClientCode($requestPayload){
        if(empty($requestPayload)||  !is_array($requestPayload)
                ||count($requestPayload)<1){
            return;
        }
    
        $clientCode=null;
        switch (true) {
            case array_key_exists('TransactionRequest', $requestPayload):
                $clientCode='SAFKE';
                break;
            case array_key_exists('credentials', $requestPayload):
                $clientCode=$requestPayload['credentials']['clientCode'];
                break;
            default:
                $clientCode=null;
                break;
        }
        return $clientCode;
    }

  
    
}
