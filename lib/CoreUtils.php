<?php

/**
 * This file contains core utilities.
 *
 * PHP VERSION 5.3.6
 *
 * @category  Core
 * @package   Core
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @copyright 2013 Cellulant Ltd
 * @license   Proprietary License
 * @link      http://www.cellulant.com
 */

include_once ('db/idiorm.php');
class CoreUtils
{
    /**
     * Replaces the ? with the right parameter for logging purposes.
     *
     * @param string $query the parameterised SQL query
     * @param array $params the SQL query parameters
     *
     * @return string the complete query for logging purposes
     */
    public static function formatQueryForLogging($query, $params)
    {
        try {
            /*
             * Divide the string using the ? delimeter so we can replace
             * correctly.
             */
            $buffer = explode('?', $query);

            $c = count($params);
            for ($index = 1; $index < $c; $index++) {
                /*
                 * Starts from index 1 to ignore the first param. Append back
                 * the ? after it was removed by the explode method.
                 */
                $sub = $buffer[$index - 1] . '?';

                // In each sub string replace the ?
                $buffer[$index - 1] = str_replace('?', $params[$index], $sub);
            }

            // Recontruct the string
            return implode("", $buffer);
        } catch (Exception $ex) {
            // If any thing goes wrong return the original string
            return $query;
        }
    }

    /**
     * Receives the post from API and decodes the JSON string.
     *
     * @return array returns the decoded JSON string or an error message and
     *               appropriate status code if there is an error
     *
     * @author Daniel Mbugua <daniel.mbugua@cellulant.com>
     */
    public static function receivePost()
    {
        $jsonPOST = file_get_contents("php://input");

        if ($jsonPOST == null) {
            // No post
            $authStatus['authStatusCode'] = StatusCodes::GENERIC_FAILURE_STATUS_CODE;
            $authStatus['authStatusDescription'] = "Payload not posted successfully to the processor.";

            $response["authStatus"] = $authStatus;
            $response["results"] = array();

            return $response;

        } else {
            $requestPayload = json_decode($jsonPOST, true);

            if ($requestPayload != null || $requestPayload != false) {

                $coreAppLogger = new CoreAppLogger();
                $coreAppLogger->infoLog(Config::INFO, -1, "Received Payload : ". json_encode($requestPayload));

                return $requestPayload;

            } else {
                // Cannot decode
                $authStatus['authStatusCode'] = StatusCodes::GENERIC_FAILURE_STATUS_CODE;
                $authStatus['authStatusDescription'] = "Internal API server error, "
                    . "payload cannot be decoded. Please Contact the Cellulant support.";

                $response["authStatus"] = $authStatus;
                $response["results"] = array();
                return $response;
            }
        }
    }

    /**
     * Function to format missing parameters in a request into a string to be
     * added to the response description.
     *
     * @param array $missingParams the missing parameters array
     *
     * @return string a string of the concatenated parameters
     *
     * @author Daniel Mbugua <daniel.mbugua@cellulant.com>
     */
    public static function formatMissingParams($missingParams)
    {
        $desc = "";

        $length = count($missingParams);

        foreach ($missingParams as $param)
        {
            if ($length > 1) {
                $desc = $desc . $param . ", ";
            } else {
                $desc = $desc . $param . " ";
            }
            $length++;
        }

        return $desc;
    }


    /**
     * POST the request to Beep Core processor using JSON.
     *
     * @param string $url the Beep Core url
     * @param string $jsonString the post data string
     *
     * @return mixed the response data on success or false on failure
     *
     * @author Titus <titus.luyo@cellulant.com>
     */
    public static function httpJsonPost($url, $jsonString)
    {
        $curl = curl_init($url);

        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, Config::CONNECTION_TIMEOUT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $curl,
            CURLOPT_HTTPHEADER,
            array("Content-type: application/json")
        );
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $jsonString);

        $jsonResponse = curl_exec($curl);

        // Close connection
        curl_close($curl);

        return $jsonResponse;
    }

    /**
     * @param $url
     * @param $fields
     * @param $fields_string
     * @param null $uniqueId
     * @return mixed|string
     *  Posts a http post request
     */
    public static function http_post($url, $fields, $fields_string,$uniqueId = null)
    {

        try {
            //open connection
            $ch = curl_init($url);
            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            //curl_setopt($ch, CURLOPT_MUTE,1);
            curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
            curl_setopt($ch, CURLOPT_POST, count($fields));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);

            //new options
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            //curl_setopt($ch, CURLOPT_CAINFO, REQUEST_SSL_CERTIFICATE);
            //execute post
            $result = curl_exec($ch);
            //log response

            $CoreAppLogger = new CoreAppLogger();

            $CoreAppLogger->infoLog(Config::DEBUG, $uniqueId, " http_post response :: ". $result);

            curl_close($ch);
            return $result;
        } catch (Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * Post functionality
     * @param type $url
     * @param type $data
     * @param type $headers
     * @return type
     */
    public static function curlPost($url, $data, $headers = array()) {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, count($data));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Config::DETAULT_CURL_CONNECTION_TIMEOUT);
        curl_setopt($ch, CURLOPT_TIMEOUT, Config::DETAULT_CURL_READ_TIMEOUT);

        if (isset($headers)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }


    /**
     * Get functionality
     * @param type $url
     * @param type $headers
     * @return string
     */
    public static function curlGet($url, $headers = null) {
        $log = new CoreAppLogger();
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_CUSTOMREQUEST => "GET",
        ));

        if (!empty($headers)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        }

        try {

            $response = curl_exec($curl);
            $log->debugLog(Config::DEBUG, $url , "Response: " . $response);
            curl_close($curl);

            return $response;

        } catch (Exception $ex) {

            $log->errorLog(Config::ERROR, -1, "Curl Error --> "
                . $ex->getMessage() . " File:" . $ex->getFile() . ":" . $ex->getLine());

            curl_close($curl);

            $response = array(
                'statusCode' => StatusCodes::FAILURE,
                'statusDescription' => 'Failed to post to url',
            );

            return $response;
        }

    }


    /**
     *
     *
     *  payload --
     * perform a simple curl post
     * @param type $url
     * @param type $fields
     * @return
     */
    public static function post($url, $fields) {
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Config::DETAULT_CURL_CONNECTION_TIMEOUT);
        curl_setopt($ch, CURLOPT_TIMEOUT, Config::DETAULT_CURL_READ_TIMEOUT);
        $result = curl_exec($ch);

        $curl_errno = curl_errno($ch);
        //close the connection
        curl_close($ch);

        // Static Function -- using $this wont work
        $log = new CoreAppLogger();

        // if we have an error throw an exception
        if($curl_errno){

            $log->errorLog(Config::ERROR, $url, "Response: " . curl_error($ch));

            // ToDo -- Throw an error consisting of the mobile number and the curl error message curl_error_message included

            throw new FriendlyException(Config::DEFAULT_FRIENDLY_RESPONSE); //CURL _RROR_GENERIC_MESSAGE

        }
        return $result;
    }

    /**
     * Renders the final response passed to the function
     * @param $response
     * @param null $uniqueID
     */
    public function renderResponse($response,$uniqueID = null){

        $log = new CoreAppLogger();

        $log->debugLog(Config::INFO, $uniqueID, "RenderingResponse ".  json_encode($response), '', '');

        echo json_encode($response);

        exit();

    }

    /**
     * Appends status code and message to an error response
     * @param null $message
     * @param null $status
     */
    public function renderError($message=null ,$status=null){

        $response=array(
            "authStatus"=>array(
                "authStatusCode"=>($status) ? $status: StatusCodes::GENERIC_FAILURE_STATUS_CODE,
                "authStatusDescription"=>$message,
            ),
            "results"=>array());

        $this -> renderResponse($response,$_GET['url']);
    }

    /**
     *  Creates a Database connection instance using IDIOM ORM
     * @param $dbhost
     * @param $dbname
     * @param $dbpass
     * @param $dbusername
     * @param $connectionName
     */
    public static function loadDBConnection($dbhost, $dbname, $dbpass, $dbusername, $connectionName) {


        $dbstring = 'mysql:host=' . $dbhost . ';dbname=' . $dbname . '';

        //Set up idiorm
        ORM::configure($dbstring, null, $connectionName);
        ORM::configure('username', $dbusername, $connectionName);
        ORM::configure('password', $dbpass, $connectionName);
        ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
        ORM::configure('logging',true, $connectionName);
    }


    /**
     *  Concatenates the passed password and timestamp with password first then timestamp
     *  Hashes the password using sha256
     *  Encodes the generated password using base64
     *  returns the encoded result set
     *
     * @param $password
     * @param $timestamp
     * @return string
     */
    public static function encodePasswordString($password,$timestamp)
    {

        $passwordText = $password.$timestamp;

        $sha256Text = hash('sha256',$passwordText);

        $base64Text = base64_encode($sha256Text);

        return $base64Text;

    }


    /**
     * @param $username
     * @param $password
     * @param $timestamp
     * @return int
     */
    public static function authenticateRequest($username, $password, $timestamp)
    {

        $CoreAppLogger = new CoreAppLogger();

        // check if the username is correct
        // Todo -- Update to Handle multiple MNO's authentication scenario
        if ($username == Config::INCOMING_AUTH_USERNAME)
        {

            // validate password
            $encodedPassword = CoreUtils::encodePasswordString(Config::INCOMING_AUTH_PASSWORD,$timestamp);

            $CoreAppLogger->debugLog(Config::DEBUG, -1, "Validating " . $encodedPassword);

            if ($encodedPassword == $password )
            {
                return SAFKEConfigs::SUCCESS;

            } else {

                $CoreAppLogger->errorLog(Config::ERROR, -1, "Validating password : password mismatch " . $password );

                return SAFKEConfigs::AUTHENTICATION_FAILURE;

            }
        } else {

            $CoreAppLogger->errorLog(Config::ERROR, -1, "Validating username  : username mismatch " . $username);

            return SAFKEConfigs::AUTHENTICATION_FAILURE;

        }

    }


    /**
     * Validates passed mobile number
     * Appends country code to the default standards
     * @param $msisdn
     * @return bool|string
     */
    public static function validateMSISDN($msisdn) {

        //get the country dial code
        $dialCode = Config::DEFAULT_COUNTRY_DIAL_CODE;
        $msisdnLength = Config::DEFAULT_COUNTRY_MSISDN_LENGTH;


        //if the first number is 0
        if (substr($msisdn, 0, 1) == '+' && strlen(substr($msisdn, 1)) == $msisdnLength)
        {
            $msisdn = substr($msisdn, 1);
            return $msisdn;
        }
        //if the first number is 0, add default dial code
        elseif (substr($msisdn, 0, 1) == 0
            && strlen(substr($msisdn, 1)) == ($msisdnLength-strlen($dialCode)))
        {
            $msisdn = $dialCode . substr($msisdn, 1);
            return $msisdn;
        }
        //if the dial code matches the default one & its the expected length
        elseif (substr($msisdn, 0, strlen($dialCode)) == $dialCode && strlen($msisdn) == $msisdnLength)
        {
            return $msisdn;
        }
        else {
            return FALSE;
        }

    }



}
