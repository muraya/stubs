<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 02/03/18
 * Time: 11:23
 */

class Authentication
{
    /**
     *  The log class instance
     * @var
     */
    public $log;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    public $tat;

    /**
     * Constructor.
     */

    public $coreUtils;

    /**
     * Authentication constructor.
     */
    public function __construct()
    {
        $this->log = new CoreAppLogger();
        $this->tat = new BenchMark(session_id());
        $this->coreUtils = new CoreUtils();

    }

    public function decryptBlowfishEncryptedPin($key)
    {
        $encryption = new BlowFishEncryption();
        $decodedCipherText = $encryption->decrypt($key);
        return $decodedCipherText;
        exit();
    }
}