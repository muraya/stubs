<?php
/**
 * Created by PhpStorm.
 * User: mnderitu
 * Date: 26/02/18
 * Time: 15:37
 */

class MulaEncryption
{
    public static function encryptPin($pin, $log)
    {
        $id = 1;

        // $path = '/etc/applications/Encrypt/';
        $path = '/etc/applications/Encrypt/MediaTypes/';
        $keyFileName = 'keyFile.dat';
        $storedKey = self::readFromFile($path, $keyFileName); #invoke function to read from file
        $cipher = 'rijndael-128';      #the cipher to be used to encrpt
        $cipherMode = 'ctr';       #the cipher mode

        if ($id == "") {
            $msg = $pin;

            $encrypted = self::encrypt($cipher, $cipherMode, $msg, $storedKey) or
            $log->debugLog(Config::DEBUG, __FUNCTION__, "Failed to complete PIN encryption for $pin"); #encrypt
        } else {
            $idHash = md5($id);
            $idHashPin = $idHash . $pin;

            $msg = str_pad($idHashPin, 45, "@");

            $encrypted = self::encrypt($cipher, $cipherMode, $msg, $storedKey) or
            $log->debugLog(Config::DEBUG, __FUNCTION__, "Failed to complete PIN encryption"); #encrypt
        }

        return $encrypted;
    }

    public static function encrypt($cipher = null, $cipherMode = null, $msg, $secret_key,
                                   $returnHexed = false, $returnBase64 = false)
    {
        $k = str_replace(array("\r\n", "\n", "\r"), "", $secret_key);
        $returnHexed = true;

        if ($cipher == "rijndael-256") $byteSize = 32; else $byteSize = 16;

        # open cipher module (do not change cipher/mode)
        if (!$td = mcrypt_module_open($cipher, '', $cipherMode, ''))
            return false;

        //$msg = serialize($msg);                                       # serialize #commented since was causing errors
        //$iv  = mcrypt_create_iv($byteSize, MCRYPT_RAND);      # create iv

        $iv = "1234567812345678";


        if (mcrypt_generic_init($td, $k, $iv) !== 0)          # initialize buffers
            return false;

        $msg = mcrypt_generic($td, $msg);                      # encrypt
        $msg = $iv . $msg;                                     # prepend iv
        $mac = self::pbkdf2($msg, $k, 1000, 32);              # create mac
        //$msg .= $mac;                                         # append mac

        mcrypt_generic_deinit($td);                             # clear buffers
        mcrypt_module_close($td);                               # close cipher module

        if ($returnHexed) $msg = bin2hex($msg);
        else if ($returnBase64) $msg = base64_encode($msg);
        return $msg;
    }


    /** function to read from file **/
    private static function readFromFile($path, $fileName)
    {
        //      echo "\n initializing read from file";
        $myFile = $path . "" . $fileName;
        $fh = fopen($myFile, 'r') or die("cant open file");
        $theData = fread($fh, filesize($myFile));
        fclose($fh);
        //      echo "\nData read from file - ".$theData;
        return $theData;
    }


    public static function pbkdf2($p, $s, $c, $kl, $a = 'sha256')
    {

        $hl = strlen(hash($a, null, true));                             # Hash length
        $kb = ceil($kl / $hl);                                          # Key blocks to compute
        $dk = '';                                                       # Derived key

        # Create key
        for ($block = 1; $block <= $kb; $block++) {

            # Initial hash for this block
            $ib = $b = hash_hmac($a, $s . pack('N', $block), $p, true);

            # Perform block iterations
            for ($i = 1; $i < $c; $i++)

                # XOR each iterate
                $ib ^= ($b = hash_hmac($a, $b, $p, true));

            $dk .= $ib; # Append iterated block
        }

        # Return derived key of correct length
        return substr($dk, 0, $kl);
    }


}