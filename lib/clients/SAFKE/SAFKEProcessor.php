<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 28/02/18
 * Time: 13:32
 */

class SAFKEProcessor
{

    /**
     * The log class instance.
     *
     * @var object
     */
    public $tat;
    public $log;
    public $toCode;
    public $fromCode;

    /**
     * SAFKEProcessor constructor.
     */
    public function __construct()
    {
        $this->tat = new BenchMark(session_id());
        $this->fromCode = "";
        $this->toCode = "";
    }

    /**
     * @param $requestPayload
     * @return array|int|string
     */
    public function process($requestPayload)
    {
        $commandID = $requestPayload['TransactionRequest']['requestHeader']['cid'];
        $response=array();
        switch ($commandID) {
            case "fetchMyFI":
                $response= $this->fetchMyFI();
                break;
            case "activate":
                $response= $this->activate();
                break;
            case "FinancialIstitutionToMpesa":
                $response= $this->processB2C();
                break;
            case "FinacialInstitutionToMpesa":
                $response= $this->processB2C();
                break;
            case "authUSSDCallback":
                $response= $this->processUSSDCallback();
                break;
            case "authCallback":
                $response= $this->processUSSDCallback();
                break;
            default:
                $response=$this->renderInvalidCommandID();
        }
        return $response;
    }


    /**
     * Method to generate a time based password
     * @param $timestamp
     */
    public function generateTimebasedPassword($timestamp)
    {
        //tod copy from dmuraya
        $text = SAFKEConfigs::DEFAULT_PASSWORD.$timestamp;
        $password = base64_encode(hash('sha256', $text));
        echo  $password;
        exit();
    }

    /**
     * A stub to simulate response for a USSDCALLBACK
     * @return array
     */
    public function processUSSDCallBack()
    {
        $payload = array(
            "AuthResponse" => array(
                "responseHeader" => array(
                    "txid" => "BNK1231231231",
                    "dt" => "20170615082020"
                ),
                "responseData" => array(
                    "rcode" => 200,
                    "rtext" => "Successfully processed"
                )
            )
        );

        return $payload;
    }


    public function processB2C()
    {
        return array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "SFC1231231231",
                    "dt" => "20170615082020"
                ),
                "responseData" => array(
                    "rcode" => 200,
                    "rtext" => "Successfully processed"
                )
            )
        );

    }

    /**
     * Method to fetch bank profiles
     * @return int|string
     */
    public function fetchMyFI()
    {
        $bankProfiles=new BankProfiles();
        return $bankProfiles->get();

    }
    public function activate()
    {
        $payload = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "SFC1231231231",
                    "dt" => "20170615082020"
                ),
                "responseData" => array(
                    "rcode" => 200,
                    "rtext" => "Successfully processed activate request"
                )
            )
        );

        return $payload;
    }


    public function renderInvalidCommandID()
    {
        return array(
            "TransactionResponse" => array(
                "header" => array(
                    "txid" => null,
                    "dt" => date('Ymdhis')
                ),
                "responseData" => array(
                    "rcode" => SAFKEConfigs::AUTHENTICATION_FAILURE,
                    "rtext" => "Invalid  command ID used"
                )
            )
        );
    }

    /**
     * Method to format the array received from safaricom
     * @param $data
     * @return array
     */
    public static function formatRequestData($data)
    {
        $formatedData=array();
        foreach ($data as $item) {
            $key=$item['key'];
            $value=$item['value'];
            $formatedData[$key]=$value;
        }

        return $formatedData;
    }

    /**
     * Method to formulate the mobile banking pin prompt message
     * @param $data
     * @return mixed
     */
    public static function formulateMBankingPinPrompt($data)
    {

        $baseMessage  = Responses::B2C_PIN_PROMPT_TEMPLATE;
        $hashMaps = array("#AMOUNT#", "#BANK#", "#MSISDN#","#CHARGE#");
        $hashMapValues   = array($data['amount'], $data['bank'], $data['msisdn'], $data['charge']);

        $formattedMessage = str_replace($hashMaps, $hashMapValues, $baseMessage);

        return $formattedMessage;


    }

    /**
     * Method to fetch the channel request data for the defined request
     * @param $txid
     * @param $msisdn
     * @return array|bool
     */
    public function getChannelRequestData($txid, $msisdn)
    {
        $this->log = new CoreAppLogger();
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        coreUtils::loadDBConnection(
            Config::HUB_HOST,
            Config::HUB_DB,
            Config::HUB_PASS,
            Config::HUB_USER,
            'hub'
        );
        try {
            $channelRequestData = ORM::for_table('c_channelRequests', 'hub')
                ->whereRaw(
                    '(dateCreated > DATE_SUB(NOW(), INTERVAL ? SECOND))',
                    array(Config::DEFAULT_FETCH_QUERY_TIMEFRAME)
                )
                ->where('gatewayUID', $txid)
                ->where('MSISDN', $msisdn)
                ->selectMany('payload', 'channelRequestID', 'statusDescription', 'overalStatus')
                ->find_one();
            if ($channelRequestData) {
                $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
                return $channelRequestData->asArray();
            } else {
                return false;
            }

        } catch (Exception $exception) {
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
            $this->log->debugLog(
                Config::ERROR,
                $msisdn,
                "Error getting the channel request data" . $exception->getMessage(),
                $this->fromCode,
                $this->toCode
            );
        }

    }

    /**
     * Method to formulate our payload to saf format of an array
     * @param $data
     * @return array
     */
    public static function generateSafKeyValueFormat($data)
    {
        $formatedArray=array();
        foreach ($data as $key => $value) {
            $formatedArray[]=array('key'=>$key,'value'=>$value);
        }
        return $formatedArray;
    }

    /**
     * Method to update the channel request payload
     * @param $txid
     * @param $payload
     * @param $msisdn
     */
    public function updateChannelRequestPayload($payload, $msisdn, $channelRequestID)
    {
        $this->log = new CoreAppLogger();
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        coreUtils::loadDBConnection(
            Config::HUB_HOST,
            Config::HUB_DB,
            Config::HUB_PASS,
            Config::HUB_USER,
            'hub'
        );

        try {
            ORM::raw_execute(
                "UPDATE c_channelRequests SET payload = ? "
                . "WHERE channelRequestID = ?",
                array (
                    json_encode($payload), $channelRequestID
                ),
                "hub"
            );
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        } catch (Exception $exception) {
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
            $this->log->debugLog(
                Config::ERROR,
                $msisdn,
                "Error updating the channel request data" . $exception->getMessage(),
                $this->fromCode,
                $this->toCode
            );
        }
    }

    /**
     * Method to update the channel request overall status
     * @param $txid
     * @param $rcode
     * @param $msisdn
     * @param $overalStatusCode
     * @param $overalStatusMessage
     * @param $newPayload
     * @param $channelRequestID
     */
    public function updateChannelRequestOverallStatus(
        $rcode,
        $msisdn,
        $overalStatusCode,
        $overalStatusMessage,
        $channelRequestID,
        $newPayload
    ) {

        $this->log = new CoreAppLogger();
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        coreUtils::loadDBConnection(
            Config::HUB_HOST,
            Config::HUB_DB,
            Config::HUB_PASS,
            Config::HUB_USER,
            'hub'
        );

        try {
            $statusDescription = $rcode .'|'. $overalStatusMessage;
            if ($newPayload != null) {
                ORM::raw_execute(
                    "UPDATE c_channelRequests SET overalStatus = ?, statusDescription = ?, "
                    . "dateClosed = NOW(), payload = ? "
                    . "WHERE channelRequestID = ? ",
                    array (
                        $overalStatusCode, $statusDescription, json_encode($newPayload), $channelRequestID
                    ),
                    "hub"
                );
            } else {
                ORM::raw_execute(
                    "UPDATE c_channelRequests SET overalStatus = ?, statusDescription = ?, "
                    . "dateClosed = NOW() "
                    . "WHERE channelRequestID = ?",
                    array (
                        $overalStatusCode, $statusDescription, $channelRequestID
                    ),
                    "hub"
                );
                //ORM::for_table('c_channelRequests', 'hub')
                //      ->use_id_column('channelRequestID')
                //    ->where('channelRequestID', $channelRequestID)
                //  ->find_result_set()
                //->set('overalStatus', $overalStatusCode)
                //->set('statusDescription', $statusDescription)
                //->set_expr('dateClosed', 'NOW()')
                //->save();
            }

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        } catch (Exception $exception) {
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
            $this->log->debugLog(
                Config::ERROR,
                $msisdn,
                "Error updating the channel request data" . $exception->getMessage(),
                $this->fromCode,
                $this->toCode
            );
        }
    }

    /** Generate a 32 character blowfishKey
     * @param int $MSISDN
     * @param string $safTxID
     * @return string
     */
    public static function getBlowfishKey($MSISDN, $safTxID){

        $string= hash('sha256',$safTxID.$MSISDN.$safTxID);

        return substr($string, 0,32);
    }

    /**
     * Method to formulate the inactive/blcocked account message
     * @param $data
     * @return mixed
     */
    public static function formulateBlockedAccountMessage($data)
    {

        $baseMessage  = Responses::BLOCKED_INACTIVE_ACCOUNT_MESSAGE;
        $hashMaps = array("#CONTACT_NUMBER#");
        $hashMapValues   = array($data);

        $formattedMessage = str_replace($hashMaps, $hashMapValues, $baseMessage);

        return $formattedMessage;
    }

    public function storeClientAccounts($statusCode, $statusDescription, $channelRequestID, $msisdn)
    {
        $this->log = new CoreAppLogger();
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        coreUtils::loadDBConnection(
            Config::HUB_HOST,
            Config::HUB_DB,
            Config::HUB_PASS,
            Config::HUB_USER,
            'hub'
        );

        try {
            ORM::raw_execute(
                "UPDATE c_channelRequests SET overalStatus = ?, statusDescription = ?, "
                . "lastSend=NOW() "
                . "WHERE channelRequestID = ?",
                array (
                    $statusCode, $statusDescription, $channelRequestID
                ),
                "hub"
            );

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
        } catch (Exception $exception) {
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__.'|'.$msisdn);
            $this->log->debugLog(
                Config::ERROR,
                $msisdn,
                "Error updating the channel request data" . $exception->getMessage(),
                $this->fromCode,
                $this->toCode
            );
        }
    }

}
