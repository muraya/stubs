<?php


require __DIR__ . '/predis/autoload.php';

Predis\Autoloader::register();

class RedisController {

    /**
     * @var \Predis\Client
     *  Holds Redis connection instance
     */
    private $redis;

    /**
     *  @var key
     *  Holds the passed key value
     */
    private  $key;

    /**
     * RedisController constructor.
     * @throws FriendlyException
     *  Initializing Base classes
     */
    public function __construct(){
        $this->log = new CoreAppLogger();
        $this->tat = new BenchMark(session_id());

        $this->init();
    }

    /**
     * @throws FriendlyException
     *  Initializes redis connection instance
     */
    public function init()
    {
        try {
            $this->redis = new Predis\Client(
                array(
                    "scheme" => Config::REDIST_TCP ,
                    "host" => Config::REDIST_HOST,
                    "port" => Config::REDIS_PORT,
                    "password" => Config::REDIS_PASSWORD,
                    "persistent" => Config::REDIS_PERSISTENT
                )
            );

        } catch (\Predis\Connection\ConnectionException $connectionException ) {

            $this->log->errorLog(Config::ERROR, -1, "Connection Exception ::" .$connectionException->getMessage());

            die($connectionException->getMessage());

        } catch (Exception $e) {

            $this->log->errorLog(Config::ERROR, -1, "General Exception ::" .$e);

            throw new FriendlyException('Could not connect to through Redis');
        }
    }

    /**
     * @param $key
     * @return bool|string
     *  Gets the value attached to the passed redis key
     */
    public function getKeyValue($key)
    {
        try{
            $result = $this->redis->hgetall($key);
            if (!empty($result))
            {
                $this->log->infoLog(Config::INFO, $key, " Key value ::  " . $this->log->printArray($result));
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $this->log->errorLog(Config::ERROR, $key, "Error Getting Key Value : " . $e->getMessage());
            return false;
        }
    }

    /**
     * @param $key
     * @param $field
     * @return bool|string
     * Returns the value attached to the passed key and field details
     */
    public function getKeyFieldValue($key, $field)
    {
        try{
            $result=$this->redis->hget($key, $field);
            $this->log->infoLog(Config::INFO,$key, " Value for field ".$field.' :: ' . $result);
            return $result;

        } catch (Exception $e) {
            $this->log->errorLog(Config::ERROR, $key, "Getting Key Field Value Error : " . $e->getMessage());
            return false;
        }
    }

    /**
     * @param $key
     * @param $value
     * @return bool
     * Sets a redis key value from the passed key and value details
     */
    public function setKeyValue($key,$value)
    {
        try{
            $this->redis->set($key, $value);
            return true;
        } catch (Exception $e) {
            $this->log->errorLog(Config::ERROR, $key, " Setting Key Value Error : " . $e->getMessage());
            return false;
        }
    }

    /**
     * @param $key
     * @param $field
     * @param $value
     * @return bool
     *  Sets a redis instance value on the passed key and field details
     */
    public function setKeyFieldValue($key, $field, $value)
    {
        try{
            $this->redis->hSet($key,$field, $value);
            return true;
        }catch (Exception $e){
            $this->log->errorLog(Config::ERROR, $key, "Setting Field Value Error :: ".$this->log ->printArray($value) ." :: ". $e->getMessage());
            return false;
        }
    }

    /**
     * @param $key
     * @param $fieldsValuesArray
     * @return bool
     *  Sets defined fields and their values to the assigned key
     */
    public function setKeyMultipleValues($key, $fieldsValuesArray)
    {
        try{
            $this -> redis -> hmset($key, $fieldsValuesArray);
            return true;
        } catch (Exception $ex) {
            $this->log->errorLog(Config::ERROR, $key, "Setting Fields Values Error :: ".$this->log ->printArray($fieldsValuesArray) ." :: ". $ex->getMessage());
            return false;
        }

    }

    /**
     * @param $key
     * @param $time
     * @return bool
     * used when we want to set an expiry time frame to redis record
     */
    public function expireRedisRecord($key, $time)
    {
        try {
            $this->redis->expire($key, $time);
            return true;
        }catch (Exception $exception){
            $this->log->errorLog(
                Config::ERROR,
                $key,
                "Setting Fields Values Error :: ".$this->log ->printArray($time) ." :: ". $exception->getMessage()
            );
            return false;
        }
    }

    /**
     * @param $pattern
     *  Delete multiple keys and values with the matching pattern
     */
    protected function deleteKetMatch($pattern)
    {
        $this ->redis->del($this ->redis->keys( ''.$pattern.''));

        die();

    }

}
