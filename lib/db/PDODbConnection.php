<?php

/**
 * Created by PhpStorm.
 * User: mikie
 * Date: 9/22/15
 * Time: 11:26 PM
 */

require_once __DIR__ . '/../../../vendor/autoload.php';

use App\Lib\Logger\BeepLogger;
use App\Lib\Conf\Config;
use \PDO;
use \PDOException;


class PDODbConnection
{
    /**
     * Database connection variable
     */
    private $dbConnection;
    /**
     * logging
     */
    private $log;

    public function __construct()
    {
        //initialize the logs;
        $this->log = new BeepLogger();
        /**
         * dsn this represents Data source name,specifies the host,port and database name
         */
        $dsn = 'mysql:host=' . Config::HOST . ';
                port=' . Config::PORT . ';
                dbname=' . Config::DATABASE . '';

        try {
            //create an instance of the PDO
            $this->dbConnection = new PDO(
                $dsn,
                Config::USER,
                Config::PASSWORD,
                array(
                    PDO::ATTR_PERSISTENT => true //allow persistent connections on pdo
                )
            );

            //enable listening for all PDO exceptions
            $this->dbConnection->setAttribute(
                PDO::ATTR_ERRMODE,
                PDO::ERRMODE_EXCEPTION
            );

            $this->log->info(
                Config::INFO_LOG_FILE,
                -1,
                "Successfully initialized database connection"
            );
        } catch (PDOException $pdoException) {
            $this->log->error(Config::ERROR_LOG_FILE,
                -1,
                "Could not establish database connection: " . $pdoException->getMessage());
            die();
        }
    }


    /**
     * Gets an instance of the dbConnection
     * @return PDO of the connection
     */
    public function getConnection()
    {
        return $this->dbConnection;
    }

}