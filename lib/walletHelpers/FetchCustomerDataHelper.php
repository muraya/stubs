<?php


/**
 * FetchCustomerDataHelper invokes wallet to fetch customer data for a customer
 *
 * @author ericwanjau
 */
class FetchCustomerDataHelper {


    private $log;

    public function __construct()
    {
        $this ->log = new CoreAppLogger();
    }
    //put your code here

    public function queryWallet($MSISDN, $username, $password, $apiUrl, $logger)
    {
        $fields = array(
            "RESPONSE_TYPE" => 'JSON',
            "MSISDN" => $MSISDN,
            "USERNAME" => $username,
            "PASSWORD" => $password,
        );

        $fields_string = '';

        foreach ($fields as $key => $value) {
            $fields_string .= ($fields_string === "" ? "" : "&");
            $fields_string .= $key . '=' . $value;
        }

        //send request to wallet
        $postRequest = CoreUtils::http_post($apiUrl, $fields, $fields_string, $MSISDN);

//        $string = "{\"SUCCESS\":true,\"customerDetails\":\"179|1|1|customer|Levis|2018-04-11 16:29:40\",\"accountDetails\":\"296|5106556001|mungaiUSD|US Dollar |840|USD |179|1#970|0100023008|Test Ac 2|Kenyan Shilling |404|KES |179|2\",\"nominationDetails\":\"brian|0110503002|NATION CENTRE|DTB Kenya|DTB|001|IFT#Liz|0104928048|NATION CENTRE|DTB Kenya|DTB|001|IFT\",\"EXCEPTION\":null}";

        $profileDetails=  json_decode($postRequest, true);

       // $profileDetails=  json_decode($string, true);

        if (!empty($profileDetails['accountDetails'])) {
            //Handle successful accounts fetch details and formulate required payload
            $accountData = $profileDetails['accountDetails'];
            $customerAccounts = explode("#" , $accountData);

            $this->log->debugLog(Config::DEBUG, $apiUrl, "Wallet Profile Details :: " .json_encode($profileDetails));

            $accountDetailsArray=array();
            $accountCount = 1;
            foreach ($customerAccounts as $accounts) {
                $customerAccountData = explode("|", $accounts);
                $accountarr = array(
                    "accountID" => $customerAccountData[0],
                    "accountNumber" => $customerAccountData[1],
                    "accountAlias" => $customerAccountData[2],
                );

                $accountDetailsArray[$accountCount] = $accountarr;
                $accountCount++;
            }

            return $accountDetailsArray;
        } else {
            return array();
        }

    }
}
