<?php

/**
 * Created by PhpStorm.
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 * Date: 1/23/18
 * Time: 1:55 PM
 * This is a class to validate passed client token against the Configured one
 */
class TokenValidation
{


    /**
     * @var CoreAppLogger
     *  CoreAppLogger Class instance
     */
    public $log;

    /**
     * TokenValidation constructor.
     */
    public function __construct()
    {
        $this -> log = new CoreAppLogger();
    }

    /**
     *  Gets the passed client token and matches againist with the stored
     *  client tokens on the ClientConfigs class.
     * @param $clientID
     * @param $requestToken
     * @return bool
     * @throws FriendlyException
     * 
     */
    public function validateToken($clientID,$requestToken)
    {
        if($requestToken == ClientConfigs::$clientData[$clientID]['clientToken']) {
            return true;
        } else{
            $this->log->debugLog(Config::DEBUG, $clientID, "Token Validation Failed ".$requestToken);
            return false;
        }
    }

    /**
     * @return TokenValidation
     * Model class
     */
    public static function model(){
        return new TokenValidation();
    }

}