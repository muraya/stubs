<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 2/7/18
 * Time: 4:14 PM
 */

require __DIR__ . '/phpseclib/autoload.php';

/**
 * This Class Handles Handles RSA RSAEncryption instances from key generation,
 * RSAEncryption and Decryption of passed texts.
 *
 * PHP Version 5.4.13
 *
 * @category Core
 * @package  Hub_Services
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 */


class RSAEncryption
{

    /**
     * The log class instance.
     *
     * @var object
     */
    public $log;

    /**
     * The RSA class instance.
     *
     * @var object
     */
    private $rsa;

    /**
     *  BLOWFISH class instance
     * @var
     */
    private $blowfish;


    /**
     * holds the key size of the RSA algorithm
     *
     * @var object
     */
    private $keyLength;


    /**
     * Holds the keyload instance which consists of the
     *  public and the private key
     *
     * @var object
     */
    private $keyload;


    /**
     * Holds the public key instance.
     *
     * @var object
     */
    private $publicKey;

    /**
     * Creates the private key instance.
     *
     * @var object
     */
    private $privateKey;


    /**
     * RSAEncryption constructor.
     */
    public function __construct()
    {
        $this -> log = new CoreAppLogger();
        $this -> rsa = new \phpseclib\Crypt\RSA();
        $this->blowfish = new \phpseclib\Crypt\Blowfish();

        $this -> getKeyPair();
    }


    /**
     * Gets the stored public and private keys from the text files
     * Appends the key values into the private variables
     *
     */
    public function getKeyPair()
    {

        $this->publicKey = $this -> getKey('public');

        $this->privateKey = $this -> getKey('private');

        if ($this->publicKey == false or $this->privateKey == false)
        {
            $this->log->debugLog(
                Config::INFO, -1, "Generating key pair",
                '', ''
            );

            $this->generateKeyPair();
        }
        else{
            // debug true statement
            $this->log->debugLog(
                Config::INFO, -1, "Keys fetched successfully ",
                '', ''
            );

        }

        return true;

    }


    /**
     *  Gets the key content from the text file and returns the string text as
     *  $keyFileContent
     * @param $keyType
     * @return bool|string
     */
    public function getKey($keyType)
    {

        $keyFile = fopen(Config::RSA_KEY_PATH."$keyType.txt", "r");

        if ($keyFile)
        {

            $keyFileContent = file_get_contents(Config::RSA_KEY_PATH."$keyType.txt");

            if ($keyFileContent && !empty($keyFileContent)){

                fclose($keyFile);
                return $keyFileContent;

            } else {

                fclose($keyFile);

                $this->log->debugLog(
                    Config::INFO, -1, " Unable to $keyType key read successfully",
                    '', ''
                );

                return false;

            }
        } else{

            fclose($keyFile);

            $this->log->debugLog(
                Config::INFO, -1, " Could not open $keyType key file successfully",
                '', ''
            );

            return false;

        }
    }


    /**
     *  Generates the key instances , in this case both the private and public
     * @return int|object
     */
    public function generateKeyPair()
    {

        $this->log->debugLog(
            Config::INFO, -1, "Generating key Pair here",
            '', ''
        );

        $this->keyload=  $this->rsa->createKey(Config::RSA_KEY_SIZE);

        $this->publicKey = $this->keyload["publickey"];
        $this->privateKey = $this->keyload["privatekey"];

        $this->saveKeyText($this->publicKey,'public',Config::RSA_KEY_PATH);
        $this->saveKeyText($this->privateKey,'private',Config::RSA_KEY_PATH);


        $this->log->debugLog(
            Config::INFO, -1, "New Key already generated as : ".$this->publicKey,
            '', ''
        );

        return $this->keyload;

    }


    /**
     * Encrypts plain text into cipherText using the passed public key in this case
     * returns the cipherText
     * @param $plainText
     * @return string
     */
    public function encodePlainText($plainText,$algorithm = null)
    {
        if ($algorithm == 'BLOWFISH'){
            $this->blowfish->setKey(Config::BLOW_FISH_KEY); // public key
            $ciphertext = $this->blowfish->encrypt($plainText);
            return base64_encode($ciphertext);

        }else{
            $this->rsa->loadKey($this->publicKey); // public key
            $ciphertext = $this->rsa->encrypt($plainText);

        }

        return  base64_encode($ciphertext);
    }


    /**
     *  Decrypts the cipherText back to plain text using the stored  RSA private key
     *  Returns the decrypted cipherText in this case the plain text.
     * @param $ciphertext
     * @return string
     */
    public function decodeCipherText($ciphertext,$algorithm = null)
    {
        if ($algorithm == 'BLOWFISH'){
            $this->blowfish->setKey(Config::BLOW_FISH_KEY); // public key
            $ciphertext = base64_decode($ciphertext);
            return $this->blowfish->decrypt($ciphertext);

        }else{
            $this->rsa->loadKey($this->privateKey); // private key
            return $this->rsa->decrypt($ciphertext);
        }

    }


    /**
     *  Saves passed key text to .txt file on the passed file path
     *
     * Returns true on success
     * @param $key
     * @param $filename
     * @param $path
     *
     */
    public function saveKeyText($key, $filename, $path)
    {
        //get the file
        $keyFile = fopen($path.$filename.".txt", "w");
        $status = fwrite($keyFile, $key);

        fclose($keyFile);
    }
    
}