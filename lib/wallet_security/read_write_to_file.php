<?php
class read_write_to_file {

/** function to write to file **/
	public function writeToFile($path,$fileName,$dataToWrite){
	//	echo "\n Initializing writing to file";
		
		$myFile = $path."".$fileName;
		$fh = fopen($myFile, 'w') or die("\ncan't open file");
		fwrite($fh, $dataToWrite);
		fclose($fh);

	}


/** function to read from file **/
	public function readFromFile($path,$fileName){
	//	echo "\n initializing read from file";
		$myFile = $path."".$fileName;
		$fh = fopen($myFile, 'r') or die("cant open file");
		$theData = fread($fh, filesize($myFile));
		fclose($fh);
	//	echo "\nData read from file - ".$theData;
		return $theData;
	}

}
