<?php

class MCrypt
{
    private $iv = 'fedcba9876543210';//'fedcba9876543210'; #Same as in JAVA
    private $key = 'tQna25tR89d6af1a';//'0123456789abcdef'; #Same as in JAVA

   public function __construct() {

    }

 

    public function encrypt($str) { 
      $str = $this->pkcs5_pad($str);   
      $iv = $this->iv; 
      $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv); 
      mcrypt_generic_init($td, $this->key, $iv);
      $encrypted = mcrypt_generic($td, $str); 
      mcrypt_generic_deinit($td);
      mcrypt_module_close($td); 
      return bin2hex($encrypted);
    }
    public function encryption($code)
    {
        $blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
        $encrypt= $this->pkcs7pad($code,$blocksize); 
        $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
        $passcrypt = trim(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key, trim($encrypt), MCRYPT_MODE_ECB, $iv));
        $encode = base64_encode($passcrypt);
      return $encode;
    }

 
    public function decrypt($code) { 

      $decoded = base64_decode($code);
      $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB), MCRYPT_RAND);
      $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, trim($decoded), MCRYPT_MODE_ECB, $iv));
      $blocksize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

     CoreUtils::flog2(Configs::INDEX_LOG_DIR, 4, "ENCRYPT POST DECRYPTED | >>>> bbbbbbbb vvvvvvvvvvv " . $this->pkcs7unpad($decrypted,$blocksize));
     return  $this->pkcs7unpad($decrypted,$blocksize);
    }
    
    /**
 * Validates and unpads the padded plaintext according to PKCS#7.
 * The resulting plaintext will be 1 to n bytes smaller depending on the amount of padding,
 * where n is the block size.
 *
 * The user is required to make sure that plaintext and padding oracles do not apply,
 * for instance by providing integrity and authenticity to the IV and ciphertext using a HMAC.
 *
 * Note that errors during uppadding may occur if the integrity of the ciphertext
 * is not validated or if the key is incorrect. A wrong key, IV or ciphertext may all
 * lead to errors within this method.
 *
 * The version of PKCS#7 padding used is the one defined in RFC 5652 chapter 6.3.
 * This padding is identical to PKCS#5 padding for 8 byte block ciphers such as DES.
 *
 * @param string padded the padded plaintext encoded as a string containing bytes
 * @param integer $blocksize the block size of the cipher in bytes
 * @return string the unpadded plaintext
 * @throws Exception if the unpadding failed
 */ 
    function pkcs7unpad($padded, $blocksize)
    {
        $l = strlen($padded);

        if ($l % $blocksize != 0) 
        {
            throw new Exception("Padded plaintext cannot be divided by the block size");
        }

        $padsize = ord($padded[$l - 1]);

        if ($padsize === 0)
        {
            throw new Exception("Zero padding found instead of PKCS#7 padding");
        }    

        if ($padsize > $blocksize)
        {
            throw new Exception("Incorrect amount of PKCS#7 padding for blocksize");
        }

        // check the correctness of the padding bytes by counting the occurance
        $padding = substr($padded, -1 * $padsize);
        if (substr_count($padding, chr($padsize)) != $padsize)
        {
            throw new Exception("Invalid PKCS#7 padding encountered");
        }

        return substr($padded, 0, $l - $padsize);
    }

    /**
 * Right-pads the data string with 1 to n bytes according to PKCS#7,
 * where n is the block size.
 * The size of the result is x times n, where x is at least 1.
 * 
 * The version of PKCS#7 padding used is the one defined in RFC 5652 chapter 6.3.
 * This padding is identical to PKCS#5 padding for 8 byte block ciphers such as DES.
 *
 * @param string $plaintext the plaintext encoded as a string containing bytes
 * @param integer $blocksize the block size of the cipher in bytes
 * @return string the padded plaintext
 */
  protected function pkcs7pad($plaintext, $blocksize)
  {
      $padsize = $blocksize - (strlen($plaintext) % $blocksize);
      return $plaintext . str_repeat(chr($padsize), $padsize);
  }
 
    protected function hex2bin($hexdata) {
      $bindata = ''; 
      for ($i = 0; $i < strlen($hexdata); $i += 2) {
          $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
      } 
      return $bindata;
    } 
 
    protected function pkcs5_pad ($text) {
      $blocksize = 16;
      $pad = $blocksize - (strlen($text) % $blocksize);
      return $text . str_repeat(chr($pad), $pad);
    }
 
    protected function pkcs5_unpad($text) {
      $pad = ord($text{strlen($text)-1});
      if ($pad > strlen($text)) {
          return false; 
      }
      if (strspn($text, chr($pad), strlen($text) - $pad) != $pad) {
          return false;
      }
       $str = substr($text, 0, -1 * $pad); 
      return rtrim($str);
    }
}

?>
