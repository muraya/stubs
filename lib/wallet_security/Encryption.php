<?php

$wwwPath = str_ireplace("/html", "", $_SERVER['DOCUMENT_ROOT']);
$wwwPath = $_SERVER['DOCUMENT_ROOT'];
require_once("read_write_to_file.php");
require_once("encryption_decryption(with+iv).php");

//require_once $wwwPath . "/_configs/coreUtils.php";
class Encryption {

    function encryptPin($pin, $id = "")
    {
        $flogpath = "pinEncryption";
        $read_write_to_file = new read_write_to_file;    #instantiate read and write class
        $path = '/etc/applications/Encrypt/MediaTypes/';///etc/applications/Encrypt/';
       // error_log(">>>>>>encrytpt");
        //$path = '/etc/applications/Encrypt/';
        $keyFileName = 'keyFile.dat';//'keyFile.dat';
           $storedKey = $read_write_to_file->readFromFile($path, $keyFileName); #invoke function to read from file
        //$storedKey = '4f2afcea680ddc1a';
        $cipher = 'rijndael-128';      #the cipher to be used to encrpt
        $cipherMode = 'ctr';       #the cipher mode

        $encryprion_decryption = new encryption_decryption;   #instantiate encryption class

        if ($id == "") {
            //encrypt plain pin
//        flog("$flogpath", "encrypting without masking for pin $pin");
            $msg = $pin;

            $encrypted = $encryprion_decryption->encrypt($cipher, $cipherMode, $msg, $storedKey) or flog("$flogpath", "failed to complete PIN encryption for $pin"); #encrypt
        } else {
            //mask pin first before encrypting
//        flog("$flogpath", "encrypting with masking");

            $idHash = md5($id);
            $idHashPin = $idHash . $pin;

            $msg = str_pad($idHashPin, 45, "@");


            $encrypted = $encryprion_decryption->encrypt($cipher, $cipherMode, $msg, $storedKey) or flog("$flogpath", "failed to complete PIN encryption"); #encrypt
//        /$originalPin = decryptPin($encrypted);
        }
//    flog("$flogpath", "the encrypted pinhash is " . $encrypted);
        return $encrypted;
    }

    function decryptPin($hash) {

        #decode and set the message

        $path = '/etc/applications/Encrypt/MediaTypes/';  #path to the secret key file
        $keyFileName = 'keyFile.dat';     #key file name

        $cipher = 'rijndael-128';     #decryption cipher
        $cipherMode = 'ctr';      #decryption cipher mode


        $read_write_to_file = new read_write_to_file;   #instantiate the file input output class

        $storedKey = $read_write_to_file->readFromFile($path, $keyFileName); #read the stored key

        $encryprion_decryption = new encryption_decryption;   #instantiate the decryption class
        $decrypted = $encryprion_decryption->decrypt($cipher, $cipherMode, $hash, $storedKey) or die("Failed to complete decryption.\n"); #decrypt
        // The 3 line below have been commented, their usefulness is not known
        // $idHash = $decrypted;
        // $pinPad = substr($idHash, 32);
        // $pin = str_replace("@", "", $pinPad);

        return $decrypted;
    }

    function decryptMessage($encryptedMessage, $keyFileName) {


        #decode and set the message

        $path = '/etc/applications/Encrypt/MediaTypes/';  #path to the secret key file
        if (!$keyFileName)
            $keyFileName = '7a733c0a9206434fd36aa12a9504ce59.dat';#key file name

        $cipher = 'rijndael-128';     #decryption cipher
        $cipherMode = 'ctr';      #decryption cipher mode


        $read_write_to_file = new Read_Write_To_File;   #instantiate the file input output class

        $storedKey = $read_write_to_file->readFromFile($path, $keyFileName); #read the stored key

        echo $storedKey;

        $encryprion_decryption = new Encryption_Decryption;   #instantiate the decryption class
        $decrypted = $encryprion_decryption->decrypt($cipher, $cipherMode, $encryptedMessage, $storedKey) or die("Failed to complete decryption.\n"); #decrypt
//return $decrypted;

        return $encryptedMessage;
    }

}

//$enc = new RSAEncryption();
//echo "Encrypted: " . $enc->encryptPin("dtbug_api_user");
//echo "Decrypted: " . $enc->decryptPin("313233343536373831323334353637384c9e2f279022bbb37e");

?>
