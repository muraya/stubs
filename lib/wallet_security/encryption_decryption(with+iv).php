<?php

class encryption_decryption {

	/** RSAEncryption Procedure
	 *
	 *	@param   mixed    msg      message/data
	 *	@param   string   k        encryption key
	 *	@param   boolean  base64   base64 encode result
	 *
	 *	@return  string   iv+ciphertext+mac or
	 *           boolean  false on error
	*/
	public function encrypt( $cipher=null, $cipherMode=null,$msg, $secret_key, $returnHexed = false, $returnBase64=false ) {
		$k = str_replace(array("\r\n","\n","\r"),"",$secret_key);
		$returnHexed = true;

		if ($cipher =="rijndael-256") $byteSize = 32; else $byteSize = 16;
		
		# open cipher module (do not change cipher/mode)
		if ( ! $td = mcrypt_module_open($cipher, '', $cipherMode, '') )
			return false;

		//$msg = serialize($msg);					# serialize #commented since was causing errors
		//$iv  = mcrypt_create_iv($byteSize, MCRYPT_RAND);	# create iv

		$iv = "1234567812345678";
		
		//$iv = "4f2afcea680ddc1a";
		if ( mcrypt_generic_init($td, $k, $iv) !== 0 )		# initialize buffers
			return false;

		$msg  = mcrypt_generic($td, $msg);			# encrypt
		$msg  = $iv . $msg;					# prepend iv
		$mac  = $this->pbkdf2($msg, $k, 1000, 32);		# create mac
		//$msg .= $mac;						# append mac

		mcrypt_generic_deinit($td);				# clear buffers
		mcrypt_module_close($td);				# close cipher module

		if ($returnHexed) $msg = bin2hex($msg);
		else if ($returnBase64) $msg = base64_encode($msg);
		return $msg;
}

	/** Decryption Procedure
	 *
	 *	@param   string   msg      output from encrypt()
	 *	@param   string   k        encryption key
	 *	@param   boolean  base64   base64 decode msg
	 *
	 *	@return  string   original message/data or
	 *           boolean  false on error
	*/
	public function decrypt( $cipher=null, $cipherMode=null,$msg, $secret_key, $base64 = false, $toBeUnhexed=true ) {
				
		$k = str_replace(array("\r\n","\n","\r"),"",$secret_key);	

		if ($cipher == 'rijndael-256') $byteSize =32; else $byteSize = 16;
		if ( $base64 ) $msg = base64_decode($msg);					# base64 decode?
		if ( $toBeUnhexed ) $msg = $this->hex2bin($msg);				# unhex message

		# open cipher module (do not change cipher/mode)
		if ( ! $td = mcrypt_module_open($cipher, '', $cipherMode, '') )
			return false;

		$iv  = substr($msg, 0, $byteSize);						# extract iv

		//$mo  = strlen($msg) - $byteSize;						# mac offset
		//$em  = substr($msg, $mo);							# extract mac
		$msg = substr($msg, $byteSize, strlen($msg)-($byteSize));			# extract ciphertext
		//$mac = $this->pbkdf2($iv . $msg, $k, 1000, $byteSize);			# create mac

		//if ( $em !== $mac )								# authenticate mac
		//	return false;

		if ( mcrypt_generic_init($td, $k, $iv) !== 0 )					# initialize buffers
			return false;

		$msg = mdecrypt_generic($td, $msg);						# decrypt
//		$msg = unserialize($msg);							# unserialize

		mcrypt_generic_deinit($td);							# clear buffers
		mcrypt_module_close($td);							# close cipher module

		return $msg;									# return original msg
	}

	/* Function to convert hexadecimal string to binary */
	
	public function hex2bin($h)
	  {
		  if (!is_string($h)) return null;
		  $r='';
		  for ($a=0; $a<strlen($h); $a+=2) { $r.=chr(hexdec($h{$a}.$h{($a+1)})); }
		  return $r;
	  }

	/** PBKDF2 Implementation (as described in RFC 2898);
	 *
	 *	@param   string  p   password
	 *	@param   string  s   salt
	 *	@param   int     c   iteration count (use 1000 or higher)
	 *	@param   int     kl  derived key length
	 *	@param   string  a   hash algorithm
	 *
	 *	@return  string  derived key
	*/
	public function pbkdf2( $p, $s, $c, $kl, $a = 'sha256' ) {

		$hl = strlen(hash($a, null, true));				# Hash length
		$kb = ceil($kl / $hl);						# Key blocks to compute
		$dk = '';							# Derived key

		# Create key
		for ( $block = 1; $block <= $kb; $block ++ ) {

			# Initial hash for this block
			$ib = $b = hash_hmac($a, $s . pack('N', $block), $p, true);

			# Perform block iterations
			for ( $i = 1; $i < $c; $i ++ ) 

				# XOR each iterate
				$ib ^= ($b = hash_hmac($a, $b, $p, true));

			$dk .= $ib; # Append iterated block
		}

		# Return derived key of correct length
		return substr($dk, 0, $kl);
	}
} #end of the class
?>
