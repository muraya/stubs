<?php

/**
 * This file holds the Authorization class used to authenticate the API Users.
 *
 * PHP VERSION 5.3.6
 *
 * This class holds the functions used to authenticate the API Users.
 *
 * @category  Core
 * @package   Authorization
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @copyright 2013 Cellulant Ltd
 * @license   Proprietary License
 * @version   Release:3.0.0
 * @link      http://www.cellulant.com
 */
class Authorization
{

    /**
     * Log class instance.
     *
     * @var object
     */
    private $log;

    /**
     * TAT turn around time for functions or loops.
     * Used for benchmarking
     * @var object
     */
    private $tat;

    /**
     *
     * @var Redis
     */
    private $redis;

    /**
     * @var array|false|string
     * Request headers value
     */
    private $requestHeaders;

    /**
     * @var CoreUtils
     *  CoreUtils class instance
     */
    private $coreUtils;

    /**
     * @int clientID
     * Variable
     */
    private $clientID;

    /**
     * Authorization constructor.
     * @throws FriendlyException
     */
    public function __construct()
    {

        $this->log = new CoreAppLogger();
        $this->coreUtils = new CoreUtils();

        $this->tat = new BenchMark(session_id());
        $this->redis=new RedisController();

        $this -> requestHeaders = $this -> getRequestHeaders();

    }

    /**
     * @return array|false|string
     * Extracts request headers
     */
    private function getRequestHeaders()
    {
        if (!function_exists('getallheaders')) {
            $headers = '';
            foreach ($_SERVER as $name => $value) {
                if (substr($name, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
                }
            }
            return $headers;
        } else {
            return getallheaders();
        }
    }

    /**
     * @param $clientID
     * @return bool
     * @throws FriendlyException
     *  Validates Syndication token
     */
    public function validateToken($clientID)
    {

        $this->clientID = $clientID;

        $this->log->debugLog(
            Config::DEBUG,
            $this -> clientID,
            "Request Headers :".json_encode($this -> requestHeaders)
        );

        if (empty($this -> requestHeaders['syndicationToken'])) {
            $this->coreUtils -> renderError('The token is missing ', StatusCodes::INVALID_TOKEN);

            throw new FriendlyException("Missing token");
        }

        $validateResult = TokenValidation::model()->validateToken($clientID,$this -> requestHeaders['syndicationToken']);

        if ($validateResult == false){
            $this->log->debugLog(Config::DEBUG, $this -> clientID, "Token was not validated successfully.");

            $this->coreUtils ->renderError('Token was not validated successfully token',StatusCodes::INVALID_TOKEN);

            throw new FriendlyException("Token was not validated successfully");
        } else {
            $this->log->debugLog(Config::DEBUG, $this -> clientID, "Token validation success. Client ID = ".$this->clientID);
            return $validateResult;
        }
    }

    /**
     * @param $username
     * @return bool
     * Gets user client from redis cache
     */
    public function getUserclientID($username) {

        $redisKey = Config::COUNTRY_CODE.".AUTH." . $username;

        $result = json_decode($this->redis->getKeyValue($redisKey), true);

        if ($result ==  false or empty($result))
        {
            return false;
        } else {
            // client exists
            $this->log->debugLog(Config::DEBUG, $this -> clientID, "Token validation : Client Exists.".$this->clientID);
            return $result['clientID'];
        }
    }

    public function renderAuthorizationError($message){
        $this->log->errorLog(Config::ERROR, -1, $message);
        header("HTTP/1.1 401 Unauthorized");
        exit();
    }
}