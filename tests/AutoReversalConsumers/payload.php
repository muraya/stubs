<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 1/9/19
 * Time: 11:04 AM
 */

$extraData = array("isSharedPaybill" => false, "paybillNumber" => 512400);

$response=array(
    "payerTransactionID"=> "ER342342FEFW",
    "paymentID"=> 12,
    "merchantPaymentID"=> 150,
    "statusCode"=> 141,
    "clientCode"=> "CELKE",
    "serviceID"=> "1",
    "receiptNumber"=> "wer23423",
    "currency"=> "KES",
    "MSISDN"=> "254708435493",
    "amountPaid"=> 100,
    "customerName"=> "Dan",
    "accountNumber"=> "300528",
    "refundID"=> 0,
    "refundMode"=> "momo",
    "reversalAmount"=> 100,
    "serviceCode"=> "DSTVKE",
    "dataSource"=> "ACK",
    "extraData"=> json_encode($extraData),
);

echo json_encode($response);

exit();
