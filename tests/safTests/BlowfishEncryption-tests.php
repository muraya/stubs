<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 06/03/18
 * Time: 10:42
 */

class BlowFishEncryption
{
    private $alg;
    private $mode;
    private $key;


    public function __construct()
    {

        $this->alg = 'blowfish';
        $this->mode = 'nofb';
        //    $this->key = '$2a$qefqfq67894rkjkv786vcjkhkjv9';//SAFKEConfigs::BLOW_FISH_KEY;
//      $this->iv = 'IGKYbe54';
        $this->key = SAFKEConfigs::BLOW_FISH_KEY;
        $this->iv = SAFKEConfigs::IV;

    }

    public function encrypt($plaintext)
    {
        $td = mcrypt_module_open($this->alg, '', $this->mode, '');
        $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
        mcrypt_generic_init($td, $this->key, $iv);
        $crypttext = mcrypt_generic($td, $plaintext);
        mcrypt_generic_deinit($td);
        return base64_encode($iv.$crypttext);

    }


    public function simpleEncrypt($plaintext)
    {
        $crypttext = mcrypt_encrypt($this->alg, $this->key, $plaintext, $this->mode, $this->iv);
        return base64_encode($crypttext);
    }


    public function decrypt($crypttext)
    {
        $crypttext = base64_decode($crypttext);
        $plaintext = "";
        $td = mcrypt_module_open($this->alg, '', $this->mode, '');
        $ivsize    = mcrypt_enc_get_iv_size($td);
        $iv        = substr($crypttext, 0, $ivsize);
        $crypttext = substr($crypttext, $ivsize);
        if ($iv) {
            mcrypt_generic_init($td, $this->key, $iv);
            $plaintext = mdecrypt_generic($td, $crypttext);
        }
        return $plaintext;

    }

    public function simpleDecrypt($crypttext)
    {
        $crypttext = base64_decode($crypttext);
        $plaintext = mcrypt_decrypt($this->alg, $this->key, $crypttext, $this->mode, $this->iv);
        return $plaintext;
    }



    public static function test()
    {
        $cl=new BlowFishEncryption();
        $plain='1234';
        $enc=$cl->encrypt($plain);
        //  $enc = "NeUSNg==";
        $dec = base64_decode($enc);

        echo " plain text: ".$plain." base64decode:  ".$dec." and decrypted to  ".$cl->decrypt($enc);
    }
}

//BlowFishEncryption::test();
