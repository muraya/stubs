<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 5/23/18
 * Time: 11:57 AM
 */

/**
 *  String response for no accounts
 */
//$string = '{\"SUCCESS\":false,\"customerDetails\":\""}';


/**
 *  Response for a single account
 */
//$string = "{\"SUCCESS\":true,\"customerDetails\":\"179|1|1|customer|Levis|2018-04-11 16:29:40\",\"accountDetails\":\"296|5106556001|mungaiUSD|US Dollar |840|USD |179|1\",\"nominationDetails\":\"brian|0110503002|NATION CENTRE|DTB Kenya|DTB|001|IFT\",\"EXCEPTION\":null}";

/**
 *  Response for two accounts
 */
$string = "{\"SUCCESS\":true,\"customerDetails\":\"179|1|1|customer|Levis|2018-04-11 16:29:40\",\"accountDetails\":\"296|5106556001|mungaiUSD|US Dollar |840|USD |179|1#970|0100023008|Test Ac 2|Kenyan Shilling |404|KES |179|2\",\"nominationDetails\":\"brian|0110503002|NATION CENTRE|DTB Kenya|DTB|001|IFT#Liz|0104928048|NATION CENTRE|DTB Kenya|DTB|001|IFT\",\"EXCEPTION\":null}";

echo $string;

exit();

