<?php
/**
 * Created by PhpStorm.
 * User: mkenya
 * Date: 01/03/18
 * Time: 11:24
 *  A stub class used to render the expected responses
 */

class LinkedAccountsStub
{

    public function fetchMyFI()
    {
        $payload = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "SFC1231231231",
                    "dt" => "20170615082020",
                    "rcode" => 200,
                    "rtext" => "Successfully processed"
                ),
                "responseData" => array(
                    "data" => array(
                        array(
                            "id" => 1,
                            "BankAlias" => "KCBKE",
                            "BankName" => "Kenya Commercial Bank",
                            "BankShortCode" => 522522,
                            "BankID" => "133",
                            "AuthType" => 2
                        ),
                        array(
                            "id" => 2,
                            "BankAlias" => "DTBKE",
                            "BankName" => "Diamond Trust Commercial Bank",
                            "BankShortCode" => 102103,
                            "BankID" => "108",
                            "AuthType" => 2
                        )
                    )
                )
            )
        );

        echo json_encode($payload);exit();
    }

    public function active()
    {
        $payload = array(
            "TransactionResponse" => array(
                "responseHeader" => array(
                    "txid" => "SFC1231231231",
                    "dt" => "20170615082020"
                ),
                "responseData" => array(
                    "rcode" => 200,
                    "rtext" => "Successfully processed"
                )
            )
        );

        echo json_encode($payload);exit();
    }
     public function activate(){
         $this->active();
     }
}