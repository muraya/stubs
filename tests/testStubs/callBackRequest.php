<?php

/**
 * This is a test script that simulates
 * an api call to handle the authcallbackrequest
 *
 * PHP Version 5.4.13
 *
 * @category Core
 * @package  Hub_Services
 * @author    Kevin Mkenya <kevin.mkenya@cellulant.com>
 * @license  Copyright Cellulant Ltd
 * @link     www.cellulant.com
 */


$payload = array(
    "CallbackRequest" => array(
        "requestHeader" => array(
            "txid" => "SFC1231231231",
            "dt" => "20170615082020",
            "tid" => "COOP1231231231",
            "cid" => "FinancialIstitutionToMpesa",
            "ln" => "COOP",
            "ps" => "NjJBQTRGMjJFNTE3MjNENjg0MERDNEU0RTBBNTI5RjUxMjJFODYyMEUyNjdFNzQ4QTBBOUREOERENUVDNzI2OA==",
            "pid" => "2175",
            "pnm" => "SAFARICOM"
        ),
        "resultData" => array(
            "rcode" => 200,
            "rtext" => "Successfully processed",
            "data" => array(
                "BankRefNo" => "123sa23as123",
                "MPESARefNo" => "JWE234234234D",
                "Amount" => "254724280840",
                "Date" => "201711291250123",
                "SenderNo" => "254724280840",
                "SenderAcc" => "12312*******845",
                "RecipientNo" => "254724280840",
                "ReceipientAcc" => "12312*******845"
            )
        )
    )
);




echo json_encode($payload, JSON_PRETTY_PRINT);

exit();


