<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 2/16/19
 * Time: 12:07 AM
 */


$arrX = array(131, 132);
$arrY = array(450, 174);

$randIndexX = array_rand($arrX);
$randIndexY = array_rand($arrY);

$response = array(
    "authStatus" => array(
//        "authStatusCode" => $arrX[$randIndexX],
        "authStatusCode" => 131,
        "authStatusDescription" => "Random Authentication status"
    ),
    "results" => [array(
        "paymentID" => 19,
//        "statusCode" => $arrY[$randIndexY],
        "statusCode" => 450,
        "statusDescription" => "Payment reversal has been initiated awaiting response"
    )],
);


echo json_encode($response);

exit();