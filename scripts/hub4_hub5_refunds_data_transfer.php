<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 3/4/19
 * Time: 8:14 AM
 */

/**
 * This file holds the CaviarBankEnrolments class
 *  The Class fetched bank enrolments from Caviar and creates
 * bank enrolment details as implicit attributes
 *
 * PHP VERSION 7.2
 *
 * @category  Core
 * @package   Authorization
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @copyright 2019 Cellulant Ltd
 * @license   Proprietary License
 * @version   Release:3.0.0
 * @link      http://www.cellulant.com
 */


date_default_timezone_set('Africa/Nairobi');
error_reporting(E_ERROR | E_PARSE);
define('UTILS_DIR', dirname(__FILE__) . '/../lib/');
require UTILS_DIR . 'conf/Config.php';
require UTILS_DIR . 'CoreUtils.php';
require UTILS_DIR . 'db/MySQL.php';
require UTILS_DIR . 'db/DatabaseUtilities.php';
require UTILS_DIR . 'db/SQLException.php';
require UTILS_DIR . 'logger/CoreAppLogger.php';
require UTILS_DIR . 'benchmark/BenchMark.php';


class hub4_hub5_refunds_data_transfer
{

    private $log;
    private $fetchStart;
    private $fetchLimit;
    private $currentMaxRefundID;

    private $hub4Link;
    private $hub5Link;

    public function __construct()
    {

        $this->log = new CoreAppLogger();

        $this->tat = new BenchMark(session_id());
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __CLASS__);

        $this->setFetchLimits();
    }

    /**
     * Sets fetch limits depending on the stored last cron run date
     */
    public function setFetchLimits()
    {

        $this->hub4Link = new PDO("mysql:host=192.168.250.238;dbname=hub4_trunk", 'cellulant', 'c3llul@nt');

        $this->hub5Link = new PDO("mysql:host=127.0.0.1;dbname=hub5_trunk", 'root', 'lemon');


        $this->fetchCurrentMaxRefundID();

        $this->fetchStart = 1;

        $this->fetchLimit = 100;

        $this->log->debugLog(Config::CRON_DEBUG, -1, " currentFetchLimit " . $this->fetchLimit);

    }


    public function process()
    {


        if ($this->fetchRefunds()) {

            $this->checkFetchLimit();

        } else {

            $this->checkFetchLimit();
        }

        return true;

    }

    public function transferRecords($refundsDatum)
    {
        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $this->log->debugLog(Config::CRON_DEBUG, -1, " to be inserted refundID >>>>>>>> " . $refundsDatum['refundID'], '', '');

            $query = "INSERT INTO refunds (refundID,merchantPaymentID,conversionRateID,currencyID,
                      amount,refundType, makerNarration,checkerNarration, active, dateCreated, dateModified,
                      insertedBy, updatedBy)
                      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            //params
            $params = array(
                $refundsDatum['refundID'],
                $refundsDatum['merchantPaymentID'],
                $refundsDatum['conversionRateID'],
                $refundsDatum['currencyID'],
                $refundsDatum['amount'],
                $refundsDatum['refundType'] == 'full'? 'reversal' : 'refund',
                $refundsDatum['makerNarration'],
                $refundsDatum['checkerNarration'],
                $refundsDatum['active'],
                $refundsDatum['dateCreated'],

                $refundsDatum['dateModified'],
                $refundsDatum['insertedBy'],
                $refundsDatum['updatedBy']
            );


            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->hub5Link->prepare($query)->execute($params);

            $this->log->debugLog(Config::CRON_DEBUG, -1, " inserted ID <<<<<< " . $refundsDatum['paymentID'], '', '');

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (PDOException $exception) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $exception->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
        }
    }

    public function checkFetchLimit()
    {
        if ($this->fetchLimit > $this->currentMaxRefundID) {
            $this->log->infoLog(Config::CRON_INFO, -1, " Processing Done ");
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __CLASS__);

            echo 'Processing Done';

            exit();

        } else {

            $this->increaseFetchLimit();

            $this->process();

        }
    }


    public function fetchRefunds()
    {

        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {


            // we are including the length part so that we skip invalid mobile numbers
            $query = "SELECT r.reversalID as refundID,p.requestLogID as merchantPaymentID, r.conversionID as conversionRateID,p.currencyID,
                      r.amount, r.reversalType as refundType, r.makerNarration,r.checkerNarration, r.active, r.dateCreated, r.dateModified,
                      r.insertedBy, r.updatedBy
                      FROM s_reversals r
                      JOIN s_payments p on r.paymentID = p.paymentID
                      WHERE  r.reversalID >= ? AND r.reversalID < ?";

            $params = array($this->fetchStart, $this->fetchLimit);

            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


            $records = $this->hub4Link->prepare($query);

            $records->execute($params);

            foreach ($records as $record) {

                $this->transferRecords($record);

            }

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return true;

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return false;
        }
    }


    public function fetchCurrentMaxRefundID()
    {


        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $query = "SELECT MAX(reversalID) AS reversalID FROM s_reversals";

            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $currentMaxRefundID = $this->hub4Link->query($query)->fetch();

            $this->currentMaxRefundID = $currentMaxRefundID['reversalID'];

            $this->log->infoLog(Config::CRON_INFO, -1, " currentMaxRefundID " .  $this->currentMaxRefundID );

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            exit();

        }

    }

    /**
     *  Increases the fetchLimit
     * @return bool
     */
    public function increaseFetchLimit()
    {
        $this->fetchStart = $this->fetchLimit;

        $this->fetchLimit += 100;

        return true;
    }

    private function getRefundType($refundType)
    {
        if ($refundType == 'full')
        {
            return 'reversal';
        }
        else {
            return 'refund';
        }
    }

}

$class = new hub4_hub5_refunds_data_transfer();
$class->process();