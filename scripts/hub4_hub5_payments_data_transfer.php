<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 2/28/19
 * Time: 2:03 PM
 */

/**
 * This file holds the CaviarBankEnrolments class
 *  The Class fetched bank enrolments from Caviar and creates
 * bank enrolment details as implicit attributes
 *
 * PHP VERSION 7.2
 *
 * @category  Core
 * @package   Authorization
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @copyright 2019 Cellulant Ltd
 * @license   Proprietary License
 * @version   Release:3.0.0
 * @link      http://www.cellulant.com
 */


date_default_timezone_set('Africa/Nairobi');
error_reporting(E_ERROR | E_PARSE);
define('UTILS_DIR', dirname(__FILE__) . '/../lib/');
require UTILS_DIR . 'conf/Config.php';
require UTILS_DIR . 'CoreUtils.php';
require UTILS_DIR . 'db/MySQL.php';
require UTILS_DIR . 'db/DatabaseUtilities.php';
require UTILS_DIR . 'db/SQLException.php';
require UTILS_DIR . 'logger/CoreAppLogger.php';
require UTILS_DIR . 'benchmark/BenchMark.php';


class hub4_hub5_payments_data_transfer
{

    private $log;
    private $fetchStart;
    private $fetchLimit;
    private $currentMaxPaymentID;
    private $dbRetries;
    private $hub4Link;
    private $hub5Link;

    public function __construct()
    {

        $this->log = new CoreAppLogger();

        $this->tat = new BenchMark(session_id());
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __CLASS__);

        $this->setFetchLimits();
    }

    /**
     * Sets fetch limits depending on the stored last cron run date
     */
    public function setFetchLimits()
    {

        $this->hub4Link = new PDO("mysql:host=192.168.250.238;dbname=hub4_trunk", 'cellulant', 'c3llul@nt');

        $this->hub5Link = new PDO("mysql:host=127.0.0.1;dbname=hub5_trunk", 'root', 'lemon');


        $this->fetchCurrentMaxPaymentID();

        $this->fetchStart = 1;

        $this->fetchLimit = 100;

        $this->log->debugLog(Config::CRON_DEBUG, -1, " currentFetchLimit " . $this->fetchLimit);

    }


    public function process()
    {


        if ($this->fetchPayments()) {

            $this->checkFetchLimit();

        } else {

            $this->checkFetchLimit();
        }

        return true;

    }

    public function transferRecords($paymentsDatum)
    {
        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $this->log->debugLog(Config::CRON_DEBUG, -1, " to be inserted ID >>>>>>>> " . $paymentsDatum['currencyID'], '', '');

            $query = "INSERT INTO payments (paymentID, currencyID, serviceID, payerClientID,payerTransactionID,requestOriginID,
                      payerCode, channelRequestID, MSISDN, accountNumber,invoiceNumber,customerName,chargeAmount,amountPaid,
                      paymentMode, paymentStatus, paymentStatusHistory, payerClientStatus,payerNarration,extraData,statusPushedDesc,statusPushed,
                      numberOfSends, paymentDate, summaryBucketID, isSummarised,statusFirstSend,statusLastSend,dateCreated,dateModified,
                      insertedBy, updatedBy)
                      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            //params
            $params = array(
                $paymentsDatum['paymentID'],
                $paymentsDatum['currencyID'],
                $paymentsDatum['serviceID'],
                $paymentsDatum['payerClientID'],
                $paymentsDatum['payerTransactionID'],
                $paymentsDatum['requestOriginID'],
                $paymentsDatum['payerCode'],
                $paymentsDatum['channelRequestID'],
                $paymentsDatum['MSISDN'],
                $paymentsDatum['accountNumber'],

                $paymentsDatum['invoiceNumber'],
                $paymentsDatum['customerName'],
                $paymentsDatum['chargeAmount'],
                $paymentsDatum['amountPaid'],
                $paymentsDatum['paymentMode'],
                $paymentsDatum['paymentStatus'],
                $paymentsDatum['paymentStatusHistory'],
                $paymentsDatum['payerClientStatus'],
                $paymentsDatum['payerNarration'],
                $paymentsDatum['extraData'],

                $paymentsDatum['statusPushedDesc'],
                $paymentsDatum['statusPushed'],
                $paymentsDatum['numberOfSends']== null ? 0 : $paymentsDatum['numberOfSends'] ,
                $paymentsDatum['paymentDate'],
                $paymentsDatum['summaryBucketID'],
                $paymentsDatum['isSummarised'],
                $paymentsDatum['statusFirstSend'],
                $paymentsDatum['statusLastSend'],
                $paymentsDatum['dateCreated'],
                $paymentsDatum['dateModified'],

                $paymentsDatum['insertedBy'],
                $paymentsDatum['updatedBy']
            );


            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->hub5Link->prepare($query)->execute($params);

            $this->log->debugLog(Config::CRON_DEBUG, -1, " inserted ID <<<<<< " . $paymentsDatum['paymentID'], '', '');

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (PDOException $exception) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $exception->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
        }
    }

    public function checkFetchLimit()
    {
        if ($this->fetchLimit > $this->currentMaxPaymentID) {
            $this->log->infoLog(Config::CRON_INFO, -1, " Processing Done ");
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __CLASS__);

            echo 'Processing Done';

            exit();

        } else {

            $this->increaseFetchLimit();

            $this->process();

        }
    }


    public function fetchPayments()
    {

        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {


            // we are including the length part so that we skip invalid mobile numbers
            $query = "SELECT sp.*, rl.requestOriginID, rl.accountNumber, rl.paymentMode, rl.overallStatus as paymentStatus,
                        rl.overallStatusHistory as paymentStatusHistory, sp.bucketID as summaryBucketID,sp.summarised as  isSummarised,
                        cl.clientCode as payerCode, rl.hubID as channelRequestID
                        FROM s_payments sp
                        JOIN s_requestLogs rl ON sp.requestLogID = rl.requestLogID 
                        JOIN clients cl ON sp.payerClientID = cl.clientID 
                        WHERE  sp.paymentID >= ? AND sp.paymentID < ?";

            $params = array($this->fetchStart, $this->fetchLimit);

            $records = $this->hub4Link->prepare($query);

            $records->execute($params);

            foreach ($records as $record) {

                $this->transferRecords($record);

            }

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return true;

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return false;
        }
    }


    public function fetchCurrentMaxPaymentID()
    {


        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {


            $query = "SELECT MAX(paymentID) AS paymentID FROM s_payments";

            $currentMaxPaymentID = $this->hub4Link->query($query)->fetch();

            $this->currentMaxPaymentID = $currentMaxPaymentID['paymentID'];

            $this->log->infoLog(Config::CRON_INFO, -1, " currentMaxPaymentID " . $this->currentMaxPaymentID);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            exit();

        }

    }

    /**
     *  Increases the fetchLimit
     * @return bool
     */
    public function increaseFetchLimit()
    {
        $this->fetchStart = $this->fetchLimit;

        $this->fetchLimit += 100;

        return true;
    }

}

$class = new hub4_hub5_payments_data_transfer();
$class->process();