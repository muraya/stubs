<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 2/28/19
 * Time: 8:29 PM
 */

/**
 * This file holds the CaviarBankEnrolments class
 *  The Class fetched bank enrolments from Caviar and creates
 * bank enrolment details as implicit attributes
 *
 * PHP VERSION 7.2
 *
 * @category  Core
 * @package   Authorization
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @copyright 2019 Cellulant Ltd
 * @license   Proprietary License
 * @version   Release:3.0.0
 * @link      http://www.cellulant.com
 */


date_default_timezone_set('Africa/Nairobi');
error_reporting(E_ERROR | E_PARSE);
define('UTILS_DIR', dirname(__FILE__) . '/../lib/');
require UTILS_DIR . 'conf/Config.php';
require UTILS_DIR . 'logger/CoreAppLogger.php';
require UTILS_DIR . 'benchmark/BenchMark.php';


class hub4_hub5_invoices_data_transfer
{

    private $log;
    private $fetchStart;
    private $fetchLimit;
    private $currentMaxInvoiceID;

    private $hub4Link;
    private $hub5Link;

    public function __construct()
    {

        $this->log = new CoreAppLogger();

        $this->tat = new BenchMark(session_id());
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __CLASS__);

        $this->setFetchLimits();
    }

    /**
     * Sets fetch limits depending on the stored last cron run date
     */
    public function setFetchLimits()
    {

        $this->hub4Link = new PDO("mysql:host=192.168.250.238;dbname=hub4_trunk", 'cellulant', 'c3llul@nt');

        $this->hub5Link = new PDO("mysql:host=127.0.0.1;dbname=hub5_trunk", 'root', 'lemon');


        $this->fetchCurrentMaxInvoiceID();

        $this->fetchStart = 1;

        $this->fetchLimit = 100;

        $this->log->debugLog(Config::CRON_DEBUG, -1, " currentFetchLimit " . $this->fetchLimit);

    }


    public function fetchInvoices()
    {

        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {


            // we are including the length part so that we skip invalid mobile numbers
            $query = "SELECT si.*, rl.requestOriginID, rl.languageID, rl.accountNumber, rl.invoiceNumber ,
                        rl.invoiceAlias , rl.invoiceAlias ,rl.overallStatus,rl.serviceID ,rl.MSISDN 
                        FROM s_invoices si
                        LEFT JOIN s_requestLogs rl ON si.requestLogID = rl.requestLogID 
                        WHERE  si.invoiceID >= ? AND si.invoiceID < ?";

            $params = array($this->fetchStart, $this->fetchLimit);

            $records = $this->hub4Link->prepare($query);

            $records->execute($params);

            foreach ($records as $record) {

                $this->transferRecords($record);

            }

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return true;

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return false;
        }
    }


    public function fetchCurrentMaxInvoiceID()
    {


        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $query = "SELECT MAX(invoiceID) AS invoiceID FROM s_invoices";

            $currentMaxInvoiceID = $this->hub4Link->query($query)->fetch();

            $this->currentMaxInvoiceID = $currentMaxInvoiceID['invoiceID'];

            $this->log->infoLog(Config::CRON_INFO, -1, " currentMaxInvoiceID " . $this->currentMaxInvoiceID);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            exit();

        }
    }


    public function process()
    {


        if ($this->fetchInvoices()) {

            $this->checkFetchLimit();

        } else {

            $this->checkFetchLimit();
        }

        return true;

    }

    public function transferRecords($invoicesDatum)
    {
        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $this->log->debugLog(Config::CRON_DEBUG, -1, " to be inserted ID >>>>>>>> " . $invoicesDatum['invoiceID'], '', '');

            $query = "INSERT INTO invoices (invoiceID, serviceID, currencyID, requestOriginID, conversionRateID,originalCurrencyID,languageID,
                      MSISDN, accountNumber, invoiceNumber, accountName,originalAmount,amount,totalAmountPaid,actualConversionRate,
                      dueDate, expiryDate, invoiceAlias, narration,timeOut,invoicePaidStatus,raisedByAPP,invoicePayments,
                      sendAppAlert, profiled, isRefreshed, canRemind,profilingBucket,extraData,metaData,nextRefreshDate,
                      dateRecieved, dateModified, dateCreated, insertedBy, updatedBy)
                      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

            //params
            $params = array(
                $invoicesDatum['invoiceID'],
                $invoicesDatum['serviceID'],
                $invoicesDatum['currencyID'],
                $invoicesDatum['requestOriginID'],
                $invoicesDatum['conversionRateID'],
                $invoicesDatum['originalCurrencyID'],
                $invoicesDatum['languageID'],

                $invoicesDatum['MSISDN'],
                $invoicesDatum['accountNumber'],
                $invoicesDatum['invoiceNumber'],
                $invoicesDatum['accountName'],
                $invoicesDatum['originalAmount'],
                $invoicesDatum['amount'],
                $invoicesDatum['totalAmountPaid'],
                $invoicesDatum['actualConversionRate'],

                $invoicesDatum['dueDate'],
                $invoicesDatum['expiryDate'],
                $invoicesDatum['invoiceAlias'],
                $invoicesDatum['narration'],
                $invoicesDatum['timeOut'],
                $this->getInvoiceStatus($invoicesDatum['invoicePaidStatus'], $invoicesDatum['overallStatus']),
                $invoicesDatum['raisedByAPP'],
                $invoicesDatum['invoicePayments'],

                $invoicesDatum['sendAppAlert'],
                $invoicesDatum['profiled'],
                $invoicesDatum['isRefreshed'],
                $invoicesDatum['canRemind'],
                $invoicesDatum['profilingBucket'],
                $invoicesDatum['extraData'],
                $invoicesDatum['metaData'],
                $invoicesDatum['nextRefreshDate'],

                $invoicesDatum['dateRecieved'],
                $invoicesDatum['dateModified'],
                $invoicesDatum['dateCreated'],
                $invoicesDatum['insertedBy'],
                $invoicesDatum['updatedBy']

            );

            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->hub5Link->prepare($query)->execute($params);

            $this->log->debugLog(Config::CRON_DEBUG, -1, " inserted ID <<<<<< " . $invoicesDatum['invoiceID'], '', '');

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (PDOException $exception) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $exception->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
        }
    }

    public function checkFetchLimit()
    {
        if ($this->fetchLimit > $this->currentMaxInvoiceID) {
            $this->log->infoLog(Config::CRON_INFO, -1, " Processing Done ");
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __CLASS__);

            echo 'Processing Done';

            exit();

        } else {

            $this->increaseFetchLimit();

            $this->process();

        }
    }

    /**
     *  Increases the fetchLimit
     * @return bool
     */
    public function increaseFetchLimit()
    {
        $this->fetchStart = $this->fetchLimit;

        $this->fetchLimit += 100;

        return true;
    }

    private function getInvoiceStatus($invoicePaidStatus, $overallStatus)
    {
        $invoiceStatusesArray = array(111,112,118,119,120,121,123,130,135,146
            ,159,175,195,221,226,227,228,233,250,251,252,253,254,255,256,257);


        if (in_array($overallStatus,$invoiceStatusesArray)) {
            return $overallStatus;
        }
        else {
            if ($overallStatus = 140)
            {
                // invoice paid
                return 253;
            }
            else if ($overallStatus = 139)
            {
                return 250;
            }
            else if ($invoicePaidStatus == 'exact' || $invoicePaidStatus == 'over')
            {
                return 253;
            }
            else if ($overallStatus = 141 || $overallStatus = 138 || $overallStatus = 216)
            {
                return 251;
            }
            else if ($invoicePaidStatus == 'partial' || $invoicePaidStatus = 294)
            {
                return 252;
            }
            else {
                // to be discussed
                return $overallStatus;
            }
        }

    }

}

$class = new hub4_hub5_invoices_data_transfer();
$class->process();