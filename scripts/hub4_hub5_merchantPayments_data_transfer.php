<?php
/**
 * Created by PhpStorm.
 * User: muraya
 * Date: 3/1/19
 * Time: 2:12 PM
 */

/**
 * This file holds the CaviarBankEnrolments class
 *  The Class fetched bank enrolments from Caviar and creates
 * bank enrolment details as implicit attributes
 *
 * PHP VERSION 7.2
 *
 * @category  Core
 * @package   Authorization
 * @author    Duncan Muraya <duncan.muraya@cellulant.com>
 * @copyright 2019 Cellulant Ltd
 * @license   Proprietary License
 * @version   Release:3.0.0
 * @link      http://www.cellulant.com
 */


date_default_timezone_set('Africa/Nairobi');
error_reporting(E_ERROR | E_PARSE);
define('UTILS_DIR', dirname(__FILE__) . '/../lib/');
require UTILS_DIR . 'conf/Config.php';
require UTILS_DIR . 'CoreUtils.php';
require UTILS_DIR . 'db/MySQL.php';
require UTILS_DIR . 'db/DatabaseUtilities.php';
require UTILS_DIR . 'db/SQLException.php';
require UTILS_DIR . 'logger/CoreAppLogger.php';
require UTILS_DIR . 'benchmark/BenchMark.php';


class hub4_hub5_merchantPayments_data_transfer
{

    private $log;
    private $fetchStart;
    private $fetchLimit;

    private $hub4Link;
    private $hub5Link;
    private $currentMaxRequestLogID;

    public function __construct()
    {

        $this->log = new CoreAppLogger();

        $this->tat = new BenchMark(session_id());
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __CLASS__);

        $this->setFetchLimits();
    }

    /**
     * Sets fetch limits depending on the stored last cron run date
     */
    public function setFetchLimits()
    {

        $this->hub4Link = new PDO("mysql:host=192.168.250.238;dbname=hub4_trunk", 'cellulant', 'c3llul@nt');

        $this->hub5Link = new PDO("mysql:host=127.0.0.1;dbname=hub5_trunk", 'root', 'lemon');

        $this->fetchCurrentMaxRequestLogID();

        $this->fetchStart = 1;

        $this->fetchLimit = 100;

        $this->log->debugLog(Config::CRON_DEBUG, -1, " currentFetchLimit " . $this->fetchLimit);

    }


    public function fetchCurrentMaxRequestLogID()
    {


        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $query = "SELECT MAX(requestLogID) AS requestLogID FROM s_requestLogs";

            $currentMaxRequestLogID = $this->hub4Link->query($query)->fetch();

            $this->currentMaxRequestLogID = $currentMaxRequestLogID['requestLogID'];

            $this->log->infoLog(Config::CRON_INFO, -1, " currentMaxRequestLogID " . $this->currentMaxRequestLogID);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            exit();

        }

    }

    public function process()
    {


        if ($this->fetchRequestLogs()) {

            $this->checkFetchLimit();

        } else {

            $this->checkFetchLimit();
        }

        return true;

    }

    public function fetchRequestLogs()
    {

        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            // we are including the length part so that we skip invalid mobile numbers
            $query = "SELECT rl.requestLogID as merchantPaymentID,i.invoiceID, rl.receiverClientID, rl.serviceID, p.merchantTierID,
                        p.convertedCurrencyID, p.conversionRateID,rl.languageID, rl.MSISDN,p.actualConversionRate, p.currencyID,
                        p.convertedAmount, p.amountPaid, rl.accountNumber, p.receiptNumber, p.receiverNarration,p.paymentID,
                        p.extraData,rl.paymentPushedStatus,rl.paymentPushedDesc,p.paymentAckDate,rl.isSync, rl.overallStatus as merchantPaymentStatus,
                        p.isTokenService,p.isMNP,rl.narration,rl.timeout,rl.numberOfSends,rl.paymentFirstSend,
                        rl.paymentLastSend,rl.dateEscalated,rl.dateCreated,rl.dateModified,p.insertedBy,p.updatedBy
                        FROM s_requestLogs rl
                        JOIN s_payments p ON rl.requestLogID = p.requestLogID 
                        LEFT JOIN s_invoices i ON rl.requestLogID = i.requestLogID
                        WHERE  rl.overallStatus <> 138
                        AND  rl.requestLogID >= ? AND rl.requestLogID < ?";

            $params = array($this->fetchStart, $this->fetchLimit);

            $this->hub4Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $records = $this->hub4Link->prepare($query);

            $records->execute($params);

            foreach ($records as $record) {

                $this->transferRecords($record);

            }

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return true;

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

            return false;

        }
    }

    public function transferRecords($merchantPaymentsDatum)
    {
        // get the data
        $this->tat->start(BenchMark::FUNCTION_LEVEL, __METHOD__);

        try {

            $this->log->debugLog(Config::CRON_DEBUG, -1,
                " to be inserted merchantPaymentID >>>>>>>>" . $merchantPaymentsDatum['merchantPaymentID'], '', '');

            $query = "INSERT INTO merchantPayments (merchantPaymentID, invoiceID, receiverClientID, serviceID,merchantTierID,currencyID, 
                      convertedCurrencyID, conversionRateID, languageID, MSISDN,actualConversionRate,convertedAmount,amountPaid,accountNumber, 
                      receiptNumber, receiverNarration, extraData, paymentPushedStatus,paymentPushedDesc,paymentAckDate,isSync,isTokenService, 
                      isMNP, narration, timeout,numberOfSends,merchantPaymentStatus,paymentFirstSend,paymentLastSend, 
                      dateEscalated, dateCreated, insertedBy, updatedBy) 
                      VALUES (?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?, ?, ?, ?, ?,  ?, ?, ?, ?)";

            //params
            $params = array(
                $merchantPaymentsDatum['merchantPaymentID'],
                $merchantPaymentsDatum['invoiceID'],
                $merchantPaymentsDatum['receiverClientID'],
                $merchantPaymentsDatum['serviceID'],
                $merchantPaymentsDatum['merchantTierID'],
                $merchantPaymentsDatum['currencyID'],

                $merchantPaymentsDatum['convertedCurrencyID'],
                $merchantPaymentsDatum['conversionRateID'] == null ? 0 : $merchantPaymentsDatum['conversionRateID'],

                $merchantPaymentsDatum['languageID'],
                $merchantPaymentsDatum['MSISDN'],
                $merchantPaymentsDatum['actualConversionRate'],
                $merchantPaymentsDatum['convertedAmount'],
                $merchantPaymentsDatum['amountPaid'],
                $merchantPaymentsDatum['accountNumber'],

                $merchantPaymentsDatum['receiptNumber'],
                $merchantPaymentsDatum['receiverNarration'],
                $merchantPaymentsDatum['extraData'],
                $merchantPaymentsDatum['paymentPushedStatus'],
                $merchantPaymentsDatum['paymentPushedDesc'],
                $merchantPaymentsDatum['paymentAckDate'] == null ? '0001-01-01 00:00:00' : $merchantPaymentsDatum['paymentAckDate'],

                $merchantPaymentsDatum['isSync'],
                $merchantPaymentsDatum['isTokenService'] == null ? 0 : $merchantPaymentsDatum['isTokenService'],


                $merchantPaymentsDatum['isMNP'],
                $merchantPaymentsDatum['narration'],
                $merchantPaymentsDatum['timeout'],
                $merchantPaymentsDatum['numberOfSends'],
                $merchantPaymentsDatum['merchantPaymentStatus'],
                $merchantPaymentsDatum['paymentFirstSend'],
                $merchantPaymentsDatum['paymentLastSend'],

                $merchantPaymentsDatum['dateEscalated'],
                $merchantPaymentsDatum['dateCreated'],
                $merchantPaymentsDatum['insertedBy'],
                $merchantPaymentsDatum['updatedBy']
            );

            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->hub5Link->prepare($query)->execute($params);

            $this->log->debugLog(Config::CRON_DEBUG, -1, " inserted paymentID <<<<<< " . $merchantPaymentsDatum['merchantPaymentID'], '', '');

            $this->generateMerchantPaymentMappings($merchantPaymentsDatum['paymentID'], $merchantPaymentsDatum['merchantPaymentID']
                , $merchantPaymentsDatum['dateCreated'], $merchantPaymentsDatum['dateModified']);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (PDOException $exception) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $exception->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
        }
    }

    public function generateMerchantPaymentMappings($paymentsID, $merchantPaymentsID, $dateCreated, $dateModified)
    {
        try {

            $query = "INSERT INTO merchantPaymentMappings (paymentID, merchantPaymentID,dateCreated, dateModified)
                      VALUES (?, ?, ?, ?)";

            $params = array(
                $paymentsID,
                $merchantPaymentsID,
                $dateCreated,
                $dateModified,
            );

            $this->hub5Link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $this->hub5Link->prepare($query)->execute($params);

            $this->log->debugLog(Config::CRON_DEBUG, -1, " inserted merchant payments mappings", '', '');

        } catch (PDOException $exception) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $exception->getMessage(), -1);

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);

        } catch (Exception $ex) {

            $this->log->errorLog(Config::CRON_ERROR, -1, $ex->getMessage());

            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __METHOD__);
        }

    }

    public function checkFetchLimit()
    {
        if ($this->fetchLimit > $this->currentMaxRequestLogID) {
            $this->log->infoLog(Config::CRON_INFO, -1, " Processing Done ");
            $this->tat->logTAT(BenchMark::FUNCTION_LEVEL, __CLASS__);

            echo 'Processing Done';

            exit();

        } else {

            $this->increaseFetchLimit();

            $this->process();

        }
    }


    /**
     *  Increases the fetchLimit
     * @return bool
     */
    public function increaseFetchLimit()
    {
        $this->fetchStart = $this->fetchLimit;

        $this->fetchLimit += 100;

        return true;
    }

}

$class = new hub4_hub5_merchantPayments_data_transfer();
$class->process();